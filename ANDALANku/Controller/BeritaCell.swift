//
//  BeritaCell.swift
//  Andalanku
//
//  Created by Handoyo on 16/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class BeritaCell: UITableViewCell {

    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var tglPromo: UILabel!
    @IBOutlet var viewPromo: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewPromo.layer.borderColor = UIColor.lightGray.cgColor
        viewPromo.layer.borderWidth = 0.5
        viewPromo.layer.shadowColor = UIColor.black.cgColor
        viewPromo.layer.shadowOpacity = 0.5
        viewPromo.layer.shadowOffset = CGSize(width: 1, height: 3)
        viewPromo.layer.cornerRadius = 10
        
        super.awakeFromNib()
        imgPromo.layer.shadowColor = UIColor.black.cgColor
        imgPromo.layer.shadowOpacity = 0.5
        imgPromo.layer.shadowOffset = CGSize.zero
        imgPromo.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            imgPromo.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                guard let data = data, error == nil else { return }
                self.imgPromo.image = UIImage(data: data as Data)
            }
        }
    }

}
