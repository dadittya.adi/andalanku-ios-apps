//
//  BeritaDetail.swift
//  Andalanku
//
//  Created by Handoyo on 16/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class BeritaDetail: UIViewController {

    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var titlePromo: UILabel!
    @IBOutlet var contentPromo: UILabel!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet weak var viewMain: UIView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewMain.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_full")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        imgPromo.layer.borderColor = UIColor.lightGray.cgColor
        imgPromo.layer.borderWidth = 0.5
        imgPromo.layer.cornerRadius = 10
        let prefs:UserDefaults = UserDefaults.standard
        let imgPromo = prefs.value(forKey: "imgNews") as! String
        let contentPromo = prefs.value(forKey: "contentNews") as! String
        let titlePromo = prefs.value(forKey: "titleNews") as! String
        downloadImage(URL(string: imgPromo)!)
        self.titlePromo.text = titlePromo
        let htmlString = contentPromo
        let htmlText = htmlString
        if let htmlData = htmlText.data(using: String.Encoding.unicode) {
            do {
                let attributedText = try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
                self.contentPromo.attributedText = attributedText
            } catch let e as NSError {
                print("Couldn't translate \(String(describing: htmlText)): \(e.localizedDescription) ")
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                guard let data = data, error == nil else { return }
                self.imgPromo.image = UIImage(data: data as Data)
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

}
