//
//  CashValue.swift
//  Andalanku
//
//  Created by Handoyo on 08/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController
import Alamofire
import SwiftyJSON

class CashValue: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewCash: UIView!
    @IBOutlet weak var txtNoKontrak: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var cvJSON: UICollectionView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var lblCash: UILabel!
    
    var arrImageCash: [UIImage] = [
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!
    ]
    
    var arrTitleCash = ["Lorem ipsum dolor","Lorem ipsum dolor","Lorem ipsum dolor"]
    var arrContentCash = ["Lorem ipsum dolor","Lorem ipsum dolor","Lorem ipsum dolor"]
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            if (no_kontrak != nil && no_kontrak != "") {
                self.txtNoKontrak.text = no_kontrak
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        viewCash.layer.shadowColor = UIColor.black.cgColor
        viewCash.layer.shadowOpacity = 0.3
        viewCash.layer.shadowOffset = CGSize.zero
        viewCash.layer.cornerRadius = 10
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(CashValue.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoKontrak.delegate = self
        
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            //let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "cash_value"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                for item in json["cash_value_list"].arrayValue {
                                    self.lblCash.text = "Rp. " + json["total_cash_value"].stringValue
                                    self.txtNoKontrak.text = item["AgreementNo"].stringValue
                                }
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("cash", forKey: "set_no_kontrak")
            prefs.synchronize()
            self.txtNoKontrak.resignFirstResponder()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("cash", forKey: "set_no_kontrak")
            prefs.synchronize()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvJSON {
            return arrTitleCash.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            if collectionView == self.cvJSON {
                /*
                let news_image : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
                let content_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
                let title_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
                
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(news_image as String, forKey: "imgNews")
                prefs.set(content_news as String, forKey: "contentNews")
                prefs.set(title_news as String, forKey: "titleNews")
                prefs.synchronize()
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_berita_detail")
                 vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                */
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if collectionView == self.cvJSON {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CashValueCell
            /*
            let news_image : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            let content_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
            let title_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
            
            cell.downloadImage(URL(string: news_image as String)!)
            cell.lblBerita.text = content_news as String
            cell.lblTitle.text = title_news as String
            */
            
            cell.lblTitle.text = arrTitleCash[indexPath.row]
            cell.imgCash.image = arrImageCash[indexPath.row]
            cell.lblContent.text = arrContentCash[indexPath.row]
            
            return cell as CashValueCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
