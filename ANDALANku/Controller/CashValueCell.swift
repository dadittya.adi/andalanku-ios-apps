//
//  CashValueCell.swift
//  Andalanku
//
//  Created by Macintosh on 19/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class CashValueCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCash: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var viewCash: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCash.layer.shadowColor = UIColor.black.cgColor
        viewCash.layer.shadowOpacity = 0.5
        viewCash.layer.shadowOffset = CGSize.zero
        viewCash.layer.cornerRadius = 10
        
        imgCash.layer.shadowColor = UIColor.black.cgColor
        imgCash.layer.shadowOpacity = 0.5
        imgCash.layer.shadowOffset = CGSize.zero
        imgCash.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            imgCash.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                guard let data = data, error == nil else { return }
                self.imgCash.image = UIImage(data: data as Data)
            }
        }
    }
}
