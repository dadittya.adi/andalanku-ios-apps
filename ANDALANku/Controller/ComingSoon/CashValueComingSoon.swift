//
//  CashValueComingSoon.swift
//  Andalanku
//
//  Created by Dadittya Adi on 06/03/20.
//  Copyright © 2020 Lokavor. All rights reserved.
//

import UIKit

class CashValueComingSoon: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTC: UITextView!
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowOpacity = 0.5
        contentView.layer.shadowOffset = CGSize(width: 1, height: 3)
        contentView.layer.cornerRadius = 10
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
                //let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_main")
                //vc.modalPresentationStyle = .fullScreen
                //self.present(vc, animated: true, completion: nil)
            let MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = MainStoryboard.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            
        })
    }
    
}
