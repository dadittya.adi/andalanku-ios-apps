//
//  DanaAndalanku.swift
//  Andalanku
//
//  Created by Handoyo on 13/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController
import Alamofire
import SwiftyJSON

class DanaAndalanku: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblAgunan: UILabel!
    @IBOutlet weak var arrowAgunan: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtBPKB: TextField!
    @IBOutlet weak var txtTenor: TextField!
    @IBOutlet weak var txtLocation: TextField!
    @IBOutlet weak var imgTenor: UIImageView!
    @IBOutlet weak var imgWilayah: UIImageView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var txtKebutuhan: TextField!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let prefs:UserDefaults = UserDefaults.standard
        let kebutuhan_dana = prefs.value(forKey: "kebutuhan_dana") as? String
        if (kebutuhan_dana != nil && kebutuhan_dana != "") {
            self.txtKebutuhan.text = kebutuhan_dana!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapLblAgunan = UITapGestureRecognizer(target:self, action:#selector(DanaAndalanku.goAgunanTap(_:)))
        self.lblAgunan.isUserInteractionEnabled = true
        self.lblAgunan.addGestureRecognizer(tapLblAgunan)
        
        let tapArrowAgunan = UITapGestureRecognizer(target:self, action:#selector(DanaAndalanku.goAgunanTap(_:)))
        self.arrowAgunan.isUserInteractionEnabled = true
        self.arrowAgunan.addGestureRecognizer(tapArrowAgunan)
        self.scrollView.keyboardDismissMode = .onDrag
        self.load.isHidden = true
        
        let prefs:UserDefaults = UserDefaults.standard
        let atas_nama = prefs.value(forKey: "atas_nama") as? String
        if (atas_nama != nil && atas_nama != "") {
            self.txtBPKB.text = "BPKB MOBIL An. " + atas_nama!
        }
        
        self.txtTenor.delegate = self
        self.txtLocation.delegate = self
        
        let tapImgTenor = UITapGestureRecognizer(target:self, action:#selector(DanaAndalanku.goTenorTap(_:)))
        self.imgTenor.isUserInteractionEnabled = true
        self.imgTenor.addGestureRecognizer(tapImgTenor)
        
        let tapImgWilayah = UITapGestureRecognizer(target:self, action:#selector(DanaAndalanku.goWilayahTap(_:)))
        self.imgWilayah.isUserInteractionEnabled = true
        self.imgWilayah.addGestureRecognizer(tapImgWilayah)
    }
    
    @IBAction func goTxtTenorBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("dana", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtTenor.resignFirstResponder()
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goTenorTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("dana", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtWilayahBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("dana", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtLocation.resignFirstResponder()
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goWilayahTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("dana", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func txtKebutuhanChange(_ sender: Any) {
        let valString:String = self.txtKebutuhan.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtKebutuhan.text = formattedTipAmount
        }
    }
    
    @objc func goAgunanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("BPKB MOBIL", forKey: "no_dokumen")
            prefs.set(self.txtKebutuhan.text!, forKey: "kebutuhan_dana")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_data_agunan")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }

    @IBAction func goKalkulasi(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            var wilayah:String = ""
            if self.txtLocation.text == "Sumatra dan Kepulauannya" {
                wilayah = "1"
            } else if self.txtLocation.text == "Jakarta, Banten dan Jawa Barat" {
                wilayah = "2"
            } else if self.txtLocation.text == "Lainnya" {
                wilayah = "3"
            } else {
                wilayah = "3"
            }
            
            let valString:String = self.txtKebutuhan.text!
            let kebString:String = valString.replacingOccurrences(of: ".", with: "")
            
            let parameters = [
                "type": "KMG",
                "otr": kebString,
                "dp": "20",
                "tenor": self.txtTenor.text!,
                "asuransi": "TLO",
                "wilayah" : wilayah
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "credit_simulation"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            if status == "success" {
                                for item in json["simulation_data"].arrayValue {
                                    if item["credit_type"].stringValue == "ARREAR" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        let biaya_admin = item["biaya_admin"].stringValue
                                        let provisi = item["provisi"].stringValue
                                        let dana_diterima = item["dana_diterima"].stringValue
                                        let pengajuan_pinjaman = self.txtKebutuhan.text!
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman")
                                        prefs.set(tenor, forKey: "tenor")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama")
                                        prefs.set(angsuran, forKey: "angsuran")
                                        prefs.set(biaya_admin, forKey: "biaya_admin")
                                        prefs.set(provisi, forKey: "provisi")
                                        prefs.set(dana_diterima, forKey: "dana_diterima")
                                        prefs.set(pengajuan_pinjaman, forKey: "pengajuan_pinjaman")
                                        prefs.synchronize()
                                    } else if item["credit_type"].stringValue == "ADVANCE" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        let biaya_admin = item["biaya_admin"].stringValue
                                        let provisi = item["provisi"].stringValue
                                        let dana_diterima = item["dana_diterima"].stringValue
                                        let pengajuan_pinjaman = self.txtKebutuhan.text!
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman_advance")
                                        prefs.set(tenor, forKey: "tenor_advance")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama_advance")
                                        prefs.set(angsuran, forKey: "angsuran_advance")
                                        prefs.set(biaya_admin, forKey: "biaya_admin_advance")
                                        prefs.set(provisi, forKey: "provisi_advance")
                                        prefs.set(dana_diterima, forKey: "dana_diterima_advance")
                                        prefs.set(pengajuan_pinjaman, forKey: "pengajuan_pinjaman_advance")
                                        prefs.synchronize()
                                    }
                                }
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_simulasi_dana")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
