//
//  DataAgunan.swift
//  Andalanku
//
//  Created by Handoyo on 24/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController
import Alamofire
import SwiftyJSON

class DataAgunan: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtNoDokumen: TextField!
    @IBOutlet weak var txtDokumenSertifikat: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var scrollViewBPKB: UIScrollView!
    @IBOutlet weak var scrollViewSertifikat: UIScrollView!
    @IBOutlet weak var txtTahun: TextField!
    @IBOutlet weak var txtKendaraan: TextField!
    @IBOutlet weak var arrowKendaraan: UIImageView!
    @IBOutlet weak var txtAtasNama: TextField!
    @IBOutlet weak var txtNoPolisi: TextField!
    @IBOutlet weak var txtHarga: TextField!
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let no_dokumen = prefs.value(forKey: "no_dokumen") as? String
            if (no_dokumen != nil && no_dokumen != "") {
                self.txtNoDokumen.text = no_dokumen
            }
        })
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(DataAgunan.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoDokumen.delegate = self
        
        self.scrollViewBPKB.keyboardDismissMode = .onDrag
        self.scrollViewSertifikat.keyboardDismissMode = .onDrag
        self.scrollViewSertifikat.isHidden = true
        
        let tapArrowKendaraan = UITapGestureRecognizer(target:self, action:#selector(DataAgunan.goDownKendaraanTap(_:)))
        self.arrowKendaraan.isUserInteractionEnabled = true
        self.arrowKendaraan.addGestureRecognizer(tapArrowKendaraan)
        self.txtKendaraan.delegate = self
    }
    
    @IBAction func goTxtKendaraanBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("data_agunan", forKey: "list_used_car")
            prefs.synchronize()
            
            self.txtKendaraan.resignFirstResponder()
            let controller = ModalPilihMobilIdaman()
            let transitionDelegate = SPStorkTransitioningDelegate()
            //transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownKendaraanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("data_agunan", forKey: "list_used_car")
            prefs.synchronize()
            
            let controller = ModalPilihMobilIdaman()
            let transitionDelegate = SPStorkTransitioningDelegate()
            //transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtNoDokumenBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtNoDokumen.resignFirstResponder()
            let controller = ModalPilihDokumen()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let controller = ModalPilihDokumen()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }

    @IBAction func goSimpan(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(self.txtAtasNama.text, forKey: "atas_nama")
        prefs.set(self.txtNoPolisi.text, forKey: "no_polisi")
        prefs.set(self.txtKendaraan.text, forKey: "kendaraan")
        prefs.synchronize()
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dana")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goSimpanSertifikat(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dana")
         vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
