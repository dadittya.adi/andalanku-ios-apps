//
//  DetailJadwalAngsuran.swift
//  Andalanku
//
//  Created by Handoyo on 23/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController
import Alamofire
import SwiftyJSON

class DetailJadwalAngsuran: UIViewController, UITextFieldDelegate, UIWebViewDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtNoKontrak: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var txtSisaPinjam: TextField!
    @IBOutlet weak var txtJatuhTempo: TextField!
    //@IBOutlet weak var lblStatus: UILabel!
    var urlDownload:String = ""
    @IBOutlet weak var webPreview: UIWebView!
    
    var arrAngs = ["1","2","3","4","5"]
    var arrTanggal = ["2019-01-25","2019-02-25","2019-03-25","2019-04-25","2019-05-25"]
    var arrTagihan = ["3.616.666,67","3.616.666,67","3.616.666,67","3.616.666,67","3.616.666,67"]
    var arrPinjaman = ["172.083.333,33","169.083.333,33","167.083.333,33","162.083.333,33","158.083.333,33"]
    
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let sisa_pinjam = prefs.value(forKey: "sisa_pinjam") as? String
            if (no_kontrak != nil && no_kontrak != "") {
                self.txtNoKontrak.text = no_kontrak
                if (sisa_pinjam != nil && sisa_pinjam != "") {
                    self.txtSisaPinjam.text = "Rp. " + sisa_pinjam!
                }
                
                DispatchQueue.main.async(execute: {
                    self.load.isHidden = false
                    self.load.startAnimating()
                    
                    let prefs:UserDefaults = UserDefaults.standard
                    let api_key = prefs.value(forKey: "api_key") as? String
                    let token_number = prefs.value(forKey: "token_number") as? String
                    let id_customer = prefs.value(forKey: "id_customer") as? String
                    let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                    
                    let headers = [
                        "api_key": api_key,
                        "token_number": token_number
                    ]
                    
                    let parameters = [
                        "agreement_no": no_kontrak!,
                        "id_customer": id_customer!
                        ] as [String : Any]
                    
                    let appConfig : app_config = app_config()
                    Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                        .responseJSON { response in
                            
                            if (response.result.value == nil) {
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    if json["status"].stringValue == "success" {
                                        
                                        let dtString:String = json["maturity_date"].stringValue
                                        let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                        
                                        let dateFormatterGet = DateFormatter()
                                        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        
                                        let dateFormatterPrint = DateFormatter()
                                        dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                        
                                        if let date = dateFormatterGet.date(from: dateString) {
                                            print(dateFormatterPrint.string(from: date))
                                            self.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                        } else {
                                            self.txtJatuhTempo.text = json["maturity_date"].stringValue
                                        }
                                    }
                                }
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                    }
                })
            }
        })
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(JadwalAngsuran.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoKontrak.delegate = self
        
        self.txtNoKontrak.textColor = UIColor.white
        self.txtSisaPinjam.textColor = UIColor.white
        self.txtJatuhTempo.textColor = UIColor.white
        self.load.isHidden = true
        
        self.webPreview.scalesPageToFit = true
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "agreement_no": no_kontrak!,
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                //self.txtSisaPinjam.text = "Rp. " + json["installment_amount"].stringValue
                                self.txtJatuhTempo.text = json["maturity_date"].stringValue
                            }
                        }
                        
                    }
            }
            
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_payment_history"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                self.urlDownload = json["kartu_piutang"].stringValue
                                
                                self.webPreview.scrollView.isScrollEnabled = true
                                self.webPreview.scrollView.bounces = false
                                self.webPreview.scrollView.isPagingEnabled = true
                                self.webPreview.isUserInteractionEnabled = true
                                self.webPreview.delegate = self
                                
                                let urlfull:String = self.urlDownload
                                self.webPreview.loadRequest(URLRequest(url: URL(string: urlfull)!))
                                
                                /*
                                 let alertController = UIAlertController(title: "", message: "Simpan PDF Berhasil", preferredStyle: .alert)
                                 let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                 }
                                 alertController.addAction(OKAction)
                                 self.present(alertController, animated: true, completion:nil)
                                 */
                            } else {
                                //self.lblStatus.text = json["message"].stringValue
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }

    func webViewDidStartLoad(_ webView: UIWebView){
        self.load.isHidden = false
        self.load.startAnimating()
    }

    func webViewDidFinishLoad(_ webView: UIWebView){
        self.load.isHidden = true
        self.load.stopAnimating()
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("detail", forKey: "set_no_kontrak")
            prefs.synchronize()
            self.txtNoKontrak.resignFirstResponder()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("detail", forKey: "set_no_kontrak")
            prefs.synchronize()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DetailJadwalAngsuranCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? DetailJadwalAngsuranCell
        cell.lblAngs.text = arrAngs[indexPath.row]
        cell.lblTanggal.text = arrTanggal[indexPath.row]
        cell.lblTagihan.text = arrTagihan[indexPath.row]
        cell.lblSisa.text = arrPinjaman[indexPath.row]
        //cell.transform = .identity
        return cell as DetailJadwalAngsuranCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAngs.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let no_mobil = arrAngs[indexPath.row]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(no_mobil, forKey: "no_mobil")
        prefs.synchronize()
        */
    }
    
    @IBAction func goSimpan(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            guard let url = URL(string: self.urlDownload) else { return }
            UIApplication.shared.open(url)
            /*
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "agreement_no": no_kontrak!,
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_payment_history"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                let urlPDF:String = json["kartu_piutang"].stringValue
                                guard let url = URL(string: urlPDF) else { return }
                                UIApplication.shared.open(url)
                                
                                /*
                                let alertController = UIAlertController(title: "", message: "Simpan PDF Berhasil", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                                */
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
            */
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
