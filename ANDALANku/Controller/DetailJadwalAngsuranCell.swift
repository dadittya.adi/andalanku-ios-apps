//
//  DetailJadwalAngsuranCell.swift
//  Andalanku
//
//  Created by Handoyo on 27/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class DetailJadwalAngsuranCell: UITableViewCell {

    @IBOutlet weak var lblAngs: UILabel!
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblTagihan: UILabel!
    @IBOutlet weak var lblSisa: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
