//
//  DetailKontrak.swift
//  Andalanku
//
//  Created by Handoyo on 28/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController
import Alamofire
import SwiftyJSON

class DetailKontrak: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lineDetailUnit: UIView!
    @IBOutlet weak var lineDetailKontak: UIView!
    @IBOutlet weak var lineHistory: UIView!
    @IBOutlet weak var linePolis: UIView!
    @IBOutlet weak var txtNoKontrak: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var txtNamaLengkap: TextField!
    @IBOutlet weak var txtJatuhTempo: TextField!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    @IBOutlet weak var tabDetailUnit: UILabel!
    @IBOutlet weak var tabDetailKontrak: UILabel!
    @IBOutlet weak var tabHistoryBayar: UILabel!
    @IBOutlet weak var tabPolisAsuransi: UILabel!
    
    @IBOutlet weak var viewDetailUnit: UIView!
    @IBOutlet weak var viewDetailKontrak: UIView!
    @IBOutlet weak var viewHistoryBayar: UIView!
    @IBOutlet weak var viewPolisAsuransi: UIView!
    
    @IBOutlet weak var lblMerk: UILabel!
    @IBOutlet weak var lblTahun: UILabel!
    @IBOutlet weak var lblWarna: UILabel!
    @IBOutlet weak var lblNoPolisi: UILabel!
    @IBOutlet weak var lblNoRangka: UILabel!
    @IBOutlet weak var lblNoMesin: UILabel!
    
    @IBOutlet weak var lblOTR: UILabel!
    @IBOutlet weak var lblUangMuka: UILabel!
    @IBOutlet weak var lblAsuransi: UILabel!
    @IBOutlet weak var lblNilaiBiaya: UILabel!
    @IBOutlet weak var lblTanggalKontrak: UILabel!
    @IBOutlet weak var lblTanggalBerlaku: UILabel!
    @IBOutlet weak var lblJatuhTempo: UILabel!
    @IBOutlet weak var lblTenor: UILabel!
    
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblPolis: UILabel!
    var urlHistory:String = ""
    var urlPolis:String = ""
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            let tanggal_kredit_terakhir = prefs.value(forKey: "tanggal_kredit_terakhir") as? String
            if (no_kontrak != nil && no_kontrak != "") {
                self.txtNoKontrak.text = no_kontrak
                self.txtNamaLengkap.text = nama_akun
                if (tanggal_kredit_terakhir != nil && tanggal_kredit_terakhir != "") {
                    self.txtJatuhTempo.text = tanggal_kredit_terakhir
                }
            }
        })
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.lineDetailUnit.isHidden = false
        self.lineDetailKontak.isHidden = true
        self.linePolis.isHidden = true
        self.lineHistory.isHidden = true
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(DetailKontrak.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoKontrak.delegate = self
        
        self.viewDetailKontrak.isHidden = true
        self.viewHistoryBayar.isHidden = true
        self.viewPolisAsuransi.isHidden = true
        
        let tapDetailUnit = UITapGestureRecognizer(target:self, action:#selector(DetailKontrak.goDetailUnitTap(_:)))
        self.tabDetailUnit.isUserInteractionEnabled = true
        self.tabDetailUnit.addGestureRecognizer(tapDetailUnit)
        
        let tapDetailKontrak = UITapGestureRecognizer(target:self, action:#selector(DetailKontrak.goDetailKontrakTap(_:)))
        self.tabDetailKontrak.isUserInteractionEnabled = true
        self.tabDetailKontrak.addGestureRecognizer(tapDetailKontrak)
        
        let tapHistory = UITapGestureRecognizer(target:self, action:#selector(DetailKontrak.goHistoryTap(_:)))
        self.tabHistoryBayar.isUserInteractionEnabled = true
        self.tabHistoryBayar.addGestureRecognizer(tapHistory)
        
        let tapPolis = UITapGestureRecognizer(target:self, action:#selector(DetailKontrak.goPolisTap(_:)))
        self.tabPolisAsuransi.isUserInteractionEnabled = true
        self.tabPolisAsuransi.addGestureRecognizer(tapPolis)
        
        self.txtNoKontrak.textColor = UIColor.white
        self.txtNamaLengkap.textColor = UIColor.white
        self.txtJatuhTempo.textColor = UIColor.white
        self.load.isHidden = true
        
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "agreement_no": no_kontrak!,
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_unit"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                self.lblMerk.text = json["asset_description"].stringValue
                                self.lblTahun.text = json["manufacturing_year"].stringValue
                                self.lblWarna.text = json["colour"].stringValue
                                self.lblNoPolisi.text = json["license_plate"].stringValue
                                self.lblNoRangka.text = json["chasis_no"].stringValue
                                self.lblNoMesin.text = json["engine_no"].stringValue
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                let otString:String = json["otr"].stringValue
                                let otrString:String = otString.replacingOccurrences(of: ".", with: "")
                                
                                let formatter = NumberFormatter()
                                formatter.locale = Locale(identifier: "id_ID")
                                formatter.groupingSeparator = "."
                                formatter.numberStyle = .decimal
                                if let formattedTipAmount = formatter.string(from: Int(otrString)! as NSNumber) {
                                    self.lblOTR.text = "Rp. " + formattedTipAmount
                                }
                                
                                let dpString:String = json["down_payment"].stringValue
                                let downString:String = dpString.replacingOccurrences(of: ".", with: "")
                                
                                formatter.locale = Locale(identifier: "id_ID")
                                formatter.groupingSeparator = "."
                                formatter.numberStyle = .decimal
                                if let formattedTipAmount = formatter.string(from: Int(downString)! as NSNumber) {
                                    self.lblUangMuka.text = "Rp. " + formattedTipAmount
                                }
                                
                                self.lblAsuransi.text = "TLO"
                                
                                let netString:String = json["net_finance"].stringValue
                                let netFinString:String = netString.replacingOccurrences(of: ".", with: "")
                                
                                formatter.locale = Locale(identifier: "id_ID")
                                formatter.groupingSeparator = "."
                                formatter.numberStyle = .decimal
                                if let formattedTipAmount = formatter.string(from: Int(netFinString)! as NSNumber) {
                                    self.lblNilaiBiaya.text = "Rp. " + formattedTipAmount
                                }
                                
                                let agString:String = json["agreement_date"].stringValue
                                let agreementString:String = agString.replacingOccurrences(of: "T", with: " ")
                                
                                let dateFormatterGet = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let dateFormatterPrint = DateFormatter()
                                dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                
                                if let date = dateFormatterGet.date(from: agreementString) {
                                    self.lblTanggalKontrak.text = dateFormatterPrint.string(from: date)
                                } else {
                                    self.lblTanggalKontrak.text = json["agreement_date"].stringValue
                                }
                                
                                let efString:String = json["effective_date"].stringValue
                                let effectiveString:String = efString.replacingOccurrences(of: "T", with: " ")
                                
                                if let date = dateFormatterGet.date(from: effectiveString) {
                                    self.lblTanggalBerlaku.text = dateFormatterPrint.string(from: date)
                                } else {
                                    self.lblTanggalBerlaku.text = json["effective_date"].stringValue
                                }
                                
                                let maString:String = json["maturity_date"].stringValue
                                let maturityString:String = maString.replacingOccurrences(of: "T", with: " ")
                                
                                if let date = dateFormatterGet.date(from: maturityString) {
                                    self.lblJatuhTempo.text = dateFormatterPrint.string(from: date)
                                } else {
                                    self.lblJatuhTempo.text = json["maturity_date"].stringValue
                                }
                                
                                self.lblTenor.text = json["tenor"].stringValue + " Bulan"
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("detail_kontrak", forKey: "set_no_kontrak")
            prefs.synchronize()
            self.txtNoKontrak.resignFirstResponder()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("detail_kontrak", forKey: "set_no_kontrak")
            prefs.synchronize()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDetailUnitTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.lineDetailUnit.isHidden = false
            self.lineDetailKontak.isHidden = true
            self.lineHistory.isHidden = true
            self.linePolis.isHidden = true
            
            self.viewDetailUnit.isHidden = false
            self.viewDetailKontrak.isHidden = true
            self.viewHistoryBayar.isHidden = true
            self.viewPolisAsuransi.isHidden = true
            
            self.tabDetailUnit.textColor = UIColor.black
            self.tabDetailKontrak.textColor = UIColor(rgb: 0x4653B3)
            self.tabHistoryBayar.textColor = UIColor(rgb: 0x4653B3)
            self.tabPolisAsuransi.textColor = UIColor(rgb: 0x4653B3)
        })
    }
    
    @objc func goDetailKontrakTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.lineDetailUnit.isHidden = true
            self.lineDetailKontak.isHidden = false
            self.lineHistory.isHidden = true
            self.linePolis.isHidden = true
            
            self.viewDetailUnit.isHidden = true
            self.viewDetailKontrak.isHidden = false
            self.viewHistoryBayar.isHidden = true
            self.viewPolisAsuransi.isHidden = true
            
            self.tabDetailUnit.textColor = UIColor(rgb: 0x4653B3)
            self.tabDetailKontrak.textColor = UIColor.black
            self.tabHistoryBayar.textColor = UIColor(rgb: 0x4653B3)
            self.tabPolisAsuransi.textColor = UIColor(rgb: 0x4653B3)
        })
    }
    
    @objc func goHistoryTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.lineDetailUnit.isHidden = true
            self.lineDetailKontak.isHidden = true
            self.lineHistory.isHidden = false
            self.linePolis.isHidden = true
            
            self.viewDetailUnit.isHidden = true
            self.viewDetailKontrak.isHidden = true
            self.viewHistoryBayar.isHidden = false
            self.viewPolisAsuransi.isHidden = true
            
            self.tabDetailUnit.textColor = UIColor(rgb: 0x4653B3)
            self.tabDetailKontrak.textColor = UIColor(rgb: 0x4653B3)
            self.tabHistoryBayar.textColor = UIColor.black
            self.tabPolisAsuransi.textColor = UIColor(rgb: 0x4653B3)
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "agreement_no": no_kontrak!,
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_payment_history"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                self.urlHistory = json["kartu_piutang"].stringValue
                            } else {
                                self.lblHistory.text = json["message"].stringValue
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @objc func goPolisTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.lineDetailUnit.isHidden = true
            self.lineDetailKontak.isHidden = true
            self.lineHistory.isHidden = true
            self.linePolis.isHidden = false
            
            self.viewDetailUnit.isHidden = true
            self.viewDetailKontrak.isHidden = true
            self.viewHistoryBayar.isHidden = true
            self.viewPolisAsuransi.isHidden = false
            
            self.tabDetailUnit.textColor = UIColor(rgb: 0x4653B3)
            self.tabDetailKontrak.textColor = UIColor(rgb: 0x4653B3)
            self.tabHistoryBayar.textColor = UIColor(rgb: 0x4653B3)
            self.tabPolisAsuransi.textColor = UIColor.black
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "agreement_no": no_kontrak!,
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_insurance"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["status"].stringValue == "success" {
                                self.urlPolis = json["kartu_piutang"].stringValue
                            } else {
                                self.lblPolis.text = json["message"].stringValue
                            }
                        }
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @IBAction func goDownloadHistory(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let urlPDF:String = self.urlHistory
            guard let url = URL(string: urlPDF) else { return }
            UIApplication.shared.open(url)
        })
    }
    
    @IBAction func goDownloadPolis(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let urlPDF:String = self.urlPolis
            guard let url = URL(string: urlPDF) else { return }
            UIApplication.shared.open(url)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
