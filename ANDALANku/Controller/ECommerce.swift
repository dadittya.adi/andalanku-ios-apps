//
//  ECommerce.swift
//  Andalanku
//
//  Created by Handoyo on 16/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class ECommerce: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

}
