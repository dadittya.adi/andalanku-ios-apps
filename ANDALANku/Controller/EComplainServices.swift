//
//  EComplainServices.swift
//  Andalanku
//
//  Created by Handoyo on 12/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import DeviceKit

class EComplainServices: UIViewController {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet weak var menuBarView: EServiceTabsView!
    
    var currentIndex: Int = 0
    var tabs = ["Semua","Diajukan","Dalam Proses","Selesai"]
    var pageController: UIPageViewController!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        menuBarView.dataArray = tabs
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.collView.backgroundColor = UIColor.clear //UIColor.init(white: 0.97, alpha: 0.97)
        
        presentPageVCOnView()
        
        menuBarView.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        
        menuBarView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        pageController.setViewControllers([viewController(At: 0)!], direction: .forward, animated: true, completion: nil)
    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "page_complain") as! PageComplain
        
        let device = Device.identifier
        print(device)
        if (device == "iPhone10,3" || device == "iPhone10,6" || device == "iPhone11,2" || device == "iPhone11,4" || device == "iPhone11,6" || device == "iPhone11,8") {
            self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY + 24, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        } else if (device == "x86_64" || device == "i386") {
            self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY + 24, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        } else {
            self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        }
        
        //self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY + 24, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menuBarView.dataArray.count == 0) || (index >= self.menuBarView.dataArray.count)) {
            return nil
        }
        
        if (index == 0) {
            let Menu2 = storyboard?.instantiateViewController(withIdentifier: "story_ecomplain_semua") as! EComplainSemua
            Menu2.pageIndex = index
            currentIndex = index
            return Menu2
        } else if (index == 1) {
            let Menu1 = storyboard?.instantiateViewController(withIdentifier: "story_ecomplain_diajukan") as! EComplainDiajukan
            Menu1.pageIndex = index
            currentIndex = index
            return Menu1
        } else if (index == 2) {
            let Menu3 = storyboard?.instantiateViewController(withIdentifier: "story_ecomplain_proses") as! EComplainProses
            Menu3.pageIndex = index
            currentIndex = index
            return Menu3
        } else if (index == 3) {
            let Menu4 = storyboard?.instantiateViewController(withIdentifier: "story_ecomplain_selesai") as! EComplainSelesai
            Menu4.pageIndex = index
            currentIndex = index
            return Menu4
        } else {
            return nil
        }
    }
    
    @IBAction func goPengajuanKomplain(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pengajuan_komplain")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

extension EComplainServices: EServiceBarDelegate {
    
    func menuBarDidSelectItemAt(menu: EServiceTabsView, index: Int) {
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            } else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            
            menuBarView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
        }
        
    }
    
}

extension EComplainServices: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = 0
        
        if viewController.isKind(of: EComplainSemua.self) {
            index = (viewController as! EComplainSemua).pageIndex
        } else if (viewController.isKind(of: EComplainDiajukan.self)) {
            index = (viewController as! EComplainDiajukan).pageIndex
        } else if (viewController.isKind(of: EComplainProses.self)) {
            index = (viewController as! EComplainProses).pageIndex
        } else if (viewController.isKind(of: EComplainSelesai.self)) {
            index = (viewController as! EComplainSelesai).pageIndex
        }
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewController(At: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = 0
        
        if viewController.isKind(of: EComplainSemua.self) {
            index = (viewController as! EComplainSemua).pageIndex
        } else if (viewController.isKind(of: EComplainDiajukan.self)) {
            index = (viewController as! EComplainDiajukan).pageIndex
        } else if (viewController.isKind(of: EComplainProses.self)) {
            index = (viewController as! EComplainProses).pageIndex
        } else if (viewController.isKind(of: EComplainSelesai.self)) {
            index = (viewController as! EComplainSelesai).pageIndex
        }
        
        if (index == tabs.count) || (index == NSNotFound) {
            return nil
        }
        
        index += 1
        return self.viewController(At: index)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                if (pageViewController.viewControllers?.first?.isKind(of: EComplainSemua.self))! {
                    let cvc = pageViewController.viewControllers!.first as! EComplainSemua
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else if (pageViewController.viewControllers?.first?.isKind(of: EComplainDiajukan.self))! {
                    let cvc = pageViewController.viewControllers!.first as! EComplainDiajukan
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else if (pageViewController.viewControllers?.first?.isKind(of: EComplainProses.self))! {
                    let cvc = pageViewController.viewControllers!.first as! EComplainProses
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else if (pageViewController.viewControllers?.first?.isKind(of: EComplainSelesai.self))! {
                    let cvc = pageViewController.viewControllers!.first as! EComplainSelesai
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
                
            }
        }
        
    }
    
}
