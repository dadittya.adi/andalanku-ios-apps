//
//  ERequestDiajukanCell.swift
//  Andalanku
//
//  Created by Handoyo on 01/07/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class ERequestDiajukanCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewCompain: UIView!
    @IBOutlet weak var lblTicket: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCompain.layer.shadowColor = UIColor.lightGray.cgColor
        viewCompain.layer.shadowOpacity = 0.3
        viewCompain.layer.shadowOffset = CGSize.zero
        viewCompain.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
