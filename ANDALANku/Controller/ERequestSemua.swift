//
//  ERequestSemua.swift
//  Andalanku
//
//  Created by Handoyo on 12/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ERequestSemua: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var pageIndex:Int = 0
    @IBOutlet var tvJSON: UITableView!
    //var arrMobilLabel = ["1. Marketing","2. Finance","3. Development","4. Hukum"]
    var arrMobilLabel = [String]()
    var arrMobilImage: [UIImage] = [
        UIImage(named: "img_idaman1")!,
        UIImage(named: "img_idaman2")!,
        UIImage(named: "img_idaman3")!,
        UIImage(named: "img_idaman4")!
    ]
    
    @IBOutlet var load: UIActivityIndicatorView!
    
    var arrMedia = [String]()
    var arrDict :NSMutableArray=[]
    var par_num = 0
    var limit = 4
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.isHidden = true
        self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        self.tvJSON.tableFooterView = UIView(frame: .zero)
        self.load.isHidden = true
        
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!,
                "status": ""
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "collateral_request_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if json["insurance_claim_list"].arrayObject != nil {
                                self.arrDict.removeAllObjects()
                                if let past : NSArray = json["insurance_claim_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["insurance_claim_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["insurance_claim_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                
                                if self.arrDict.count == 0 {
                                    self.tvJSON.isHidden = true
                                    self.scrollView.isHidden = false
                                } else {
                                    self.tvJSON.isHidden = false
                                    self.scrollView.isHidden = true
                                    self.tvJSON.reloadData()
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    }
            }
        })
    }
    
    @objc func loadBottom() {
        /*
         DispatchQueue.main.async(execute: {
         self.load.isHidden = false
         self.load.startAnimating()
         
         let prefs:UserDefaults = UserDefaults.standard
         let api_key = prefs.value(forKey: "api_key") as? String
         let token_number = prefs.value(forKey: "token_number") as? String
         
         let headers = [
         "api_key": api_key,
         "token_number": token_number
         ]
         
         let parameters = [
         "keyword": "",
         "page": self.par_num
         ] as [String : Any]
         
         let appConfig : app_config = app_config()
         Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
         .responseJSON { response in
         
         if (response.result.value == nil) {
         self.load.isHidden = true
         self.load.stopAnimating()
         let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
         }
         alertController.addAction(OKAction)
         self.present(alertController, animated: true, completion:nil)
         } else {
         
         if let value = response.result.value {
         let json = JSON(value)
         if json["promo_list"].arrayObject != nil {
         if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
         print(past)
         for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
         {
         self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
         }
         }
         self.par_num = self.par_num + self.limit
         self.tvJSON.reloadData()
         
         self.load.isHidden = true
         self.load.stopAnimating()
         }
         }
         }
         }
         })
         */
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ERequestSemuaCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ERequestSemuaCell
        let agreement_number : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "agreement_number") as! NSString
        cell.lblTitle.text = agreement_number as String
        
        let request_date : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "request_date") as! NSString
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: request_date as String) {
            print(dateFormatterPrint.string(from: date))
            cell.lblTanggal.text = dateFormatterPrint.string(from: date)
        } else {
            cell.lblTanggal.text = request_date as String
        }
        
        let status : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "status") as! NSString
        cell.lblStatus.text = "Status : " + (status as String) as String
        
        let ticket_number : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "ticket_number") as! NSString
        cell.lblTicket.text = ticket_number as String
        return cell as ERequestSemuaCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            //let json = JSON(self.arrDict[indexPath.row])
            //var i:Int = 0
            self.arrMedia.removeAll()
            /*
            for item in json["media_list"].arrayValue {
                print(item)
                self.arrMedia.append(json["media_list"][i].string!)
                i = i + 1
            }
            */
            
            let ticket_number : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "ticket_number") as! NSString
            let submitted_date : NSString=(self.arrDict[indexPath.row] as AnyObject).value(forKey: "request_date") as! NSString
            let category : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "agreement_number") as! NSString
            let status : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "status") as! NSString
            
            var msg:String = ""
            if let message : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "ready_date") as? NSString {
                msg = message as String
            } else {
                msg = ""
            }
            
            var ph:String = ""
            if let phone : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "form_number") as? NSString {
                ph = phone as String
            } else {
                ph = ""
            }
            
            var set_solution = String()
            if let solution:NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "customer_name") as? NSString {
                set_solution = solution as String
            } else {
                set_solution = ""
            }
            
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(ph, forKey: "phone")
            prefs.set(ticket_number as String, forKey: "ticket_number")
            prefs.set(submitted_date as String, forKey: "submitted_date")
            prefs.set(category as String, forKey: "category")
            prefs.set(status as String, forKey: "status")
            prefs.set(msg, forKey: "message")
            prefs.set(set_solution, forKey: "set_solution")
            prefs.set(self.arrMedia, forKey: "arrMedia")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_ambil_jaminan_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goAjukan(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_ambil_jaminan")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
