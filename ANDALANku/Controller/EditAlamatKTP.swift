//
//  EditAlamatKTP.swift
//  Andalanku
//
//  Created by Handoyo on 21/06/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import SPStorkController

class EditAlamatKTP: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var txtZipCode: TextField!
    @IBOutlet weak var txtAddress: TextField!
    @IBOutlet weak var txtRT: TextField!
    @IBOutlet weak var txtRW: TextField!
    
    @IBOutlet weak var imgKTP: UIImageView!
    
    @IBOutlet weak var txtProvince: TextField!
    @IBOutlet weak var arrowProvince: UIImageView!
    var idProvince:String = ""
    
    @IBOutlet weak var txtCity: TextField!
    @IBOutlet weak var arrowCity: UIImageView!
    var idCity:String = ""
    
    @IBOutlet weak var txtDistrict: TextField!
    @IBOutlet weak var arrowDistrict: UIImageView!
    var idDistrict:String = ""
    
    @IBOutlet weak var txtVillage: TextField!
    @IBOutlet weak var arrowVillage: UIImageView!
    var idVillage:String = ""
    @IBOutlet weak var lblUpload: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var checkSetuju: CCheckbox!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.scrollView.keyboardDismissMode = .onDrag
        
        let tapImgProvince = UITapGestureRecognizer(target:self, action:#selector(EditAlamatKTP.goProvinceTap(_:)))
        self.arrowProvince.isUserInteractionEnabled = true
        self.arrowProvince.addGestureRecognizer(tapImgProvince)
        self.txtProvince.delegate = self
        
        let tapImgCity = UITapGestureRecognizer(target:self, action:#selector(EditAlamatKTP.goCityTap(_:)))
        self.arrowCity.isUserInteractionEnabled = true
        self.arrowCity.addGestureRecognizer(tapImgCity)
        self.txtCity.delegate = self
        
        let tapImgDistrict = UITapGestureRecognizer(target:self, action:#selector(EditAlamatKTP.goDistrictTap(_:)))
        self.arrowDistrict.isUserInteractionEnabled = true
        self.arrowDistrict.addGestureRecognizer(tapImgDistrict)
        self.txtDistrict.delegate = self
        
        let tapImgVillage = UITapGestureRecognizer(target:self, action:#selector(EditAlamatKTP.goVillageTap(_:)))
        self.arrowVillage.isUserInteractionEnabled = true
        self.arrowVillage.addGestureRecognizer(tapImgVillage)
        self.txtVillage.delegate = self
        
        let tapImgUpload = UITapGestureRecognizer(target:self, action:#selector(EditAlamatKTP.imageTapped(_:)))
        self.lblUpload.isUserInteractionEnabled = true
        self.lblUpload.addGestureRecognizer(tapImgUpload)
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set("sesuai_ktp", forKey: "set_formulir")
        prefs.synchronize()
        
        let province_name = prefs.value(forKey: "province_name_legal") as? String
        let city_name = prefs.value(forKey: "city_name_legal") as? String
        let district_name = prefs.value(forKey: "district_name_legal") as? String
        let village_name = prefs.value(forKey: "village_name_legal") as? String
        
        let zip_code = prefs.value(forKey: "zip_code_legal") as? String
        let address = prefs.value(forKey: "address_legal") as? String
        let rt = prefs.value(forKey: "rt_legal") as? String
        let rw = prefs.value(forKey: "rw_legal") as? String
        
        self.txtProvince.text = province_name!
        self.txtCity.text = city_name!
        self.txtDistrict.text = district_name!
        self.txtVillage.text = village_name!
        
        self.txtZipCode.text = zip_code!
        self.txtAddress.text = address!
        self.txtRT.text = rt!
        self.txtRW.text = rw!
        
        self.load.isHidden = true
    }
    
    @IBAction func goTxtProvinceBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtProvince.resignFirstResponder()
            let controller = ModalProvince()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goProvinceTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let controller = ModalProvince()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtCityBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtCity.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let province = prefs.value(forKey: "province") as? String
            if (province != nil && province != "") {
                let controller = ModalCity()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goCityTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let province = prefs.value(forKey: "province") as? String
            if (province != nil && province != "") {
                let controller = ModalCity()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goTxtDistrictBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtDistrict.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let city = prefs.value(forKey: "city") as? String
            if (city != nil && city != "") {
                let controller = ModalDistrict()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goDistrictTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let city = prefs.value(forKey: "city") as? String
            if (city != nil && city != "") {
                let controller = ModalDistrict()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goTxtVillageBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtVillage.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let district = prefs.value(forKey: "district") as? String
            if (district != nil && district != "") {
                let controller = ModalVillage()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goVillageTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let district = prefs.value(forKey: "district") as? String
            if (district != nil && district != "") {
                let controller = ModalVillage()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        DispatchQueue.main.async(execute: {
            self.imgKTP.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    @IBAction func goSimpan(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            if self.checkSetuju.isCheckboxSelected == true {
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(self.txtRT.text, forKey: "rt")
                prefs.set(self.txtRW.text, forKey: "rw")
                prefs.set(self.txtAddress.text, forKey: "alamat")
                prefs.set(self.txtZipCode.text, forKey: "kodePos")
                prefs.set("LEGAL", forKey: "typeAddress")
                prefs.synchronize()
                self.dismiss(animated: true, completion: nil)
            } else {
                let alertController = UIAlertController(title: "", message: "Please Accept Agreement ANDALAN Finance", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
