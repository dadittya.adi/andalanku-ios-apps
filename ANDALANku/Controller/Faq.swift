//
//  Faq.swift
//  Andalanku
//
//  Created by Handoyo on 03/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Faq: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet var load: UIActivityIndicatorView!
    var arrDict :NSMutableArray=[]
    @IBOutlet weak var tvJSON: UITableView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navBar.shadowImage = UIImage()
        self.load.isHidden = true
        self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "keyword": ""
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "faq_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["faq_list"].arrayObject != nil {
                                self.arrDict.removeAllObjects()
                                if let past : NSArray = json["faq_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["faq_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["faq_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.tvJSON.reloadData()
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : FaqCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? FaqCell
        
        let question : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "question") as! NSString
        cell.lblFaq.text = question as String
        
        return cell as FaqCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            //self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            let json = JSON(self.arrDict[indexPath.row])
            let question = json["question"].stringValue
            
            var strAnswer:String = ""
            for item in json["faq_answer_list"].arrayValue {
                if strAnswer == "" {
                    strAnswer = " • " + item["description"].stringValue
                } else {
                    strAnswer = strAnswer + "\n • " + item["description"].stringValue
                }
            }
            
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(question, forKey: "faq_question")
            prefs.set(strAnswer, forKey: "faq_answer")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_faq_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
