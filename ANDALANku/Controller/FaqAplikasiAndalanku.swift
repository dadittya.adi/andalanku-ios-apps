//
//  FaqAplikasiAndalanku.swift
//  Andalanku
//
//  Created by Macintosh on 03/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class FaqAplikasiAndalanku: UIViewController {

    @IBOutlet weak var lblDanaContent: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let text = """
            Mengapa harus download aplikasi ANDALANKU ?
            Aplikasi ANDALANKU didedikasikan untuk anda untuk memberikan kemudahan dalam mengajukan kredit multiguna Dana Andalanku maupun pembelian kendaraan baik mobil baru maupun mobil bekas. Anda juga bisa menikmati fitur-fitur lain yang mudah digunakan serta amat membantu kami untuk memberikan pelayanan yang cepat.

            Apa itu Plafond Kredit ?
            Plafond kredit merupakan fasilitas yang diberikan kepada anda berupa nilai maksimum pinjaman yang dapat dicairkan atau digunakan untuk berbagai kebutuhan finansial.

            Berapa batas plafond kredit yang diberikan Dana Andalanku ?
            Plafond kredit maksimal bisa anda dapatkan dengan cara mengajukan di aplikasi ANDALANKU hanya melalui beberapa langkah mudah yaitu :
            Mendaftarkan diri sebagai member ANDALANKU
            Mengajukan Plafond Kredit dari menu utama ANDALANKU
            Proses survey oleh PIC ANDALANKU
            Mendapatkan informasi plafond kredit
            Atau anda dapat mengetahui nilai plafond sementara dengan melakukan simulasi plafond kredit di menu "simulasi"

            Apa itu kode referral dan apa fungsinya ?
            Kode referral adalah kode yang dimiliki oleh semua pengguna apikasi ANDALANKU. Anda bisa membagikan kode referral kepada teman atau kerabat anda dan apabila teman dan kerabat anda mengunduh dan mendaftar sebagai member ANDALANKU maka anda akan mendapatkan point yang bisa ditukar dengan berbagai hadiah.

            Bagaimana kalau saya lupa password ?
            Anda cukup memilih "lupa password" pada menu login, kemudian sistem kami akan mengirimkan kode verifikasi ke ponsel anda sesuai dengan nomor telepon yang didaftarkan pertama kali. Jika data sudah sesuai dan sukses silahkan masukkan password yang baru.
            """
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        lblDanaContent.attributedText = NSAttributedString(attributedListString: attributedText, withFont: lblDanaContent.font)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
