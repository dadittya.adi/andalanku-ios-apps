//
//  FaqCaraPembayaranAngsuran.swift
//  Andalanku
//
//  Created by Macintosh on 04/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class FaqCaraPembayaranAngsuran: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tvJSON: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    var arrMobilLabel = ["1. SETOR TUNAI DI KANTOR CABANG PT. AFI","2. PEMBAYARAN TUNAI MELALUI INDOMARET","3. PEMBAYARAN VIA PICK UP COLLECTOR","4. PEMBAYARAN VIA BCA VIRTUAL ACCOUNT"]
    var arrMobilImage: [UIImage] = [
        UIImage(named: "img_idaman1")!,
        UIImage(named: "img_idaman2")!,
        UIImage(named: "img_idaman3")!,
        UIImage(named: "img_idaman4")!
    ]
    
    @IBOutlet var load: UIActivityIndicatorView!
    
    var arrDict :NSMutableArray=[]
    var par_num = 0
    var limit = 4
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        self.tvJSON.tableFooterView = UIView(frame: .zero)
        self.load.isHidden = true
        
        /*
         DispatchQueue.main.async(execute: {
         
         self.load.isHidden = false
         self.load.startAnimating()
         
         let prefs:UserDefaults = UserDefaults.standard
         let api_key = prefs.value(forKey: "api_key") as? String
         let token_number = prefs.value(forKey: "token_number") as? String
         
         let headers = [
         "api_key": api_key,
         "token_number": token_number
         ]
         
         let parameters = [
         "keyword": "",
         "page": 1
         ] as [String : Any]
         
         let appConfig : app_config = app_config()
         Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
         .responseJSON { response in
         
         if (response.result.value == nil) {
         self.load.isHidden = true
         self.load.stopAnimating()
         let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
         }
         alertController.addAction(OKAction)
         self.present(alertController, animated: true, completion:nil)
         } else {
         
         if let value = response.result.value {
         let json = JSON(value)
         print(json)
         if json["promo_list"].arrayObject != nil {
         self.arrDict.removeAllObjects()
         if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
         print(past)
         for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
         {
         self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
         }
         }
         self.par_num = self.par_num + self.limit
         self.tvJSON.reloadData()
         
         self.load.isHidden = true
         self.load.stopAnimating()
         }
         }
         }
         }
         })
         */
    }
    
    @objc func loadBottom() {
        /*
         DispatchQueue.main.async(execute: {
         self.load.isHidden = false
         self.load.startAnimating()
         
         let prefs:UserDefaults = UserDefaults.standard
         let api_key = prefs.value(forKey: "api_key") as? String
         let token_number = prefs.value(forKey: "token_number") as? String
         
         let headers = [
         "api_key": api_key,
         "token_number": token_number
         ]
         
         let parameters = [
         "keyword": "",
         "page": self.par_num
         ] as [String : Any]
         
         let appConfig : app_config = app_config()
         Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
         .responseJSON { response in
         
         if (response.result.value == nil) {
         self.load.isHidden = true
         self.load.stopAnimating()
         let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
         }
         alertController.addAction(OKAction)
         self.present(alertController, animated: true, completion:nil)
         } else {
         
         if let value = response.result.value {
         let json = JSON(value)
         if json["promo_list"].arrayObject != nil {
         if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
         print(past)
         for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
         {
         self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
         }
         }
         self.par_num = self.par_num + self.limit
         self.tvJSON.reloadData()
         
         self.load.isHidden = true
         self.load.stopAnimating()
         }
         }
         }
         }
         })
         */
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMobilLabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : EComplainSemuaCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? EComplainSemuaCell
        /*
         let promo_tanggal : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "promo_date") as! NSString
         cell.lblTitle.text = promo_tanggal as String
         
         let promo_image : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
         cell.downloadImage(URL(string: promo_image as String)!)
         */
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: arrMobilLabel[indexPath.row], attributes: [.paragraphStyle: lineHeightStyle])
        cell.lblTitle.attributedText = NSAttributedString(attributedListString: attributedText, withFont: cell.lblTitle.font)
        
        return cell as EComplainSemuaCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_komplain_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            /*
             let promo_image : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
             let promo_tanggal : NSString=(self.arrDict[indexPath.row] as AnyObject).value(forKey: "promo_date") as! NSString
             let content_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
             let title_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
             let prefs:UserDefaults = UserDefaults.standard
             prefs.set(promo_image as String, forKey: "imgPromo")
             prefs.set(content_promo as String, forKey: "contentPromo")
             prefs.set(title_promo as String, forKey: "titlePromo")
             prefs.set(promo_tanggal as String, forKey: "tanggalPromo")
             prefs.synchronize()
             let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo_detail")
             vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: true, completion: nil)
             */
        })
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
