//
//  FaqCell.swift
//  Andalanku
//
//  Created by Handoyo on 16/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class FaqCell: UITableViewCell {

    @IBOutlet weak var lblFaq: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
