//
//  FaqDanaAndalanku.swift
//  Andalanku
//
//  Created by Macintosh on 03/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class FaqDanaAndalanku: UIViewController {

    @IBOutlet weak var lblDanaContent: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navBar.shadowImage = UIImage()
        
        let text = """
            Apa itu Dana Andalanku ?
            Dana Andalanku adalah salah satu produk unggulan Andalan Finance yang memberikan solusi terbaik dalam kebutuhan pembiayaan untuk pengadaan barang dan / atau jasa yang diperlukan oleh debitur dalam jangka waktu yang diperjanjikan yang berbasis jaminan BPKB kendaraan.

            Untuk Apa Saja Kegunaan Dana Andalanku ?
            Pembiayaan Dana Andalanku dapat digunakan untuk tujuan sebagai berikut :
            a. Untuk Biaya Renovasi Rumah
            b. Untuk Biaya Pendidikan
            c. Untuk Biaya Kesehatan
            d. Untuk Biaya Perjalanan Wisata
            e. Dan untuk biaya konsumtif lainnya

            Apa Saja Dokumen Persyaratan Pengajuan Dana Andalanku ?
            Dokumen persyaratan umum Pembiayaan Multiguna adalah sebagai berikut :
            1. FC Kartu Identitas Pemohon dan Pasangan
            2. FC NPWP Pemohon Pembiayaan
            3. FC Kartu Keluarga / Buku Nikah / Akta Nikah
            4. FC Bukti Penghasilan / Keuangan (min. 6 bulan terakhir)
            5. FC Kepemilikan Rumah (PBB / Rek. Listrik / Rek. Telepon / Rek. Air)
            6. Dokumen lain yang diperlukan
            """
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        lblDanaContent.attributedText = NSAttributedString(attributedListString: attributedText, withFont: lblDanaContent.font)
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
