//
//  FaqDetail.swift
//  Andalanku
//
//  Created by Handoyo on 16/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SwiftyJSON

class FaqDetail: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        let prefs:UserDefaults = UserDefaults.standard
        let faq_question = prefs.value(forKey: "faq_question") as? String
        let faq_answer = prefs.value(forKey: "faq_answer") as? String
        self.lblQuestion.text = faq_question!
        
        let text = faq_answer!
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        self.lblAnswer.attributedText = NSAttributedString(attributedListString: attributedText, withFont: self.lblAnswer.font)
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
