//
//  FormulirAplikasiPembiayaan.swift
//  Andalanku
//
//  Created by Handoyo on 19/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import Material
import SwiftyJSON
import SPStorkController

class FormulirAplikasiPembiayaan: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtOccupation: TextField!
    @IBOutlet weak var arrowOccupation: UIImageView!
    @IBOutlet weak var txtTanggalLahir: TextField!
    @IBOutlet weak var txtNamaLengkap: TextField!
    @IBOutlet weak var txtNoKTP: TextField!
    @IBOutlet weak var txtTempatLahir: TextField!
    @IBOutlet weak var txtIbuKandung: TextField!
    @IBOutlet weak var txtTelpRumah: TextField!
    @IBOutlet weak var txtHP: TextField!
    @IBOutlet weak var txtKodePos: TextField!
    @IBOutlet weak var txtAlamat: TextField!
    @IBOutlet weak var txtRT: TextField!
    @IBOutlet weak var txtRW: TextField!
    @IBOutlet weak var txtPenghasilanPerBulan: TextField!
    @IBOutlet weak var txtPenghasilanTambahan: TextField!
    @IBOutlet weak var txtSumberPenghasilan: TextField!
    
    @IBOutlet weak var txtTimeSurvey: TextField!
    @IBOutlet weak var txtTanggalSurvey: TextField!
    @IBOutlet weak var txtProvince: TextField!
    @IBOutlet weak var arrowProvince: UIImageView!
    var idProvince:String = ""
    
    @IBOutlet weak var txtCity: TextField!
    @IBOutlet weak var arrowCity: UIImageView!
    var idCity:String = ""
    
    @IBOutlet weak var txtDistrict: TextField!
    @IBOutlet weak var arrowDistrict: UIImageView!
    var idDistrict:String = ""
    
    @IBOutlet weak var txtVillage: TextField!
    @IBOutlet weak var arrowVillage: UIImageView!
    var idVillage:String = ""
    
    @IBOutlet weak var txtWilayah: TextField!
    @IBOutlet weak var arrowWilayah: UIImageView!
    var idWilayah:String = ""
    
    var datePicker: UIDatePicker!
    var datePickerSurvey: UIDatePicker!
    var timePicker: UIDatePicker!
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    @IBOutlet weak var checkReferensi: CCheckbox!
    @IBOutlet weak var checkSetuju: CCheckbox!
    
    var tempNamaLengkap:String = ""
    var tempTempatLahir:String = ""
    var tempTanggalLahir:String = ""
    var tempRT:String = ""
    var tempRW:String = ""
    var tempKodePos:String = ""
    var tempIbuKandung:String = ""
    var tempPenghasilanTambahan:String = ""
    var tempPenghasilanPerBulan:String = ""
    var tempSumberPenghasilan:String = ""
    var tempTelpRumah:String = ""
    var tempHP:String = ""
    var tempOccupation:String = ""
    var tempProvince:String = ""
    var tempDistrict:String = ""
    var tempCity:String = ""
    var tempVillage:String = ""
    var tempAlamat:String = ""
    var tempIDNumber:String = ""
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFormulirTitle: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        
        let tapImgOccupation = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goOccupTap(_:)))
        self.arrowOccupation.isUserInteractionEnabled = true
        self.arrowOccupation.addGestureRecognizer(tapImgOccupation)
        self.txtOccupation.delegate = self
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set("pembiayaan", forKey: "set_formulir")
        prefs.synchronize()
        
        let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
        if (set_pembiayaan == "plafond") {
            self.lblTitle.text = "Pengajuan Plafond"
            self.lblFormulirTitle.text = "Formulir Pengajuan Plafond"
            self.checkReferensi.isHidden = true
        }
        
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        let ktp = prefs.value(forKey: "ktp") as? String
        
        if (nama_akun != nil && nama_akun != "") {
            self.txtNamaLengkap.text = nama_akun
        }
        
        if ktp != nil {
            self.txtNoKTP.text = ktp
        }
        
        let tapImgProvince = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goProvinceTap(_:)))
        self.arrowProvince.isUserInteractionEnabled = true
        self.arrowProvince.addGestureRecognizer(tapImgProvince)
        self.txtProvince.delegate = self
        
        let tapImgCity = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goCityTap(_:)))
        self.arrowCity.isUserInteractionEnabled = true
        self.arrowCity.addGestureRecognizer(tapImgCity)
        self.txtCity.delegate = self
        
        let tapImgDistrict = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goDistrictTap(_:)))
        self.arrowDistrict.isUserInteractionEnabled = true
        self.arrowDistrict.addGestureRecognizer(tapImgDistrict)
        self.txtDistrict.delegate = self
        
        let tapImgVillage = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goVillageTap(_:)))
        self.arrowVillage.isUserInteractionEnabled = true
        self.arrowVillage.addGestureRecognizer(tapImgVillage)
        self.txtVillage.delegate = self
        
        let tapImgWilayah = UITapGestureRecognizer(target:self, action:#selector(FormulirAplikasiPembiayaan.goWilayahTap(_:)))
        self.arrowWilayah.isUserInteractionEnabled = true
        self.arrowWilayah.addGestureRecognizer(tapImgWilayah)
        self.txtWilayah.delegate = self
        
        self.datePicker = UIDatePicker()
        self.datePickerSurvey = UIDatePicker()
        self.timePicker = UIDatePicker()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        timeFormatter.dateFormat = "HH:mm:ss"
        
        let dateA = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))
        let timeA = timeFormatter.date(from: timeFormatter.string(from: timePicker.date))
        let dateSurvey = dateFormatter.date(from: dateFormatter.string(from: datePickerSurvey.date))
        
        let dateBirth = dateA?.addingTimeInterval(60 * 90 * 24 * 1)
        self.datePicker.date = dateBirth!
        self.datePicker.datePickerMode = .date
        
        self.datePickerSurvey.date = dateSurvey!
        self.datePickerSurvey.datePickerMode = .date
        
        self.timePicker.date = timeA! //timeBirth!
        self.timePicker.datePickerMode = .time
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(rgb: 0x5DC4F8)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(FormulirAplikasiPembiayaan.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(FormulirAplikasiPembiayaan.donePicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.txtTanggalLahir.inputView = self.datePicker
        self.txtTanggalLahir.inputAccessoryView = toolBar
        
        self.txtTanggalSurvey.inputView = self.datePickerSurvey
        self.txtTanggalSurvey.inputAccessoryView = toolBar
        
        self.txtTimeSurvey.inputView = self.timePicker
        self.txtTimeSurvey.inputAccessoryView = toolBar
        
        self.datePicker.addTarget(self, action: #selector(FormulirAplikasiPembiayaan.handleDatePicker(_:)), for: UIControl.Event.valueChanged)
        
        self.datePickerSurvey.addTarget(self, action: #selector(FormulirAplikasiPembiayaan.surveyDatePicker(_:)), for: UIControl.Event.valueChanged)
        
        self.timePicker.addTarget(self, action: #selector(FormulirAplikasiPembiayaan.surveyTimePicker(_:)), for: UIControl.Event.valueChanged)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "personal_information"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            self.tempIDNumber = json["id_number"].stringValue
                            self.tempNamaLengkap = json["customer_name"].stringValue
                            self.tempTempatLahir = json["place_of_birth"].stringValue
                            self.tempTanggalLahir = json["date_of_birth"].stringValue
                            self.tempRT = json["rt_legal"].stringValue
                            self.tempRW = json["rw_legal"].stringValue
                            self.tempKodePos = json["zip_code_legal"].stringValue
                            self.tempIbuKandung = json["mother_name"].stringValue
                            self.tempPenghasilanTambahan = json["additional_income"].stringValue
                            self.tempPenghasilanPerBulan = json["monthly_income"].stringValue
                            self.tempSumberPenghasilan = json["side_job"].stringValue
                            self.tempTelpRumah = json["phone_number"].stringValue
                            self.tempHP = json["phone_number"].stringValue
                            self.tempOccupation = "" //json["occupation"].stringValue
                            self.tempProvince = json["province_name_legal"].stringValue
                            self.tempDistrict = json["district_name_legal"].stringValue
                            self.tempCity = json["city_name_legal"].stringValue
                            self.tempVillage = json["village_name_legal"].stringValue
                            self.tempAlamat = json["address_legal"].stringValue
                            
                            prefs.set(json["province_id_legal"].stringValue, forKey: "id_province")
                            prefs.set(json["city_id_legal"].stringValue, forKey: "id_city")
                            prefs.set(json["district_id_legal"].stringValue, forKey: "id_district")
                            prefs.set(json["village_id_legal"].stringValue, forKey: "id_village")
                            
                            self.txtNoKTP.text = self.tempIDNumber
                            self.txtNamaLengkap.text = self.tempNamaLengkap
                            self.txtTempatLahir.text = self.tempTempatLahir
                            self.txtTanggalLahir.text = self.tempTanggalLahir
                            self.txtRT.text = self.tempRT
                            self.txtRW.text = self.tempRW
                            self.txtKodePos.text = self.tempKodePos
                            self.txtIbuKandung.text = self.tempIbuKandung
                            
                            let valString:String = self.tempPenghasilanTambahan
                            let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
                            
                            let formatter = NumberFormatter()
                            formatter.locale = Locale(identifier: "id_ID")
                            formatter.groupingSeparator = "."
                            formatter.numberStyle = .decimal
                            if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
                                self.txtPenghasilanTambahan.text = formattedTipAmount
                            }
                            
                            let tmbString:String = self.tempPenghasilanPerBulan
                            let tambahanString:String = tmbString.replacingOccurrences(of: ".", with: "")
                            
                            formatter.locale = Locale(identifier: "id_ID")
                            formatter.groupingSeparator = "."
                            formatter.numberStyle = .decimal
                            if let formattedTipAmount = formatter.string(from: Int(tambahanString)! as NSNumber) {
                                self.txtPenghasilanPerBulan.text = formattedTipAmount
                            }
                            
                            self.txtSumberPenghasilan.text = self.tempSumberPenghasilan
                            self.txtTelpRumah.text = self.tempTelpRumah
                            self.txtHP.text = self.tempTelpRumah
                            self.txtOccupation.text = self.tempOccupation
                            self.txtProvince.text = self.tempProvince
                            self.txtDistrict.text = self.tempDistrict
                            self.txtCity.text = self.tempCity
                            self.txtVillage.text = self.tempVillage
                            self.txtAlamat.text = self.tempAlamat
                            
                            /*
                            for item in json["car_type_list"].arrayValue {
                                var i:Int = 0
                                if id_car_type == item["id_car_type"].stringValue {
                                    for item2 in item["car_type_image"].arrayValue {
                                        print(item2)
                                        self.imgSourceArray.append(item["car_type_image"][i].string!)
                                        i = i + 1
                                    }
                                }
                                self.titleArray.append(item["name"].stringValue)
                            }
                            */
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goCheckReferensi(_ sender: Any) {
        if checkReferensi.isCheckboxSelected == false {
            self.txtNoKTP.text = ""
            self.txtNamaLengkap.text = ""
            self.txtTempatLahir.text = ""
            self.txtTanggalLahir.text = ""
            self.txtRT.text = ""
            self.txtRW.text = ""
            self.txtKodePos.text = ""
            self.txtIbuKandung.text = ""
            self.txtPenghasilanTambahan.text = ""
            self.txtPenghasilanPerBulan.text = ""
            self.txtSumberPenghasilan.text = ""
            self.txtTelpRumah.text = ""
            self.txtHP.text = ""
            self.txtOccupation.text = ""
            self.txtProvince.text = ""
            self.txtDistrict.text = ""
            self.txtCity.text = ""
            self.txtVillage.text = ""
            self.txtAlamat.text = ""
        } else {
            self.txtNoKTP.text = self.tempIDNumber
            self.txtNamaLengkap.text = self.tempNamaLengkap
            self.txtTempatLahir.text = self.tempTempatLahir
            self.txtTanggalLahir.text = self.tempTanggalLahir
            self.txtRT.text = self.tempRT
            self.txtRW.text = self.tempRW
            self.txtKodePos.text = self.tempKodePos
            self.txtIbuKandung.text = self.tempIbuKandung
            
            let valString:String = self.tempPenghasilanTambahan
            let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
            
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "id_ID")
            formatter.groupingSeparator = "."
            formatter.numberStyle = .decimal
            if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
                self.txtPenghasilanTambahan.text = formattedTipAmount
            }
            
            let tmbString:String = self.tempPenghasilanPerBulan
            let tambahanString:String = tmbString.replacingOccurrences(of: ".", with: "")
            
            formatter.locale = Locale(identifier: "id_ID")
            formatter.groupingSeparator = "."
            formatter.numberStyle = .decimal
            if let formattedTipAmount = formatter.string(from: Int(tambahanString)! as NSNumber) {
                self.txtPenghasilanPerBulan.text = formattedTipAmount
            }
            
            self.txtSumberPenghasilan.text = self.tempSumberPenghasilan
            self.txtTelpRumah.text = self.tempTelpRumah
            self.txtHP.text = self.tempTelpRumah
            self.txtOccupation.text = self.tempOccupation
            self.txtProvince.text = self.tempProvince
            self.txtDistrict.text = self.tempDistrict
            self.txtCity.text = self.tempCity
            self.txtVillage.text = self.tempVillage
            self.txtAlamat.text = self.tempAlamat
        }
    }
    
    @IBAction func goTxtPenghasilanPerBulanChange(_ sender: Any) {
        let tmbString:String = self.txtPenghasilanPerBulan.text!
        let tambahanString:String = tmbString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(tambahanString)! as NSNumber) {
            self.txtPenghasilanPerBulan.text = formattedTipAmount
        }
    }
    
    @IBAction func goTxtPenghasilanTambahanChange(_ sender: Any) {
        let valString:String = self.txtPenghasilanTambahan.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtPenghasilanTambahan.text = formattedTipAmount
        }
    }
    
    @IBAction func goTxtProvinceBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtProvince.resignFirstResponder()
            let controller = ModalProvince()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goProvinceTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let controller = ModalProvince()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtCityBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtCity.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let province = prefs.value(forKey: "province") as? String
            if (province != nil && province != "") {
                let controller = ModalCity()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goCityTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let province = prefs.value(forKey: "province") as? String
            if (province != nil && province != "") {
                let controller = ModalCity()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goTxtDistrictBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtDistrict.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let city = prefs.value(forKey: "city") as? String
            if (city != nil && city != "") {
                let controller = ModalDistrict()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goDistrictTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let city = prefs.value(forKey: "city") as? String
            if (city != nil && city != "") {
                let controller = ModalDistrict()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goTxtVillageBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtVillage.resignFirstResponder()
            let prefs:UserDefaults = UserDefaults.standard
            let district = prefs.value(forKey: "district") as? String
            if (district != nil && district != "") {
                let controller = ModalVillage()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goVillageTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let district = prefs.value(forKey: "district") as? String
            if (district != nil && district != "") {
                let controller = ModalVillage()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 300
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goTxtWilayahBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("formulir", forKey: "set_simulasi")
            prefs.synchronize()
            self.txtWilayah.resignFirstResponder()
            let controller = ModalBranch()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goWilayahTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("formulir", forKey: "set_simulasi")
            prefs.synchronize()
            let controller = ModalBranch()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtOccupationBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("formulir", forKey: "set_simulasi")
            prefs.synchronize()
            self.txtOccupation.resignFirstResponder()
            let controller = ModalOccupation()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goOccupTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("formulir", forKey: "set_simulasi")
            prefs.synchronize()
            let controller = ModalOccupation()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goLanjut(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let id_province = prefs.value(forKey: "id_province") as? String
        let id_city = prefs.value(forKey: "id_city") as? String
        let id_district = prefs.value(forKey: "id_district") as? String
        let id_village = prefs.value(forKey: "id_village") as? String
        let id_occup = prefs.value(forKey: "id_occup") as? String
        let branch_id = prefs.value(forKey: "branch_id") as? String
        
        if (id_province != nil && id_city != nil && id_district != nil && id_village != nil && id_occup != nil && branch_id != nil) {
            
            if (txtTanggalLahir.text != "" && txtTelpRumah.text != "" && txtHP.text != "" && txtRT.text != "" && txtRW.text != "" && txtIbuKandung.text != "" && txtTempatLahir.text != "" && txtKodePos.text != "" && txtAlamat.text != "" && txtPenghasilanPerBulan.text != "" && txtPenghasilanTambahan.text != "" && txtSumberPenghasilan.text != "") {
                
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(self.txtTanggalLahir.text!, forKey: "tanggal_lahir")
                prefs.set(self.txtTelpRumah.text!, forKey: "telp_rumah")
                prefs.set(self.txtHP.text!, forKey: "hp")
                prefs.set(self.txtRT.text!, forKey: "rt")
                prefs.set(self.txtRW.text!, forKey: "rw")
                prefs.set(self.txtIbuKandung.text!, forKey: "ibu_kandung")
                prefs.set(self.txtTempatLahir.text!, forKey: "tempat_lahir")
                prefs.set(self.txtKodePos.text!, forKey: "kode_pos")
                prefs.set(self.txtAlamat.text!, forKey: "alamat")
                prefs.set(self.txtPenghasilanPerBulan.text!, forKey: "penghasilan_per_bulan")
                prefs.set(self.txtPenghasilanTambahan.text!, forKey: "penghasilan_tambahan")
                prefs.set(self.txtSumberPenghasilan.text!, forKey: "side_job")
                prefs.synchronize()
                
                if checkSetuju.isCheckboxSelected == true {
                    let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
                    if (set_pembiayaan == "plafond") {
                        DispatchQueue.main.async(execute: {
                            self.load.isHidden = false
                            self.load.startAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = true
                            
                            let prefs:UserDefaults = UserDefaults.standard
                            let api_key = prefs.value(forKey: "api_key") as? String
                            let token_number = prefs.value(forKey: "token_number") as? String
                            let id_customer = prefs.value(forKey: "id_customer") as? String
                            let id_occup = prefs.value(forKey: "id_occup") as? String
                            let penghasilan_per_bulan = prefs.value(forKey: "penghasilan_per_bulan") as? String
                            let penghasilan_tambahan = prefs.value(forKey: "penghasilan_tambahan") as? String
                            let side_job = prefs.value(forKey: "side_job") as? String
                            let branch_id = prefs.value(forKey: "branch_id") as? String
                            
                            let headers = [
                                "api_key": api_key,
                                "token_number": token_number
                            ]
                            
                            let parameters = [
                                "id_customer": id_customer!,
                                "occupation_id": id_occup!,
                                "monthly_income": penghasilan_per_bulan!,
                                "additional_income": penghasilan_tambahan!,
                                "side_job": side_job!,
                                "andalan_branch_id": branch_id!,
                                "survey_date": "2019-06-19",
                                "survey_time": "10:00:00"
                                ] as [String : Any]
                            
                            let appConfig : app_config = app_config()
                            Alamofire.request(appConfig.getApiUrl(type: "plafond_application"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                                .responseJSON { response in
                                    
                                    if (response.result.value == nil) {
                                        self.load.isHidden = true
                                        self.load.stopAnimating()
                                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    } else {
                                        
                                        if let value = response.result.value {
                                            let json = JSON(value)
                                            print(json)
                                            
                                            if json["status"].stringValue != "failed" {
                                                let alertController = UIAlertController(title: "Pengajuan Plafond", message: "Formulir pengajuan Plafond Anda sudah kami terima, untuk mengetahui status pengajuan anda lihat progres nya di Halaman Inbox ", preferredStyle: .alert)
                                                let OKAction = UIAlertAction(title: "Lihat", style: .default) { (action:UIAlertAction!) in
                                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                                    vc.selectedIndex = 2
                                                    vc.modalPresentationStyle = .fullScreen
                                                    self.present(vc, animated: true, completion: nil)
                                                }
                                                let CancelAction = UIAlertAction(title: "Kembali", style: .cancel) { (action:UIAlertAction!) in
                                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                                    vc.modalPresentationStyle = .fullScreen
                                                    self.present(vc, animated: true, completion: nil)
                                                }
                                                alertController.addAction(OKAction)
                                                alertController.addAction(CancelAction)
                                                self.present(alertController, animated: true, completion:nil)
                                            } else {
                                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                                let OKAction = UIAlertAction(title: "Ulang", style: .default) { (action:UIAlertAction!) in
                                                }
                                                alertController.addAction(OKAction)
                                                self.present(alertController, animated: true, completion:nil)
                                            }
                                            
                                            self.load.isHidden = true
                                            self.load.stopAnimating()
                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                        }
                                    }
                            }
                        })
                    } else {
                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_kelengkapan_dokumen")
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    let alertController = UIAlertController(title: "", message: "Please Accept Agreement ANDALAN Finance", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            } else {
                let alertController = UIAlertController(title: "", message: "Input Not Completed", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        } else {
            let alertController = UIAlertController(title: "", message: "Input Not Completed", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    @objc func donePicker() {
        txtTanggalLahir.resignFirstResponder()
        txtTanggalSurvey.resignFirstResponder()
        txtTimeSurvey.resignFirstResponder()
    }
    
    @objc func handleDatePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtTanggalLahir.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func surveyTimePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        txtTimeSurvey.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func surveyDatePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtTanggalSurvey.text = dateFormatter.string(from: sender.date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 0 {
            //return justGender.count
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        print(pickerView.tag)
        if pickerView.tag == 0 {
            //return justGender[row]
        }
        
        return " "
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        print(pickerView.tag)
        if pickerView.tag == 0 {
            //txtGender.text = justGender[row]
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
