//
//  GantiPIN.swift
//  Andalanku
//
//  Created by Handoyo on 09/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class GantiPIN: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
    }

    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: "Informasi", message: "PIN Aplikasi Andalanku berhasil diubah", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pin_andalanku")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.selectedIndex = 3
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
