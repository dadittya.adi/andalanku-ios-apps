//
//  HasilSimulasiPlafond.swift
//  Andalanku
//
//  Created by Handoyo on 23/06/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class HasilSimulasiPlafond: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var viewPlafond: UIView!
    @IBOutlet weak var lblPlafond: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        viewPlafond.layer.shadowColor = UIColor.black.cgColor
        viewPlafond.layer.shadowOpacity = 0.3
        viewPlafond.layer.shadowOffset = CGSize.zero
        viewPlafond.layer.cornerRadius = 10
        
        let prefs:UserDefaults = UserDefaults.standard
        let plafond = prefs.value(forKey: "plafond") as? String
        self.lblPlafond.text = plafond!
    }

    @IBAction func goAjukan(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("plafond", forKey: "set_pembiayaan")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_formulir_pembiayaan")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
