//
//  Help.swift
//  Andalanku
//
//  Created by Handoyo on 02/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class Help: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblFAQ: UILabel!
    @IBOutlet weak var lblTentangKami: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapLblFAQ = UITapGestureRecognizer(target:self, action:#selector(Help.goFAQTap(_:)))
        self.lblFAQ.isUserInteractionEnabled = true
        self.lblFAQ.addGestureRecognizer(tapLblFAQ)
        
        let tapLblTentang = UITapGestureRecognizer(target:self, action:#selector(Help.goTentangTap(_:)))
        self.lblTentangKami.isUserInteractionEnabled = true
        self.lblTentangKami.addGestureRecognizer(tapLblTentang)
    }
    
    @objc func goFAQTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_faq")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTentangTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_tentang_kami")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
