//
//  Home.swift
//  Andalanku
//
//  Created by Handoyo on 02/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import AACarousel
import Kingfisher
import Alamofire
import SwiftyJSON
import CoreLocation

class Home: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, AACarouselDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet var viewPlafond: UIView!
    @IBOutlet var imgPembayaran: UIImageView!
    @IBOutlet var imgPengajuan: UIImageView!
    @IBOutlet var imgEServices: UIImageView!
    @IBOutlet weak var imgCashValue: UIImageView!
    
    @IBOutlet weak var helperSeparator: UIView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewPromo: UIView!
    @IBOutlet var imgSimulasiKredit: UIImageView!
    @IBOutlet var imgJadwalAngsuran: UIImageView!
    @IBOutlet var imgSimulasiPelunasan: UIImageView!
    @IBOutlet var imgTranferCash: UIImageView!
    @IBOutlet var cvJSON: UICollectionView!
    @IBOutlet var cvPARTNER: UICollectionView!
    @IBOutlet var cvBERITA: UICollectionView!
    @IBOutlet var lblProfil: UILabel!
    @IBOutlet var imgProfil: UIImageView!
    @IBOutlet var carouselView: AACarousel!
    @IBOutlet var lblLihatSemuaPromo: UILabel!
    @IBOutlet var lblLihatSemuaBerita: UILabel!
    
    @IBOutlet weak var arrowCashValue: UIImageView!
    @IBOutlet weak var lblCashValue: UILabel!
    @IBOutlet weak var viewPartner: UIView!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var arrowPlafond: UIImageView!
    @IBOutlet weak var lblPlafond: UILabel!
    @IBOutlet weak var viewBerita: UIView!
    
    var arrTitleLabel = ["Cooming soon","Cooming soon","Cooming soon"]
    var imgSourceArray = [String]()
    var titleArray = [String]()
    var tglArray = [String]()
    var contentArray = [String]()
    
    var arrBeritaLabel = ["Cooming soon","Cooming soon","Cooming soon"]
    
    var arrPromoImage: [UIImage] = [
        UIImage(named: "sample_promo4")!,
        UIImage(named: "sample_promo2")!,
        UIImage(named: "sample_promo3")!,
        UIImage(named: "sample_promo")!
    ]
    
    var arrPartnerImage: [UIImage] = [
        UIImage(named: "sel_partner1")!,
        UIImage(named: "sel_partner2")!,
        UIImage(named: "sel_partner3")!,
        UIImage(named: "sel_partner1")!,
        UIImage(named: "sel_partner2")!
    ]
    
    var arrBeritaImage: [UIImage] = [
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!
    ]
    
    var arrNewsNow: NSMutableArray = []
    var arrPartnerNow: NSMutableArray = []
    
    @IBOutlet weak var barNotifLogin: UIBarButtonItem!
    @IBOutlet weak var navBar: UINavigationBar!
    var locationManager = CLLocationManager()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_spc")!)
        
        self.carouselView.setCarouselOpaque(layer: true, describedTitle:
            true, pageIndicator: false)
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        viewPlafond.layer.shadowColor = UIColor.gray.cgColor
        viewPlafond.layer.shadowOpacity = 0.5
        viewPlafond.layer.shadowOffset = CGSize(width: 1, height: 3)
        viewPlafond.layer.cornerRadius = 10
        
        carouselView.layer.borderWidth = 0.5
        carouselView.layer.borderColor = UIColor.lightGray.cgColor
        carouselView.layer.shadowColor = UIColor.black.cgColor
        carouselView.layer.shadowOpacity = 0.5
        carouselView.layer.shadowOffset = CGSize(width: 0, height: 3)
        carouselView.layer.cornerRadius = 5
        carouselView.layoutMargins = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        
        let tapImgPembayaran = UITapGestureRecognizer(target:self, action:#selector(Home.goPembayaranTap(_:)))
        self.imgPembayaran.isUserInteractionEnabled = true
        self.imgPembayaran.addGestureRecognizer(tapImgPembayaran)
        
        let tapImgPengajuan = UITapGestureRecognizer(target:self, action:#selector(Home.goPengajuanTap(_:)))
        self.imgPengajuan.isUserInteractionEnabled = true
        self.imgPengajuan.addGestureRecognizer(tapImgPengajuan)
        
        let tapImgKredit = UITapGestureRecognizer(target:self, action:#selector(Home.goPengajuanTap(_:)))
        self.imgSimulasiKredit.isUserInteractionEnabled = true
        self.imgSimulasiKredit.addGestureRecognizer(tapImgKredit)
        
        let tapImgJadwal = UITapGestureRecognizer(target:self, action:#selector(Home.goJadwalAngsuranTap(_:)))
        self.imgJadwalAngsuran.isUserInteractionEnabled = true
        self.imgJadwalAngsuran.addGestureRecognizer(tapImgJadwal)
        
        let tapImgDokumen = UITapGestureRecognizer(target:self, action:#selector(Home.goDokumenKontrakTap(_:)))
        self.imgTranferCash.isUserInteractionEnabled = true
        self.imgTranferCash.addGestureRecognizer(tapImgDokumen)
        
        let tapImgPelunasan = UITapGestureRecognizer(target:self, action:#selector(Home.goPelunasanTap(_:)))
        self.imgSimulasiPelunasan.isUserInteractionEnabled = true
        self.imgSimulasiPelunasan.addGestureRecognizer(tapImgPelunasan)
        
        let tapImgEServices = UITapGestureRecognizer(target:self, action:#selector(Home.goEServicesTap(_:)))
        self.imgEServices.isUserInteractionEnabled = true
        self.imgEServices.addGestureRecognizer(tapImgEServices)
        
        let tapLblProfil = UITapGestureRecognizer(target:self, action:#selector(Home.goProfilTap(_:)))
        self.lblProfil.isUserInteractionEnabled = true
        self.lblProfil.addGestureRecognizer(tapLblProfil)
        
        let tapImgProfil = UITapGestureRecognizer(target:self, action:#selector(Home.goProfilTap(_:)))
        self.imgProfil.isUserInteractionEnabled = true
        self.imgProfil.addGestureRecognizer(tapImgProfil)
        
        let tapArrowPlafond = UITapGestureRecognizer(target:self, action:#selector(Home.goPlafondTap(_:)))
        self.arrowPlafond.isUserInteractionEnabled = true
        self.arrowPlafond.addGestureRecognizer(tapArrowPlafond)
        
        let tapLblPlafond = UITapGestureRecognizer(target:self, action:#selector(Home.goPlafondTap(_:)))
        self.lblPlafond.isUserInteractionEnabled = true
        self.lblPlafond.addGestureRecognizer(tapLblPlafond)
        
        let tapLblLihatPromo = UITapGestureRecognizer(target:self, action:#selector(Home.goLihatPromoTap(_:)))
        self.lblLihatSemuaPromo.isUserInteractionEnabled = true
        self.lblLihatSemuaPromo.addGestureRecognizer(tapLblLihatPromo)
        
        let tapLblLihatBerita = UITapGestureRecognizer(target:self, action:#selector(Home.goLihatBeritaTap(_:)))
        self.lblLihatSemuaBerita.isUserInteractionEnabled = true
        self.lblLihatSemuaBerita.addGestureRecognizer(tapLblLihatBerita)
        
        let tapImgCash = UITapGestureRecognizer(target:self, action:#selector(Home.goCashTap(_:)))
        self.imgCashValue.isUserInteractionEnabled = true
        self.imgCashValue.addGestureRecognizer(tapImgCash)
        
        let tapCashValue = UITapGestureRecognizer(target:self, action:#selector(Home.goCashValueTap))
        self.arrowCashValue.isUserInteractionEnabled = true
        self.arrowCashValue.addGestureRecognizer(tapCashValue)
        
        self.lblProfil.isHidden = true
        self.imgProfil.isHidden = true
        
        fillCashValue()
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("", forKey: "no_kontrak")
            prefs.set("", forKey: "no_kategori")
            prefs.synchronize()
            
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            if (nama_akun != nil && nama_akun != "") {
                self.lblProfil.text = "Hi, " + nama_akun!
                self.barNotifLogin.title = ""
                self.barNotifLogin.image = UIImage(named: "icon_notifikasi")
                
                self.lblProfil.isHidden = false
                self.imgProfil.isHidden = false
                self.lblWelcome.isHidden = true
            } else {
                self.lblProfil.isHidden = true
                self.imgProfil.isHidden = true
                self.lblWelcome.isHidden = false
                self.lblProfil.text = "Selamat Datang"
                self.barNotifLogin.title = "Login"
            }
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "keyword": "",
                "page": 1
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            for item in json["promo_list"].arrayValue {
                                self.titleArray.append(item["title"].stringValue)
                                self.tglArray.append(item["promo_date"].stringValue)
                                self.contentArray.append(item["content"].stringValue)
                                self.imgSourceArray.append(item["image"].stringValue)
                            }
                        }
                        
                        self.carouselView.delegate = self
                        self.carouselView.setCarouselData(paths: self.imgSourceArray,  describedTitle: self.titleArray, isAutoScroll: true, timer: 5.0, defaultImage: "defaultImage")
                        
                        self.carouselView.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: UIColor.white, layerColor: nil)
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            Alamofire.request(appConfig.getApiUrl(type: "news_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["news_list"].arrayObject != nil {
                                self.arrNewsNow.removeAllObjects()
                                if let past : NSArray = json["news_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["news_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrNewsNow.add((json["news_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.cvBERITA.reloadData()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    }
            }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            Alamofire.request(appConfig.getApiUrl(type: "partner_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["partner_list"].arrayObject != nil {
                                self.arrPartnerNow.removeAllObjects()
                                if let past : NSArray = json["partner_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["partner_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrPartnerNow.add((json["partner_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.cvPARTNER.reloadData()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    }
            }
        })
    }
    
    func didSelectCarouselView(_ view:AACarousel ,_ index:Int) {
        let promo_image = imgSourceArray[index]
        let tanggalPromo = tglArray[index]
        let contentPromo = contentArray[index]
        let titlePromo = titleArray[index]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(promo_image as String, forKey: "imgPromo")
        prefs.set(tanggalPromo as String, forKey: "tanggalPromo")
        prefs.set(contentPromo as String, forKey: "contentPromo")
        prefs.set(titlePromo as String, forKey: "titlePromo")
        prefs.synchronize()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo_detail")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func downloadImages(_ url: String, _ index:Int) {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize.zero
        imageView.layer.cornerRadius = 5
        imageView.layoutMargins = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        
        /*
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let leading = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: carouselView, attribute: .leading, multiplier: 1.0, constant: 4.0)
        let trailing = NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: carouselView, attribute: .trailing, multiplier: 1.0, constant: 4.0)
        NSLayoutConstraint.activate([leading,trailing])
        */
        
        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.carouselView.contentMode = .scaleAspectFill
            self.carouselView.clipsToBounds = true
            self.carouselView.images[index] = self.cropToBounds(image: downloadImage!, width: 343, height: 145)
        })
    }
    
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize.zero
        imageView.layer.cornerRadius = 5
        imageView.layoutMargins = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
            
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.carouselView.images[index] = downloadImage!
        })
    }
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize = contextImage.size
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async(execute: {
            self.locationManager.stopUpdatingLocation()
            let location = locations.last
            let latitude = location?.coordinate.latitude
            let longitude = location?.coordinate.longitude
            print(latitude)
            print(longitude)
        })
    }
    
    func startAutoScroll() {
        carouselView.startScrollImageView()
    }
    
    func stopAutoScroll() {
        carouselView.stopScrollImageView()
    }
    
    @objc func goProfilTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                vc.selectedIndex = 3
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goCashTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_cash_value")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goJadwalAngsuranTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_jadwal_angsuran")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goDokumenKontrakTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dokumen_kontrak")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goPelunasanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_simulasi_pelunasan")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goPlafondTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set("home", forKey: "set_plafond")
                prefs.synchronize()
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_plafond")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goLihatPromoTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("home", forKey: "set_promo")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goLihatBeritaTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("home", forKey: "set_berita")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_berita")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goPembayaranTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set("", forKey: "no_kontrak")
                prefs.synchronize()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembayaran_angsuran")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @objc func goCashValueTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            let ComingSoonStoryboard = UIStoryboard(name: "ComingSoon", bundle: nil)
            let vc = ComingSoonStoryboard.instantiateViewController(withIdentifier: "story_cs_cashvalue")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            /*if (nama_akun != nil && nama_akun != "") {
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set("", forKey: "no_kontrak")
                prefs.synchronize()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembayaran_angsuran")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }*/
        })
    }
    
    @objc func goPengajuanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("home", forKey: "set_pembiayaan")
            prefs.synchronize()
            
            let controller = ModalPengajuanPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 320
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
            
        })
    }
    
    @objc func goEServicesTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let nama_akun = prefs.value(forKey: "nama_akun") as? String
            
            if (nama_akun != nil && nama_akun != "") {
                let controller = ModalController()
                let transitionDelegate = SPStorkTransitioningDelegate()
                transitionDelegate.customHeight = 240
                controller.transitioningDelegate = transitionDelegate
                controller.modalPresentationStyle = .custom
                self.present(controller, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goNofit(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        if (nama_akun != nil && nama_akun != "") {
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvJSON {
            return arrPromoImage.count
        } else if collectionView == self.cvPARTNER {
            return arrPartnerNow.count
        } else if collectionView == self.cvBERITA {
            return arrNewsNow.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            if collectionView == self.cvBERITA {
                
                let news_image : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
                let content_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
                let title_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
                
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(news_image as String, forKey: "imgNews")
                prefs.set(content_news as String, forKey: "contentNews")
                prefs.set(title_news as String, forKey: "titleNews")
                prefs.synchronize()
                
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_berita_detail")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else if collectionView == self.cvPARTNER {
                let partner_url : NSString = (self.arrPartnerNow[indexPath.row] as AnyObject).value(forKey: "url") as! NSString
                
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(partner_url as String, forKey: "partner_url")
                prefs.synchronize()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_partner")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if collectionView == self.cvJSON {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HomePromoCell
            cell.imgPromo.image = arrPromoImage[indexPath.row]
            return cell as HomePromoCell
        } else if collectionView == self.cvPARTNER {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HomePartnerCell
            
            let news_image : NSString = (self.arrPartnerNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            
            cell.downloadImage(URL(string: news_image as String)!)
            
            return cell as HomePartnerCell
        } else if collectionView == self.cvBERITA {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HomeBeritaCell
            
            let news_image : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            let content_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
            let title_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
            
            cell.downloadImage(URL(string: news_image as String)!)
            cell.lblBerita.text = content_news as String
            cell.lblTitle.text = title_news as String
            return cell as HomeBeritaCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    func fillCashValue() {
        let prefs:UserDefaults = UserDefaults.standard
        if let id_customer = prefs.value(forKey: "id_customer") as? String {
            DispatchQueue.main.async(execute: {
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "id_customer": id_customer
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "cash_value"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            //self.load.isHidden = true
                            //self.load.stopAnimating()
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                
                                if json["status"].stringValue == "success" {
                                    for item in json["cash_value_list"].arrayValue {
                                        self.lblCashValue.text = "Rp. " + json["total_cash_value"].stringValue
                                        //self.txtNoKontrak.text = item["AgreementNo"].stringValue
                                    }
                                }
                            }
                            
                            //self.load.isHidden = true
                            //self.load.stopAnimating()
                        }
                }
            })
        }
        
    }
}
