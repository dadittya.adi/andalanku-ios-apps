//
//  HomeBeritaCell.swift
//  Andalanku
//
//  Created by Handoyo on 03/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class HomeBeritaCell: UICollectionViewCell {
    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var viewPromo: UIView!
    @IBOutlet var lblBerita: UILabel!
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet weak var loadBerita: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadBerita.isHidden = true
        viewPromo.layer.borderColor = UIColor.lightGray.cgColor
        viewPromo.layer.borderWidth = 0.5
        viewPromo.layer.cornerRadius = 5
        
        imgPromo.layer.cornerRadius = 5
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        self.loadBerita.isHidden = false
        self.loadBerita.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                self.loadBerita.isHidden = true
                self.loadBerita.stopAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let data = data, error == nil else
                {
                    return
                }
                self.imgPromo.image = UIImage(data: data as Data)
            }
        }
    }
}
