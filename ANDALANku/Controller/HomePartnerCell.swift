//
//  HomePartnerCell.swift
//  Andalanku
//
//  Created by Handoyo on 03/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class HomePartnerCell: UICollectionViewCell {
    
    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var viewPromo: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //viewPromo.layer.masksToBounds = false
        viewPromo.layer.shadowColor = UIColor.gray.cgColor
        viewPromo.layer.shadowOpacity = 0.5
        viewPromo.layer.shadowOffset = CGSize(width: 1, height: 3) //CGSize.zero
        viewPromo.layer.cornerRadius = 5
        //viewPromo.layer.shadowRadius = 1
        
        //viewPromo.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        //viewPromo.layer.shouldRasterize = true
        //viewPromo.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let data = data, error == nil else
                {
                    return
                }
                self.imgPromo.image = UIImage(data: data as Data)
            }
        }
    }
}
