//
//  HomePromoCell.swift
//  Andalanku
//
//  Created by Handoyo on 03/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class HomePromoCell: UICollectionViewCell {
    
    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var viewPromo: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewPromo.layer.shadowColor = UIColor.black.cgColor
        viewPromo.layer.shadowOpacity = 0.5
        viewPromo.layer.shadowOffset = CGSize.zero
        viewPromo.layer.cornerRadius = 5
        
        super.awakeFromNib()
        imgPromo.layer.shadowColor = UIColor.black.cgColor
        imgPromo.layer.shadowOpacity = 0.5
        imgPromo.layer.shadowOffset = CGSize.zero
        imgPromo.layer.cornerRadius = 5
    }
}
