//
//  Inbox.swift
//  Andalanku
//
//  Created by Handoyo on 02/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Inbox: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tvJSON: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    var arrDict :NSMutableArray=[]
    var arrInbox = ["Lorem ipsum","Lorem ipsum","Lorem ipsum"]
    var par_num = 0
    var limit = 1
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.isHidden = true
        self.load.isHidden = true
        //self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        
        let prefs:UserDefaults = UserDefaults.standard
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        
        if (nama_akun != nil && nama_akun != "") {
            
            DispatchQueue.main.async(execute: {
                
                self.load.isHidden = false
                self.load.startAnimating()
                
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                let id_customer = prefs.value(forKey: "id_customer") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "id_customer": id_customer!,
                    "page": "1"
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "inbox_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                               
                                if json["inbox_list"].arrayObject != nil {
                                    self.arrDict.removeAllObjects()
                                    if let past : NSArray = json["inbox_list"].arrayObject as NSArray? {
                                        print(past)
                                        for i in 0 ..< (json["inbox_list"].arrayObject! as NSArray).count
                                        {
                                            self.arrDict.add((json["inbox_list"].arrayObject! as NSArray) .object(at: i))
                                        }
                                    }
                                    
                                    if self.arrDict.count == 0 {
                                        self.tvJSON.isHidden = true
                                        self.scrollView.isHidden = false
                                    } else {
                                        self.tvJSON.isHidden = false
                                        self.scrollView.isHidden = true
                                         self.par_num = self.par_num + self.limit
                                        self.tvJSON.reloadData()
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                            }
                        }
                }
            })
            
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func loadBottom() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!,
                "page": self.par_num
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "inbox_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            if json["inbox_list"].arrayObject != nil {
                                if let past : NSArray = json["inbox_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["inbox_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["inbox_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                if self.arrDict.count == 0 {
                                    self.tvJSON.isHidden = true
                                    self.scrollView.isHidden = false
                                } else {
                                    self.tvJSON.isHidden = false
                                    self.scrollView.isHidden = true
                                    self.par_num = self.par_num + self.limit
                                    self.tvJSON.reloadData()
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : InboxCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? InboxCell
        
        let title : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
        cell.lblTitle.text = title as String
        
        let id_inbox : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "id") as! NSString
        
        /*
        DispatchQueue.main.async(execute: {
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!,
                "id_inbox": id_inbox as String
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "inbox_detail"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let js_decode = json["content"].stringValue
                            print(js_decode)
                            
                            if let data = js_decode.data(using: .utf8) {
                                if let js_decode = try? JSON(data: data) {
                                    
                                    if title == "Pengajuan Kredit" {
                                        cell.lblType.text = js_decode["message"].stringValue + "\nSilahkan Lihat Detail >"
                                    } else if title == "E-Complain" {
                                        cell.lblType.text = "No Complain : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["processed_status"].stringValue + "\nSilahkan Lihat Detail >"
                                    } else if title == "E-Claim Asuransi" {
                                        cell.lblType.text = "No Request : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["status"].stringValue + "\nSilahkan Lihat Detail >"
                                    } else if title == "Pengajuan Plafond" {
                                        cell.lblType.text = js_decode["message"].stringValue + "\nSilahkan Lihat Detail >"
                                    } else if title == "E-Request Ambil Jaminan" {
                                        cell.lblType.text = js_decode["message"].stringValue + "\nSilahkan Lihat Detail >"
                                    }
                                    
                                    /*
                                     let htmlString = js_decode["message"].stringValue
                                    let htmlText = htmlString
                                    if let htmlData = htmlText.data(using: String.Encoding.unicode) {
                                        do {
                                            let attributedText = try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
                                            cell.lblType.attributedText = attributedText
                                        } catch let e as NSError {
                                            print("Couldn't translate \(String(describing: htmlText)): \(e.localizedDescription) ")
                                        }
                                    }
                                    */
                                }
                            }
                        }
                    }
            }
        })
        */
        
        let type : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "message") as! NSString
        cell.lblType.text = type as String
        
        let tanggal : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "date") as! NSString
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: tanggal as String) {
            print(dateFormatterPrint.string(from: date))
            cell.lblTanggal.text = dateFormatterPrint.string(from: date)
        } else {
            cell.lblTanggal.text = tanggal as String
        }
        
        return cell as InboxCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            let id_inbox : NSString=(self.arrDict[indexPath.row] as AnyObject).value(forKey: "id") as! NSString
            
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(id_inbox, forKey: "id_inbox")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_inbox_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
