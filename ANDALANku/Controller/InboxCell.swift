//
//  InboxCell.swift
//  Andalanku
//
//  Created by Handoyo on 20/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblTanggal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
