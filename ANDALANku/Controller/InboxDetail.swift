//
//  InboxDetail.swift
//  Andalanku
//
//  Created by Handoyo on 20/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class InboxDetail: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblMessage: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        DispatchQueue.main.async(execute: {
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            let id_inbox = prefs.value(forKey: "id_inbox") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!,
                "id_inbox": id_inbox!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "inbox_detail"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["status"].stringValue == "success" {
                                let title:String = json["title"].stringValue
                                let js_decode = json["content"].stringValue
                                print(js_decode)
                                
                                if let data = js_decode.data(using: .utf8) {
                                    if let js_decode = try? JSON(data: data) {
                                        
                                        if title == "Pengajuan Kredit" {
                                            self.lblMessage.text = js_decode["message"].stringValue + "\n\nNo Request : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["status"].stringValue + "\nTanggal Pengajuan : " + js_decode["tangal_pengajuan"].stringValue
                                        } else if title == "E-Complain" {
                                            self.lblMessage.text = js_decode["complain_message"].stringValue + "\n\nNo Complain : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["processed_status"].stringValue + "\nTanggal Pengajuan : " + js_decode["input_date"].stringValue
                                        } else if title == "E-Claim Asuransi" {
                                            self.lblMessage.text = js_decode["message"].stringValue + "\n\nNo Request : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["status"].stringValue + "\nTanggal Pengajuan : " + js_decode["input_date"].stringValue
                                        } else if title == "Pengajuan Plafond" {
                                            self.lblMessage.text = js_decode["message"].stringValue + "\n\nNo Request : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["status"].stringValue + "\nTanggal Pengajuan : " + js_decode["input_date"].stringValue
                                        } else if title == "E-Request Ambil Jaminan" {
                                            self.lblMessage.text = js_decode["message"].stringValue + "\n\nNo Request : " + js_decode["ticket_number"].stringValue + "\nStatus : " + js_decode["status"].stringValue + "\nTanggal Pengajuan : " + js_decode["input_date"].stringValue
                                        }
                                        
                                        /*
                                        let htmlString = js_decode["message"].stringValue
                                        let htmlText = htmlString
                                        if let htmlData = htmlText.data(using: String.Encoding.unicode) {
                                            do {
                                                let attributedText = try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
                                                self.lblMessage.attributedText = attributedText
                                            } catch let e as NSError {
                                                print("Couldn't translate \(String(describing: htmlText)): \(e.localizedDescription) ")
                                            }
                                        }
                                        */
                                    }
                                }
                            }
                        }
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
