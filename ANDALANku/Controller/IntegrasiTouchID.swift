//
//  IntegrasiTouchID.swift
//  Andalanku
//
//  Created by Handoyo on 10/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class IntegrasiTouchID: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
    }

    @IBAction func goYA(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: "Informasi", message: "Integrasi Touch ID berhasil ditetapkan", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pengaturan")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        })
    }
    
    @IBAction func goTidak(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
