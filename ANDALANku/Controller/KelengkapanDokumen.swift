//
//  KelengkapanDokumen.swift
//  Andalanku
//
//  Created by Handoyo on 23/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class KelengkapanDokumen: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewKeuangan: UIView!
    @IBOutlet weak var viewRekening: UIView!
    @IBOutlet weak var viewSlip: UIView!
    @IBOutlet weak var viewKK: UIView!
    @IBOutlet weak var viewKTP: UIView!
    @IBOutlet weak var imgKTP: UIImageView!
    @IBOutlet weak var lblKTP: UILabel!
    @IBOutlet weak var imgKK: UIImageView!
    @IBOutlet weak var lblKK: UILabel!
    @IBOutlet weak var imgSlip: UIImageView!
    @IBOutlet weak var lblSlip: UILabel!
    @IBOutlet weak var imgKoran: UIImageView!
    @IBOutlet weak var lblKoran: UILabel!
    @IBOutlet weak var imgKeuangan: UIImageView!
    @IBOutlet weak var lblKeuangan: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.load.isHidden = true
        
        viewKTP.layer.cornerRadius = 10
        viewKTP.layer.borderColor = UIColor(rgb: 0xFC874E).cgColor
        viewKTP.layer.borderWidth = 1
        
        viewKK.layer.cornerRadius = 10
        viewKK.layer.borderColor = UIColor(rgb: 0xFC874E).cgColor
        viewKK.layer.borderWidth = 1
        
        viewSlip.layer.cornerRadius = 10
        viewSlip.layer.borderColor = UIColor(rgb: 0xFC874E).cgColor
        viewSlip.layer.borderWidth = 1
        
        viewRekening.layer.cornerRadius = 10
        viewRekening.layer.borderColor = UIColor(rgb: 0xFC874E).cgColor
        viewRekening.layer.borderWidth = 1
        
        viewKeuangan.layer.cornerRadius = 10
        viewKeuangan.layer.borderColor = UIColor(rgb: 0xFC874E).cgColor
        viewKeuangan.layer.borderWidth = 1
        
        let tapImgKTP = UITapGestureRecognizer(target:self, action:#selector(KelengkapanDokumen.imageTapped(_:)))
        self.lblKTP.isUserInteractionEnabled = true
        self.lblKTP.addGestureRecognizer(tapImgKTP)
        
        let tapImgKK = UITapGestureRecognizer(target:self, action:#selector(KelengkapanDokumen.imageKK(_:)))
        self.lblKK.isUserInteractionEnabled = true
        self.lblKK.addGestureRecognizer(tapImgKK)
        
        let tapImgSlip = UITapGestureRecognizer(target:self, action:#selector(KelengkapanDokumen.imageSlip(_:)))
        self.lblSlip.isUserInteractionEnabled = true
        self.lblSlip.addGestureRecognizer(tapImgSlip)
        
        let tapImgKoran = UITapGestureRecognizer(target:self, action:#selector(KelengkapanDokumen.imageKoran(_:)))
        self.lblKoran.isUserInteractionEnabled = true
        self.lblKoran.addGestureRecognizer(tapImgKoran)
        
        let tapImgKeuangan = UITapGestureRecognizer(target:self, action:#selector(KelengkapanDokumen.imageKeuangan(_:)))
        self.lblKeuangan.isUserInteractionEnabled = true
        self.lblKeuangan.addGestureRecognizer(tapImgKeuangan)
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("KTP", forKey: "set_picker")
            prefs.synchronize()
            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    @objc func imageKK(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("KK", forKey: "set_picker")
            prefs.synchronize()
            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    @objc func imageSlip(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("Slip", forKey: "set_picker")
            prefs.synchronize()
            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    @objc func imageKoran(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("Koran", forKey: "set_picker")
            prefs.synchronize()
            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    @objc func imageKeuangan(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("Keuangan", forKey: "set_picker")
            prefs.synchronize()
            
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let set_picker = prefs.value(forKey: "set_picker") as? String
            if set_picker == "KTP" {
                self.imgKTP.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                self.dismiss(animated: true, completion: nil)
            } else if set_picker == "KK" {
                self.imgKK.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                self.dismiss(animated: true, completion: nil)
            } else if set_picker == "Slip" {
                self.imgSlip.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                self.dismiss(animated: true, completion: nil)
            } else if set_picker == "Koran" {
                self.imgKoran.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                self.dismiss(animated: true, completion: nil)
            } else if set_picker == "Keuangan" {
                self.imgKeuangan.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func goAjukan(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
        
        if (set_pembiayaan == "new_cars") {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                let id_customer = prefs.value(forKey: "id_customer") as? String
                
                let hasAlpha = false
                let scale: CGFloat = 0.0
                
                let image: UIImage = self.imgKTP.image!
                let size = image.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
                image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
                UIGraphicsEndImageContext()
                let imageDataKTP = image.jpegData(compressionQuality: 0.9)
                let base64_ktp = imageDataKTP!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKK: UIImage = self.imgKK.image!
                let sizeKK = imageKK.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKK, !hasAlpha, scale)
                imageKK.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKK))
                UIGraphicsEndImageContext()
                let imageDataKK = imageKK.jpegData(compressionQuality: 0.9)
                let base64_kk = imageDataKK!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageSlip: UIImage = self.imgSlip.image!
                let sizeSlip = imageSlip.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeSlip, !hasAlpha, scale)
                imageSlip.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeSlip))
                UIGraphicsEndImageContext()
                let imageDataSlip = imageSlip.jpegData(compressionQuality: 0.9)
                let base64_slip = imageDataSlip!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKoran: UIImage = self.imgKoran.image!
                let sizeKoran = imageKoran.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKoran, !hasAlpha, scale)
                imageKoran.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKoran))
                UIGraphicsEndImageContext()
                let imageDataKoran = imageKoran.jpegData(compressionQuality: 0.9)
                let base64_koran = imageDataKoran!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKeuangan: UIImage = self.imgKeuangan.image!
                let sizeKeuangan = imageKeuangan.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKeuangan, !hasAlpha, scale)
                imageKeuangan.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKeuangan))
                UIGraphicsEndImageContext()
                let imageDataKeuangan = imageKeuangan.jpegData(compressionQuality: 0.9)
                let base64_keuangan = imageDataKeuangan!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let base64 = "iVBORw0KGgoAAAANSUhEUgAAAIYAAABHCAYAAADCzT+AAAAYXWlDQ1BJQ0MgUHJvZmlsZQAAWIWVWQdUFEuz7tnZTM45R8k5Ss4ZkSxpyTksQUQkyUUFVBBEEAQxAwIqkhRECSKCiIGkoCAiiEhSREThDUHv/e9/znvn9Tkz8211dYXu6qmuHQA4kkjh4cEoWgBCQqPINka6vI5Ozry4CYAGbIAaSAEpkldkuI61tTlA2u/nf7ZvAwDafL6Q3JT13/3/a6Pz9on0AgCyRrCnd6RXCIJvA4BO9QonRwGAVUHoAvujwjexC4IZyYiBCA7fxH7bOHUTe27j/C0eWxs9BF8HAE9JIpH9AKBuQOi8MV5+iBzqIaSPPtQ7IBRhnUWwppc/yRsADgmERyIkJGwTOyJY1PMfcvz+Q6bnH5kkkt8fvO3LVsPrB0SGB5MO/D+n4/9uIcHRv3UIIxelP9nYZtNnZN6GgsLMNjElgmdDPS2tEEyP4O8B3lv8CEYR/aON7bb5UZxekXrInAFmBMt4k/TNEMyJYMPQYEvzHbqnb4ChCYKRCEHFBkSZ2O6MPeoTabB3R2YROczG6jf2Jevp7IytIpG39G7yt0cH2ensyB/y9zH5Lf9rnL+tw7bNMDEmwN4SwdQIZo4M2mu2zQMLxvnrWf7mIUfbbNoviGA1n1Aj3W35sJsv2dBmh58cEvnbX/iof4CJ5Q4+G+Vva7wj57oXact+VgQ3+ITq2P2W4xPpaP7bF28ffYNt3+E+n1C7HX/hsfAoXZudsUvhwdY7/GiiT7DRJp0fwZyRMXt3xqI1o5CA3JaPtgyPsrbdthPtGUgytd62Bx0LzIEe0Ae8IBq5PEEYCAQBvbP1s8iv7R5DQAJk4Ad8gOQO5fcIh62eUOS+F8SBzwjyAZF/xulu9fqAGIT+6w91+y4JfLd6Y7ZGBIEPCA4BZiAY+R29NSr0jzZ78B6hBPyXdi/E1mDk2uz7b5oOQjHfoUT/lstL85sTa4DVxxpjDbG70OxoTbQ62hy5ayOXHFoFrfrb2r/5MR8wzzDvMP2YMcywe0AK+V/+8AILMIZoMNzx2fOfPqOFEamKaF20BiIfkY1mRrMDSbQCokkHrYXoVkSoejuWb3r/b9n/4cM/Zn2HjyBDQBFYCNoE0X+PpBajVvwjZXNO/zlD27Z6/plXvT89/9av94+Z9kaeZv/mhI/CNXAn/ADugpvgesALt8ANcA/cvIn/RNH7rSj6rc1my54gRE7Af+kj7ejcnMlImQqZjzI/t/uifGKjNjeYXlj4AXKAn38Urw7y5vfhNQn1kpLglZORVQVgM49sv6a+2GzlB4j56d80ciMAuzWQ/bP8N82DHYCaLCQlhPxNE4SRLSQPQPNjr2hyzDYNvXnDACKgQXYUG+AGAkAU8UcOKAF1oA0MgCmwArbACbghs+yPxDMZ7AfxIBmkgQyQBU6Ds6AEXATXQCW4BepBE3gAHoJu0Af6wWskeibBJ7AAvoE1CIJwEBXEALFBPJAQJA7JQSqQJmQAmUM2kBPkAflBoVA0FA8dhjKgU9BZqBQqg25CjdADqAt6Bg1D49BHaAn6gYJRlChGFBdKGCWNUkHpoMxQtihXlB8qAhWHSkWdQOWjLqCuo+pQD1DdqH7UGOoTahkGMAXMDPPBkrAKrAdbwc6wL0yGE+B0OA++AFfBd5B1fgGPwbPwKhqLZkDzoiWRCDZG26G90BHoBHQm+iz6GroO3Y5+gR5HL6DXMVQYTow4Rg1jgnHE+GH2Y9IweZgrmFpMB7KbJjHfsFgsM1YEq4zsRidsIPYgNhN7DluNvY99hp3ALuNwODacOE4DZ4Uj4aJwabgC3HVcC+45bhL3HU+B58HL4Q3xzvhQfAo+D1+Ov4d/jp/CrxFoCUIENYIVwZtwgHCScIlwh/CUMElYI9IRRYgaRFtiIDGZmE+sInYQR4hfKCgo+ClUKfZQBFAkUeRT3KB4RDFOsUpJTylGqUfpQhlNeYLyKuV9ymHKL1RUVMJU2lTOVFFUJ6jKqNqo3lB9p2aglqI2ofamTqQupK6jfk49R0OgEaLRoXGjiaPJo6mheUozS0ugFabVoyXRJtAW0jbSDtIu0zHQydJZ0YXQZdKV03XRTdPj6IXpDei96VPpL9K30U8wwAwCDHoMXgyHGS4xdDBMMmIZRRhNGAMZMxgrGXsZF5jomRSY7JlimQqZmpnGmGFmYWYT5mDmk8y3mAeYf7Bwseiw+LAcY6liec6ywsrBqs3qw5rOWs3az/qDjZfNgC2ILZutnm2UHc0uxr6HfT97MXsH+ywHI4c6hxdHOsctjlecKE4xThvOg5wXOXs4l7m4uYy4wrkKuNq4ZrmZubW5A7lzue9xf+Rh4NHkCeDJ5WnhmeFl4tXhDebN523nXeDj5DPmi+Yr5evlW+MX4bfjT+Gv5h8VIAqoCPgK5Aq0CiwI8ghaCMYLVgi+EiIIqQj5C50R6hRaERYRdhA+IlwvPC3CKmIiEidSITIiSiWqJRohekH05S7sLpVdQbvO7eoTQ4kpivmLFYo9FUeJK4kHiJ8TfyaBkVCVCJW4IDEoSSmpIxkjWSE5LsUsZS6VIlUvNSctKO0snS3dKb0uoygTLHNJ5rUsvaypbIrsHdklOTE5L7lCuZfyVPKG8onyDfKLCuIKPgrFCkOKDIoWikcUWxV/KSkrkZWqlD4qCyp7KBcpD6owqlirZKo8UsWo6qomqjaprqopqUWp3VKbV5dUD1IvV5/eLbLbZ/el3RMa/BokjVKNMU1eTQ/N85pjWnxaJK0LWu+0BbS9ta9oT+ns0gnUua4zpyujS9at1V3RU9M7pHdfH9Y30k/X7zWgN7AzOGvwxpDf0M+wwnDBSNHooNF9Y4yxmXG28aAJl4mXSZnJgqmy6SHTdjNKs71mZ83emYuZk83vWKAsTC1yLEYshSxDLeutgJWJVY7VqLWIdYT13T3YPdZ7Cvd8sJG1ibfp3Muw131v+d5vtrq2J21f24naRdu12tPYu9iX2a846DucchhzlHY85NjtxO4U4NTgjHO2d77ivLzPYN/pfZMuii5pLgOuIq6xrl1u7G7Bbs3uNO4k9xoPjIeDR7nHT5IV6QJp2dPEs8hzwUvP64zXJ29t71zvjz4aPqd8pnw1fE/5Tvtp+OX4ffTX8s/znw3QCzgbsBhoHFgSuBJkFXQ1aCPYIbg6BB/iEdIYSh8aFNoexh0WG/YsXDw8LXwsQi3idMQC2Yx8JRKKdI1siGJEDuw90aLRf0WPx2jGFMZ832+/vyaWLjY0tueA2IFjB6biDOMuH0Qf9DrYGs8Xnxw/fkjnUGkClOCZ0JookJiaOJlklHQtmZgclPwkRSblVMrXww6H76RypSalTvxl9FdFGnUaOW3wiPqRkqPoowFHe4/JHys4tp7unf44QyYjL+Nnplfm4+Oyx/OPb5zwPdF7UulkcRY2KzRrIFsr+9opulNxpyZyLHLqcnlz03O/nnY/3ZWnkFdyhngm+sxYvnl+Q4FgQVbBz7P+Z/sLdQuriziLjhWtnPM+97xYu7iqhKsko+TH+YDzQ6VGpXUXhC/kXcRejLn44ZL9pc7LKpfLrrBfybjy62ro1bFrNtfay5TLyso5y09WoCqiKz5ed7neV6lf2VAlWVVazVydcQPciL4xc9Pj5sAts1utNSo1VbeFbhfVMtSm10F1B+oW6v3rxxqcGp41mja23lG/U3tX6u7VJr6mwmam5pP3iPdS7220xLUs3w+/P/vA78FEq3vr6zbHtpfte9p7O8w6Hj00fNjWqdPZ8kjjUVOXWlfjY5XH9d1K3XU9ij21TxSf1PYq9dY9VX7a0Kfad+fZ7mf3nms9f/BC/8XDlyYvu/st+58N2A0MDboMjg15D00PBw8vvop5tfY6aQQzkj5KO5r3hvPNhbe73laPKY01j+uP97zb++71hNfEp/eR739Opn6g+pA3xTNVNi033fTR8GPfzL6ZyU/hn9Zm0z7TfS6aE527Pa8937PguDC5SF7cWMr8wvbl6leFr63L1stvvoV8W1tJ/872/dqqymrnD4cfU2v7f+J+5v/a9evOutn6yEbIxkY4iUzaOgogpwOA8vUFYOkqAFROADD0AUDct13n7TQYOXygto4NjEAGOIMcMILk8kRoHGWHGob90DC6FhOJVcNR4mbxI4QeYitFO2Un1RPqQVp5uhIGBsZjTOssCWwweyonHVcpjyLvI35fQZxQhche0XWxSgkXyRnpAJkZOT/5cUUnpW4VZdUSdWj3Po0bWpC2lU6u7mt9boM9hh5GYcZJJtmmF81qzbssRi2XrPF7eG1U9lrZ+tsdti9xqHfsdZreB7mwucq7mbt7exwk5XpWeLV6D/ss+hH9uQIkAlWCDINtQ0ihYWEHw49G5JJLI69HNUa3xzzb/zr2zYHxuPcHp+I/HppJmE38nDSXPJcyd3gudf6v+bTPR2aOTh+bSp/O+JQ5f/zbiY0sYjbLKeEcpVyT0z55mWdu578q2CjkL9I7Ryo+VJJ//mZp14W3F5cvE66wXxW/plZmUu5Q4XU9rPJAVQoSsbk3i25dqmm5PVq7Wk/XINSoeEf7rmGTcbPhPe0W1fsyDxRbrdsC2pM6ch9e6qx+VNtV//h2d1XPpScFvelP4/oCnjk8138h+5KzH9s/NzAw2DR0YfjIK//XhiP8IxujI28a3+aNRY3bvJOZoJn4/L5nsuRD4JTc1Mp0/UfyjOjM0KfkWaHZjs8en9fmSudN5r8slCyaL/5cqvpC+srytXf5yDeVbyMrTit9362+v1gN/EHxo3vt6s/iX5XrTzc2tmKFEqk+FZFzewqoA18hXagIhUJFoZbgeDQ9+g4mGDn9zOBq8YcJjkQlCjqKRSoCtQKNB20G3T36H4xyTNHMd1mJbI7s1zlxXD7cXbxSfDkCsGCoUJ+IrGj6rvfiuyWyJSekpWQiZWvl5hWEFW2VkpXLVfpUv6gTdzNpcGkKaIlqS+rI6srqyehLG0gaihrxG3OasJjSmuHNfpovWkxZjlg9t368p9Xm7t5a2yq7MvtLDucdi5zynXP3Zbr85XrILcY9zMOf5O5p72Xhre+j5ivjJ4LEBl0gHLgcNBU8HNId2hxWFV4SkUVOjoyIIkXbxOjtl4nlOkA48A2Jj2fxLYeqEooTTyQlJZNTfA47p9r95ZTmdsTzqO+xwPTgjNDMsOMRJyJPRmXFZB84lZCTknvkdGbeyTOn8nMLTp/NKzxTlH+uoLiw5Nz5G6UdFwYvTl9auYK6SnGNvoy1nLOC97pApWiVYrX5Da+bcbdO1ly8XVP7oK6n/mXDcOPrO6N33zWt3WNskbxv8MClNaLtcHtOx/mHFZ01j+50NT++132/p+NJd2//03d9i8/hFywvJfv1B1wGA4cihmNfJb9OHzk9Wvqm+u29safjY++W3sOT6MmND2tIZHz9uDSz+Glxdunz17mV+bVFaInmC/9X9WWXb0dW7q2iftit3f7FtZ65tf4ogEeqdAGgCuyQeuA8eAphIQfoJooZlQZD8HG0IPohJgTLge3BHcLL42cJ5cRkijBKEpUDtSWNCa0JnQW9LYMHYzhTGnMxSyPrENsaBz+nCVcYdzbPdd5Ovnf8a4L0QkLCciK7RfV3GYrpIvEgJykkxSKNkV6QGZJ9IFcmf0rhoKKPkqWyigqfKlH1i9ob9Ue7b2kUaaZouWrL6xB03ure1jum72oga4g2HDAqMz5oYmnKY7po1maea+FrqWJFaTVuXb/nhI3PXk1bZtsFu8f2lx2SHJ2dZJ3xzm/31bkcd/V2U3enc5/2aCHleYZ6GXpze3/xeeRb7BfpbxzAheTzh0GFweEhBqHsofNhHeGFEZFk00j+yNWovugrMfH7rWMFY78f6IkrOUiOl4+fP1SREJQol7iR9CK5KiXr8IFU/7/2pdkcMTuqf2x3ukKGWCbPcdrjv058ONmdVZF95JR7jkIu92muPN4zQvliBTJnlQu1iozOWRU7lLif9yv1u7DvosUl3ctqV5SuKl3bXWZU7lQRfj2jsqLqSfXcTepbEjWmt31rD9eV1N9tGGr8dpe5SbnZ615+y5MHUKt8m1f7yY6Gh6Od611cj9W6XXtu9HI+Pdb37bn3i+f9OgM3h/iHc19jR2LfCIxpvPN+f/7DysewWZq5kcW3y+zfM35KbK7/9v99mw2rBEDpIAD2RAAsuwEoHgZAiA4AGqT2tKYCwFYVoK74AhRRA0Cxe/7kD0akxjRAasoYcBKUgTYwClYhZkgeqQXJUA50HXqI1Hs/UawoBZQNKgyp6q6jelCzMCVSwVnC4XAOXAcPw7/QfGhDdBA6G12PfoPBYCQxdpgkTDXmLZYWq4ONwl7DjuIYcGa4VKS2WsOr4g/g7+I3CPqETEI/kY8YSmyioKLwonhIKU6ZTblK5YlkKVXqqzTMNEdoVmmDacfpHOn66E3o2xi0Ge4z6jC2MRkx9TLbMb9lCWJZZT3Kxs5Wya7H/oqDzEnJWcm1h+s793keU54V3st89vwE/nsC0YLSgjNC14T9RERFlkRbdp0QcxeXlyBKTEjekzonnSBDkjWSk5ZnU8AqrCp+VnqnPKTSq/pQrVm9dnelxmXNYq0z2tk6Gbrxev769gZGhupGcsYSJmKmEmay5moWhpZ2Vn7W8XtybG7tfW77zZ7TwdAx0umS85ALraup21H3RyQKT2uvZO+bPpN+3P5uAZcCl4KNQ66GUYcfiliKDIn6HKOx/2Tshzjdg5cPUSckJa4lJx6mTL2Qpn3k/bGsDN3M1RM1WZGnVHLh0y/PlBekFfqeMy3RLtW6qH3Z4Kp5mX2FZyW5OvVmQc3N2vkG0zs1zfItT1uzOxIfFXQP9I4/G305MNj56ubo6bGoCesPOZ9457WWmpbPfaf4ofxTbV1o6/3BD3SAB0gE50ATGAG/IF5IDwqAsqAbUB+0iNT3yqh9qETUZaSGX4TZYG04AD4FN8Ef0HTo3Wh/9Gl0G3oRw4OxQtb7NmYay421x57APsLBOG1cAq4Fj0Lq5+P4QQI/IYLwgMhIDCDep2CniKMYozSmrKHipTpJDVHHIDWtP80krQ/tNFKxfqVPYKBgOMcow9jB5Mr0jTmLRZKlmzWYjZrtFrsjB+Ao57TnwnPd5Y7g2cUzwXuez4NfgH9a4IbgQSFjYRbhjyL3RQt3xYo5iqtJ8EjiJJekxqT7ZNpl78jdkq9UqFasUbqr3I5krzdqc7uBBoOmqJamtr1OmG6y3nH9QoNKwwdGQ8ZfkNwlY25jsd+y2KrTetGGc6+Jbaxduf1bR1YnW+ecff2u7G4k93KPZU8dr3jvOp9lP2X/xICeIJ7gmJAXYfLhZyLWI/2insWI7I+P7Y+TOZgV/zXBLfFJsl5KS6ruX91HHI9OpztmPD6udaIuSzb7Ro5i7r08yzMTBYmFfEWdxdHnxUvfXsy5bHBl+drFcquK1cqLSPZZvlVy27SOsX6ssepufLNpC9v9qda69tSHNo/4u5a6257UPW181vKio79nsH949PWH0YW3P95h3tN94Jhmn6GfRX2enm9bzP5is4z5VvHdaPXVmt/PxfXYrfVXAPtAMrgMusA8xIKcHsKgc1AHNIfseH1UBKoE1Ytah6VhErLTO+E1tCzaD12MHsDQYEwxaZiHWDzWDJuFHcTx4oJxd/FUeBK+kcBEiCIMEjWIVygYKFIpVinDKD9SeVO9o/aknqQJpFmijafD052hF6VvYnBhRDNWMbkwUzO3scSzqrKusjWzp3JYcXJyznG1cZ/lieS14BND9vCMQK9gjVChcJpIlGjgLk8xN3FXCXdJH6lQ6TiZY7KFcjfkOxWmlAjKUioOqqlqt9WnNXg0nbTytAd02fRc9S8ZzBlpGGeZTJsZmF+xpLCKtp6w2W/LY9fsYOXY72y1r9mV3y3FfZSk7HnSa9bHzLfanyUgLXA9OCmUKuxKhDl5PaoxRmH/5QOscZnx4FBMwuck3+Txw26pb9I8kF0aj2SMoROpWZLZz3PCTxPyCvNFCqoK5YpqiuVK6ks1Ljy6ZHd56urBMvryiuuGle+qE25y3Gq4bVs7Uu/ZMHMnpgnbXNAid/9Za0w7f8fLzswu027KnsZerqdJfZPPTV/c6GcZSBycHt7z6u6I8Gj2m19jQePDE8bv6ydXPqxOfZ9e/jg/M/VpeLbr8+25s/PxC06LskuYpedfzn31WBZAqo68FbOVte/lqzarqz9K1jTXRn/G/2L6VbVuuD68Eby5/pG+8nJb6QOi1AUA82Zj44swALhTAPzK3thYu7Cx8esicsgcAeB+8PY3pK1cQwtAUfcm6khuvPHvbzn/A9Ws8cvAx6gWAAABnGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyI+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4xMzQ8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+NzE8L2V4aWY6UGl4ZWxZRGltZW5zaW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4K/RX+OgAAAT1JREFUeAHt0rENwDAMBDHH+0+X0sskgEvhRqA6fXngc877LafAKLDH71XgFgADhCwARmYxgsFAFgAjsxjBYCALgJFZjGAwkAXAyCxGMBjIAmBkFiMYDGQBMDKLEQwGsgAYmcUIBgNZAIzMYgSDgSwARmYxgsFAFgAjsxjBYCALgJFZjGAwkAXAyCxGMBjIAmBkFiMYDGQBMDKLEQwGsgAYmcUIBgNZAIzMYgSDgSwARmYxgsFAFgAjsxjBYCALgJFZjGAwkAXAyCxGMBjIAmBkFiMYDGQBMDKLEQwGsgAYmcUIBgNZAIzMYgSDgSwARmYxgsFAFgAjsxjBYCALgJFZjGAwkAXAyCxGMBjIAmBkFiMYDGQBMDKLEQwGsgAYmcUIBgNZAIzMYgSDgSwARmYxgsFAFgAjsxjBYCAL/CiNBBOL9CgyAAAAAElFTkSuQmCC"
                
                let otr = prefs.value(forKey: "otr") as? String
                let dp = prefs.value(forKey: "dp") as? String
                let dp_amount = prefs.value(forKey: "hasil") as? String
                let tenor = prefs.value(forKey: "no_tenor") as? String
                let plafond = prefs.value(forKey: "pokok_pinjaman") as? String
                
                var premi = prefs.value(forKey: "no_premi") as? String
                if (premi == "All Risk") {
                    premi = "ARK"
                }
                
                var wilayah = prefs.value(forKey: "no_wilayah") as? String
                if wilayah == "Sumatra dan Kepulauannya" {
                    wilayah = "1"
                } else if wilayah == "Jakarta, Banten dan Jawa Barat" {
                    wilayah = "2"
                } else if wilayah == "Lainnya" {
                    wilayah = "3"
                } else {
                    wilayah = "3"
                }
                
                let ktp = prefs.value(forKey: "ktp") as? String
                let id_province = prefs.value(forKey: "id_province") as? String
                let id_district = prefs.value(forKey: "id_district") as? String
                let id_occup = prefs.value(forKey: "id_occup") as? String
                let id_village = prefs.value(forKey: "id_village") as? String
                let id_city = prefs.value(forKey: "id_city") as? String
                let tempat_lahir = prefs.value(forKey: "tempat_lahir") as? String
                let tanggal_lahir = prefs.value(forKey: "tanggal_lahir") as? String
                let ibu_kandung = prefs.value(forKey: "ibu_kandung") as? String
                let telp_rumah = prefs.value(forKey: "telp_rumah") as? String
                let hp = prefs.value(forKey: "hp") as? String
                let penghasilan_per_bulan = prefs.value(forKey: "penghasilan_per_bulan") as? String
                let penghasilan_tambahan = prefs.value(forKey: "penghasilan_tambahan") as? String
                let side_job = prefs.value(forKey: "side_job") as? String
                let alamat = prefs.value(forKey: "alamat") as? String
                let rt = prefs.value(forKey: "rt") as? String
                let rw = prefs.value(forKey: "rw") as? String
                let kode_pos = prefs.value(forKey: "kode_pos") as? String
                let branch_id = prefs.value(forKey: "branch_id") as? String
                let email = prefs.value(forKey: "email") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "id_customer": id_customer!,
                    "product": "NEW CAR",
                    "otr": otr!,
                    "dp_percent": dp!,
                    "dp_amount": dp_amount!,
                    "plafond": plafond!,
                    "tenor": tenor!,
                    "insurance": premi!,
                    "region": wilayah!,
                    "collateral_type": "BPKB",
                    "collateral": "Mobil",
                    "collateral_year": "2015",
                    "collateral_owner": "",
                    "collateral_paper_number": "",
                    "place_of_birth": tempat_lahir!,
                    "date_of_birth": tanggal_lahir!,
                    "id_number": ktp!,
                    "mother_name": ibu_kandung!,
                    "home_phone_number": telp_rumah!,
                    "phone_number": hp!,
                    "occupation_id": id_occup!,
                    "monthly_income": penghasilan_per_bulan!,
                    "additional_income": penghasilan_tambahan!,
                    "side_job": side_job!,
                    "province_id": id_province!,
                    "city_id": id_city!,
                    "district_id": id_district!,
                    "village_id": id_village!,
                    "address": alamat!,
                    "rt": rt!,
                    "rw": rw!,
                    "branch_id": branch_id!,
                    "loan_application_referal": "N",
                    "customer_name": "",
                    "survey_date": "2019-06-19",
                    "email": email!,
                    "zip_code": kode_pos!,
                    "foto_ktp": base64_ktp,
                    "foto_kk": base64_kk,
                    "foto_slip_gaji": base64_slip,
                    "foto_rekening_koran": base64_koran,
                    "foto_keuangan_lainnya": base64_keuangan
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "loan_application"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                
                                if json["status"].stringValue != "failed" {
                                    let alertController = UIAlertController(title: "Pengajuan Plafond", message: "Formulir pengajuan pembiayaan New Cars Anda sudah kami terima, untuk mengetahui status pengajuan anda lihat progres nya di Halaman Inbox ", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Lihat", style: .default) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.selectedIndex = 2
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    let CancelAction = UIAlertAction(title: "Kembali", style: .cancel) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    alertController.addAction(CancelAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Ulang", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                }
            })
        } else if (set_pembiayaan == "dana") {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                let id_customer = prefs.value(forKey: "id_customer") as? String
                
                let hasAlpha = false
                let scale: CGFloat = 0.0
                
                let image: UIImage = self.imgKTP.image!
                let size = image.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
                image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
                UIGraphicsEndImageContext()
                let imageDataKTP = image.jpegData(compressionQuality: 0.9)
                let base64_ktp = imageDataKTP!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKK: UIImage = self.imgKK.image!
                let sizeKK = imageKK.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKK, !hasAlpha, scale)
                imageKK.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKK))
                UIGraphicsEndImageContext()
                let imageDataKK = imageKK.jpegData(compressionQuality: 0.9)
                let base64_kk = imageDataKK!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageSlip: UIImage = self.imgSlip.image!
                let sizeSlip = imageSlip.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeSlip, !hasAlpha, scale)
                imageSlip.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeSlip))
                UIGraphicsEndImageContext()
                let imageDataSlip = imageSlip.jpegData(compressionQuality: 0.9)
                let base64_slip = imageDataSlip!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKoran: UIImage = self.imgKoran.image!
                let sizeKoran = imageKoran.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKoran, !hasAlpha, scale)
                imageKoran.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKoran))
                UIGraphicsEndImageContext()
                let imageDataKoran = imageKoran.jpegData(compressionQuality: 0.9)
                let base64_koran = imageDataKoran!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKeuangan: UIImage = self.imgKeuangan.image!
                let sizeKeuangan = imageKeuangan.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKeuangan, !hasAlpha, scale)
                imageKeuangan.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKeuangan))
                UIGraphicsEndImageContext()
                let imageDataKeuangan = imageKeuangan.jpegData(compressionQuality: 0.9)
                let base64_keuangan = imageDataKeuangan!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let otr = prefs.value(forKey: "otr") as? String
                let dp = prefs.value(forKey: "dp") as? String
                let dp_amount = prefs.value(forKey: "hasil") as? String
                let tenor = prefs.value(forKey: "no_tenor") as? String
                let plafond = prefs.value(forKey: "pokok_pinjaman") as? String
                
                var premi = prefs.value(forKey: "no_premi") as? String
                if (premi == "All Risk") {
                    premi = "ARK"
                }
                
                var wilayah = prefs.value(forKey: "no_wilayah") as? String
                if wilayah == "Sumatra dan Kepulauannya" {
                    wilayah = "1"
                } else if wilayah == "Jakarta, Banten dan Jawa Barat" {
                    wilayah = "2"
                } else if wilayah == "Lainnya" {
                    wilayah = "3"
                } else {
                    wilayah = "3"
                }
                
                let ktp = prefs.value(forKey: "ktp") as? String
                let id_province = prefs.value(forKey: "id_province") as? String
                let id_district = prefs.value(forKey: "id_district") as? String
                let id_occup = prefs.value(forKey: "id_occup") as? String
                let id_village = prefs.value(forKey: "id_village") as? String
                let id_city = prefs.value(forKey: "id_city") as? String
                let tempat_lahir = prefs.value(forKey: "tempat_lahir") as? String
                let tanggal_lahir = prefs.value(forKey: "tanggal_lahir") as? String
                let ibu_kandung = prefs.value(forKey: "ibu_kandung") as? String
                let telp_rumah = prefs.value(forKey: "telp_rumah") as? String
                let hp = prefs.value(forKey: "hp") as? String
                let penghasilan_per_bulan = prefs.value(forKey: "penghasilan_per_bulan") as? String
                let penghasilan_tambahan = prefs.value(forKey: "penghasilan_tambahan") as? String
                let side_job = prefs.value(forKey: "side_job") as? String
                let alamat = prefs.value(forKey: "alamat") as? String
                let rt = prefs.value(forKey: "rt") as? String
                let rw = prefs.value(forKey: "rw") as? String
                let kode_pos = prefs.value(forKey: "kode_pos") as? String
                let branch_id = prefs.value(forKey: "branch_id") as? String
                let email = prefs.value(forKey: "email") as? String
                let atas_nama = prefs.value(forKey: "atas_nama") as? String
                let no_polisi = prefs.value(forKey: "no_polisi") as? String
                let kendaraan = prefs.value(forKey: "kendaraan") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "id_customer": id_customer!,
                    "product": "DANA ANDALANKU",
                    "otr": otr!,
                    "dp_percent": "0",
                    "dp_amount": "0",
                    "plafond": plafond!,
                    "tenor": tenor!,
                    "insurance": premi!,
                    "region": wilayah!,
                    "collateral_type": "BPKB",
                    "collateral": "Mobil",
                    "collateral_year": "2015",
                    "collateral_owner": atas_nama!,
                    "collateral_paper_number": no_polisi!,
                    "car_year": "2015",
                    "asset_code": kendaraan!,
                    "regional_mrp": "901",
                    "branch_id": branch_id!,
                    "survey_date": "2019-06-19",
                    "loan_application_referal": "N",
                    "customer_name": "Donovan",
                    "gender": "L",
                    "place_of_birth": tempat_lahir!,
                    "date_of_birth": tanggal_lahir!,
                    "id_number": ktp!,
                    "mother_name": ibu_kandung!,
                    "home_phone_number": telp_rumah!,
                    "phone_number": hp!,
                    "occupation_id": id_occup!,
                    "monthly_income": penghasilan_per_bulan!,
                    "additional_income": penghasilan_tambahan!,
                    "side_job": side_job!,
                    "province_id": id_province!,
                    "city_id": id_city!,
                    "district_id": id_district!,
                    "village_id": id_village!,
                    "address": alamat!,
                    "rt": rt!,
                    "rw": rw!,
                    "email": email!,
                    "zip_code": kode_pos!,
                    "foto_ktp": base64_ktp,
                    "foto_kk": base64_kk,
                    "foto_slip_gaji": base64_slip,
                    "foto_rekening_koran": base64_koran,
                    "foto_keuangan_lainnya": base64_keuangan
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "loan_application"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                
                                if json["status"].stringValue != "failed" {
                                    let alertController = UIAlertController(title: "Pengajuan Plafond", message: "Formulir pengajuan pembiayaan Dana Andalanku Anda sudah kami terima, untuk mengetahui status pengajuan anda lihat progres nya di Halaman Inbox ", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Lihat", style: .default) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.selectedIndex = 2
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    let CancelAction = UIAlertAction(title: "Kembali", style: .cancel) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    alertController.addAction(CancelAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                }
            })
        } else {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                let id_customer = prefs.value(forKey: "id_customer") as? String
                
                let hasAlpha = false
                let scale: CGFloat = 0.0
                
                let image: UIImage = self.imgKTP.image!
                let size = image.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
                image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
                UIGraphicsEndImageContext()
                let imageDataKTP = image.jpegData(compressionQuality: 0.9)
                let base64_ktp = imageDataKTP!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKK: UIImage = self.imgKK.image!
                let sizeKK = imageKK.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKK, !hasAlpha, scale)
                imageKK.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKK))
                UIGraphicsEndImageContext()
                let imageDataKK = imageKK.jpegData(compressionQuality: 0.9)
                let base64_kk = imageDataKK!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageSlip: UIImage = self.imgSlip.image!
                let sizeSlip = imageSlip.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeSlip, !hasAlpha, scale)
                imageSlip.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeSlip))
                UIGraphicsEndImageContext()
                let imageDataSlip = imageSlip.jpegData(compressionQuality: 0.9)
                let base64_slip = imageDataSlip!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKoran: UIImage = self.imgKoran.image!
                let sizeKoran = imageKoran.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKoran, !hasAlpha, scale)
                imageKoran.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKoran))
                UIGraphicsEndImageContext()
                let imageDataKoran = imageKoran.jpegData(compressionQuality: 0.9)
                let base64_koran = imageDataKoran!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let imageKeuangan: UIImage = self.imgKeuangan.image!
                let sizeKeuangan = imageKeuangan.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                UIGraphicsBeginImageContextWithOptions(sizeKeuangan, !hasAlpha, scale)
                imageKeuangan.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: sizeKeuangan))
                UIGraphicsEndImageContext()
                let imageDataKeuangan = imageKeuangan.jpegData(compressionQuality: 0.9)
                let base64_keuangan = imageDataKeuangan!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
                
                let otr = prefs.value(forKey: "otr") as? String
                let dp = prefs.value(forKey: "dp") as? String
                let dp_amount = prefs.value(forKey: "hasil") as? String
                let tenor = prefs.value(forKey: "no_tenor") as? String
                let plafond = prefs.value(forKey: "pokok_pinjaman") as? String
                
                var premi = prefs.value(forKey: "no_premi") as? String
                if (premi == "All Risk") {
                    premi = "ARK"
                }
                
                var wilayah = prefs.value(forKey: "no_wilayah") as? String
                if wilayah == "Sumatra dan Kepulauannya" {
                    wilayah = "1"
                } else if wilayah == "Jakarta, Banten dan Jawa Barat" {
                    wilayah = "2"
                } else if wilayah == "Lainnya" {
                    wilayah = "3"
                } else {
                    wilayah = "3"
                }
                
                let ktp = prefs.value(forKey: "ktp") as? String
                let id_province = prefs.value(forKey: "id_province") as? String
                let id_district = prefs.value(forKey: "id_district") as? String
                let id_occup = prefs.value(forKey: "id_occup") as? String
                let id_village = prefs.value(forKey: "id_village") as? String
                let id_city = prefs.value(forKey: "id_city") as? String
                let tempat_lahir = prefs.value(forKey: "tempat_lahir") as? String
                let tanggal_lahir = prefs.value(forKey: "tanggal_lahir") as? String
                let ibu_kandung = prefs.value(forKey: "ibu_kandung") as? String
                let telp_rumah = prefs.value(forKey: "telp_rumah") as? String
                let hp = prefs.value(forKey: "hp") as? String
                let penghasilan_per_bulan = prefs.value(forKey: "penghasilan_per_bulan") as? String
                let penghasilan_tambahan = prefs.value(forKey: "penghasilan_tambahan") as? String
                let side_job = prefs.value(forKey: "side_job") as? String
                let alamat = prefs.value(forKey: "alamat") as? String
                let rt = prefs.value(forKey: "rt") as? String
                let rw = prefs.value(forKey: "rw") as? String
                let kode_pos = prefs.value(forKey: "kode_pos") as? String
                let branch_id = prefs.value(forKey: "branch_id") as? String
                let email = prefs.value(forKey: "email") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "id_customer": id_customer!,
                    "product": "USED CAR",
                    "otr": otr!,
                    "dp_percent": dp!,
                    "dp_amount": dp_amount!,
                    "plafond": plafond!,
                    "tenor": tenor!,
                    "insurance": premi!,
                    "region": wilayah!,
                    "collateral_type": "BPKB",
                    "collateral": "motor",
                    "collateral_year": "2015",
                    "collateral_owner": "",
                    "collateral_paper_number": "",
                    "place_of_birth": tempat_lahir!,
                    "date_of_birth": tanggal_lahir!,
                    "id_number": ktp!,
                    "mother_name": ibu_kandung!,
                    "home_phone_number": telp_rumah!,
                    "phone_number": hp!,
                    "occupation_id": id_occup!,
                    "monthly_income": penghasilan_per_bulan!,
                    "additional_income": penghasilan_tambahan!,
                    "side_job": side_job!,
                    "province_id": id_province!,
                    "city_id": id_city!,
                    "district_id": id_district!,
                    "village_id": id_village!,
                    "address": alamat!,
                    "rt": rt!,
                    "rw": rw!,
                    "branch_id": branch_id!,
                    "loan_application_referal": "N",
                    "customer_name": "Donovan",
                    "survey_date": "2019-06-19",
                    "email": email!,
                    "zip_code": kode_pos!,
                    "foto_ktp": base64_ktp,
                    "foto_kk": base64_kk,
                    "foto_slip_gaji": base64_slip,
                    "foto_rekening_koran": base64_koran,
                    "foto_keuangan_lainnya": base64_keuangan
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "loan_application"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                
                                if json["status"].stringValue != "failed" {
                                    let alertController = UIAlertController(title: "Pengajuan Plafond", message: "Formulir pengajuan pembiayaan Used Cars Anda sudah kami terima, untuk mengetahui status pengajuan anda lihat progres nya di Halaman Inbox ", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Lihat", style: .default) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.selectedIndex = 2
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    let CancelAction = UIAlertAction(title: "Kembali", style: .cancel) { (action:UIAlertAction!) in
                                        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    alertController.addAction(CancelAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Ulang", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                }
            })
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
