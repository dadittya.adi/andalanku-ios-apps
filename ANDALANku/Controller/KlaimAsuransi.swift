//
//  KlaimAsuransi.swift
//  Andalanku
//
//  Created by Handoyo on 02/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Material
import SPStorkController
import FSCalendar

class KlaimAsuransi: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, UIScrollViewDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewFoto: UIView!
    @IBOutlet weak var txtPesan: UITextView!
    @IBOutlet weak var lblUpload: UILabel!
    @IBOutlet weak var imgUpload: UIImageView!
    @IBOutlet weak var txtNoKontrak: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var txtPremiAsuransi: TextField!
    
    @IBOutlet weak var txtNamaLengkap: TextField!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var txtNoHandphone: TextField!
    @IBOutlet weak var txtNoTelp: TextField!
    @IBOutlet weak var txtTanggal: TextField!
    @IBOutlet weak var downTanggal: UIImageView!
    
    var saveTanggal = String()
    var datePicker: UIDatePicker!
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    
    @IBOutlet weak var calendarView: FSCalendar!
    
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendarView.isHidden = true
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        self.scrollView.delegate = self
        self.load.isHidden = true
        calendarView.placeholderType = .none
        calendarView.layer.shadowColor = UIColor.lightGray.cgColor
        calendarView.layer.shadowOpacity = 0.3
        calendarView.layer.shadowOffset = CGSize.zero
        
        viewFoto.layer.shadowColor = UIColor.lightGray.cgColor
        viewFoto.layer.shadowOpacity = 0.3
        viewFoto.layer.shadowOffset = CGSize.zero
        viewFoto.layer.cornerRadius = 10
        
        let prefs:UserDefaults = UserDefaults.standard
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        self.txtNamaLengkap.text = nama_akun!
        
        let tapImgUpload = UITapGestureRecognizer(target:self, action:#selector(KlaimAsuransi.imageTapped(_:)))
        self.lblUpload.isUserInteractionEnabled = true
        self.lblUpload.addGestureRecognizer(tapImgUpload)
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(KlaimAsuransi.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoKontrak.delegate = self
        
        self.txtPremiAsuransi.delegate = self
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "personal_information"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            self.txtNamaLengkap.text = json["customer_name"].stringValue
                            self.txtNoTelp.text = json["home_phone_number"].stringValue
                            self.txtNoHandphone.text = json["phone_number"].stringValue
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
        
        self.datePicker = UIDatePicker()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        timeFormatter.dateFormat = "HH:mm:ss"
        
        let dateA = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))
        
        let dateBirth = dateA?.addingTimeInterval(60 * 90 * 24 * 1)
        self.datePicker.date = dateBirth!
        self.datePicker.datePickerMode = .date
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(rgb: 0x5DC4F8)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(KlaimAsuransi.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(KlaimAsuransi.donePicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        //self.txtTanggal.inputView = self.datePicker
        //self.txtTanggal.inputAccessoryView = toolBar
        
        //self.datePicker.addTarget(self, action: #selector(KlaimAsuransi.handleDatePicker(_:)), for: UIControl.Event.valueChanged)
        
        let tapDown = UITapGestureRecognizer(target:self, action:#selector(KlaimAsuransi.goDownTanggal(_:)))
        self.downTanggal.isUserInteractionEnabled = true
        self.downTanggal.addGestureRecognizer(tapDown)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("end scroll")
        self.calendarView.isHidden = true
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("calendar did select date \(self.dateFormatter1.string(from: date))")
        self.txtTanggal.text = self.dateFormatter1.string(from: date)
        self.saveTanggal = self.dateFormatter2.string(from: date)
        self.calendarView.isHidden = true
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        self.datePicker = UIDatePicker()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        //let dateAndalan = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))
        
        //let isoDate = datePicker.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "id")
        
        let dateString = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))!
        
        if (date > dateString) {
            return false
        } else {
            return true
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        self.datePicker = UIDatePicker()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        //let dateAndalan = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "id")
        
        let dateString = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))!
        
        if (date > dateString) {
            return UIColor.lightGray
        } else {
            return nil
        }
    }
    
    @objc func donePicker() {
        txtTanggal.resignFirstResponder()
    }
    
    @objc func handleDatePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtTanggal.text = dateFormatter.string(from: sender.date)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            //return justGender.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        print(pickerView.tag)
        if pickerView.tag == 0 {
            //return justGender[row]
        }
        return " "
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerView.tag == 0 {
            //txtGender.text = justGender[row]
        }
    }
    
    @IBAction func goTxtTanggalBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtTanggal.resignFirstResponder()
            self.calendarView.isHidden = false
        })
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("klaim", forKey: "set_no_kontrak")
            prefs.synchronize()
            self.txtNoKontrak.resignFirstResponder()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTanggal(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txtTanggal.resignFirstResponder()
            self.calendarView.isHidden = false
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("klaim", forKey: "set_no_kontrak")
            prefs.synchronize()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtPremiBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("klaim", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtPremiAsuransi.resignFirstResponder()
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goPremiTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("klaim", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        DispatchQueue.main.async(execute: {
            self.imgUpload.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            var type = self.txtPremiAsuransi.text!
            if type == "Total Loss" {
                type = "TLO"
            } else {
                type = "ARK"
            }
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_customer": id_customer!,
                "agreement_number": no_kontrak!,
                "claim_type": type,
                "event_case": "TLO",
                "event_date": self.saveTanggal, //self.txtTanggal.text!,
                "location": "Yogyakarta",
                "notes": self.txtPesan.text!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "insurance_claim"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["status"].stringValue == "success" {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_eclaim_asuransi")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
