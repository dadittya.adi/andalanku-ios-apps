//
//  KomplainDetail.swift
//  Andalanku
//
//  Created by Handoyo on 03/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class KomplainDetail: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var cvJSON: UICollectionView!
    
    var arrImageCash: [UIImage] = [
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!,
        UIImage(named: "sample_berita")!
    ]
    
    var arrList = [String]()
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblPesan: UILabel!
    @IBOutlet weak var lblHandphone: UILabel!
    @IBOutlet weak var lblSolution: UILabel!
    
    @IBOutlet weak var viewPesan: UIView!
    @IBOutlet weak var lblKeluhan: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        let prefs:UserDefaults = UserDefaults.standard
        let ticket_number = prefs.value(forKey: "ticket_number") as? String
        let category = prefs.value(forKey: "category") as? String
        let submitted_date = prefs.value(forKey: "submitted_date") as! String
        let status = prefs.value(forKey: "status") as? String
        let message = prefs.value(forKey: "message") as? String
        let set_solution = prefs.value(forKey: "set_solution") as? String
        let phone = prefs.value(forKey: "phone") as? String
        let arrMedia = prefs.value(forKey: "arrMedia") as! Array<Any>
        
        self.lblTicketNumber.text = ticket_number
        self.lblCategory.text = category
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: submitted_date) {
            self.lblTanggal.text = dateFormatterPrint.string(from: date)
        } else {
            self.lblTanggal.text = submitted_date
        }
        
        self.lblStatus.text = status
        self.lblPesan.text = message
        self.lblHandphone.text = phone
        self.lblSolution.text = set_solution
        
        if (arrMedia.count == 0) {
            lblKeluhan.translatesAutoresizingMaskIntoConstraints = false
            let heightLblKeluhan = NSLayoutConstraint(item: lblKeluhan, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
            NSLayoutConstraint.activate([heightLblKeluhan])
            
            cvJSON.translatesAutoresizingMaskIntoConstraints = false
            let heightTitle3 = NSLayoutConstraint(item: cvJSON, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
            NSLayoutConstraint.activate([heightTitle3])
        }
        self.arrList.removeAll()
        for i in 0 ..< arrMedia.count {
            self.arrList.append(arrMedia[i] as! String)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvJSON {
            return arrList.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            if collectionView == self.cvJSON {
                /*
                 let news_image : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
                 let content_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
                 let title_news : NSString = (self.arrNewsNow[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
                 
                 let prefs:UserDefaults = UserDefaults.standard
                 prefs.set(news_image as String, forKey: "imgNews")
                 prefs.set(content_news as String, forKey: "contentNews")
                 prefs.set(title_news as String, forKey: "titleNews")
                 prefs.synchronize()
                 
                 let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_berita_detail")
                 vc.modalPresentationStyle = .fullScreen
                 self.present(vc, animated: true, completion: nil)
                 */
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if collectionView == self.cvJSON {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! KomplainDetailCell
            
            let img = self.arrList[indexPath.row]
            cell.downloadImage(URL(string: img as String)!)
            return cell as KomplainDetailCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
