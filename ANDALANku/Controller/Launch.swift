//
//  Launch.swift
//  Andalanku
//
//  Created by Handoyo on 11/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Launch: UIViewController {

    @IBOutlet var load: UIActivityIndicatorView!
    @IBOutlet var btnRefresh: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.load.isHidden = true
        self.btnRefresh.isHidden = true
        
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        self.btnRefresh.isHidden = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        self.btnRefresh.isHidden = true
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                self.btnRefresh.isHidden = false
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
        
        /*
        let prefs:UserDefaults = UserDefaults.standard
        let set_otp = prefs.value(forKey: "set_otp") as? String
        if (set_otp != nil)
        {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                
                let parameters = [
                    "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                    "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            self.btnRefresh.isHidden = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            self.btnRefresh.isHidden = true
                            if let value = response.result.value {
                                let json = JSON(value)
                                let status = json["status"].stringValue
                                let token_number = json["token_number"].stringValue
                                if status == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                    prefs.set(token_number, forKey: "token_number")
                                    prefs.synchronize()
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    self.btnRefresh.isHidden = false
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                            self.load.isHidden = true
                            self.load.stopAnimating()
                        }
                }
            })
        } else {
            getMain()
        }
        */
    }
    
    func getMain() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        self.btnRefresh.isHidden = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        self.btnRefresh.isHidden = true
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                //let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                //let destination = storyboard.instantiateViewController(withIdentifier: "story_main") as! Home
                                
                                //self.navigationController?.pushViewController(destination, animated: true)
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_main")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                self.btnRefresh.isHidden = false
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @IBAction func goRefresh(_ sender: Any) {
        self.btnRefresh.isHidden = true
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        self.btnRefresh.isHidden = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        self.btnRefresh.isHidden = true
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                self.btnRefresh.isHidden = false
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
}
