//
//  Login.swift
//  Andalanku
//
//  Created by Handoyo on 29/03/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import LocalAuthentication
import Material
import Alamofire
import SwiftyJSON

class Login: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var navBar: UINavigationBar!
    var iconClick = true
    
    @IBOutlet var txt_email: TextField!
    @IBOutlet var txt_password: TextField!
    @IBOutlet var imgEyes: UIImageView!
    @IBOutlet var imgMain: UIImageView!
    @IBOutlet var imgFinger: UIImageView!
    @IBOutlet var lblBuatAkun: UILabel!
    @IBOutlet weak var lblLupaKataSandi: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.txt_email.delegate = self
        self.txt_password.delegate = self
        
        let tapImg = UITapGestureRecognizer(target:self, action:#selector(Login.goImgTap(_:)))
        self.imgEyes.isUserInteractionEnabled = true
        self.imgEyes.addGestureRecognizer(tapImg)
        
        let tapMain = UITapGestureRecognizer(target:self, action:#selector(Login.goMainTap(_:)))
        self.imgMain.isUserInteractionEnabled = true
        self.imgMain.addGestureRecognizer(tapMain)
        
        let tapFinger = UITapGestureRecognizer(target:self, action:#selector(Login.goFingerTap(_:)))
        self.imgFinger.isUserInteractionEnabled = true
        self.imgFinger.addGestureRecognizer(tapFinger)
        
        let tapAkun = UITapGestureRecognizer(target:self, action:#selector(Login.goAkunTap(_:)))
        self.lblBuatAkun.isUserInteractionEnabled = true
        self.lblBuatAkun.addGestureRecognizer(tapAkun)
        
        let tapLupa = UITapGestureRecognizer(target:self, action:#selector(Login.goLupaTap(_:)))
        self.lblLupaKataSandi.isUserInteractionEnabled = true
        self.lblLupaKataSandi.addGestureRecognizer(tapLupa)
        
        self.txt_email.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_email.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_email.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_email.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_password.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_password.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_password.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_password.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        self.load.isHidden = true
    }
    
    @objc func goImgTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            if(self.iconClick == true) {
                self.txt_password.isSecureTextEntry = false
                self.imgEyes.image = UIImage(named: "icon_mata")
            } else {
                self.txt_password.isSecureTextEntry = true
                self.imgEyes.image = UIImage(named: "icon_mata")
            }
            self.iconClick = !self.iconClick
        })
    }
    
    @objc func goMainTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_email.resignFirstResponder()
            self.txt_password.resignFirstResponder()
        })
    }
    
    @objc func goFingerTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let myContext = LAContext()
            let myLocalizedReasonString = "Biometric Authentication ANDALANku !! "
            myContext.localizedFallbackTitle = ""
            
            var authError: NSError?
            if #available(iOS 8.0, macOS 10.12.1, *) {
                if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                    myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                        
                        DispatchQueue.main.async {
                            if success {
                                let prefs:UserDefaults = UserDefaults.standard
                                let api_key = prefs.value(forKey: "api_key") as? String
                                let token_number = prefs.value(forKey: "token_number") as? String
                                let email = prefs.value(forKey: "email") as? String
                                let password = prefs.value(forKey: "password") as? String
                                
                                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                                self.load.isHidden = false
                                self.load.startAnimating()
                                
                                let headers = [
                                    "api_key": api_key,
                                    "token_number": token_number
                                ]
                                
                                let parameters = [
                                    "email": email!,
                                    "password": password!
                                    ] as [String : Any]
                                
                                let appConfig : app_config = app_config()
                                Alamofire.request(appConfig.getApiUrl(type: "login"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                                    .responseJSON { response in
                                        
                                        if (response.result.value == nil) {
                                            self.load.isHidden = true
                                            self.load.stopAnimating()
                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                            }
                                            alertController.addAction(OKAction)
                                            self.present(alertController, animated: true, completion:nil)
                                        } else {
                                            if let value = response.result.value {
                                                let json = JSON(value)
                                                print(json)
                                                let status = json["status"].stringValue
                                                let id_customer = json["id_customer"].stringValue
                                                let customer_name = json["customer_name"].stringValue
                                                let ktp = json["id_number"].stringValue
                                                let email = json["email"].stringValue
                                                if status == "success" {
                                                    let prefs:UserDefaults = UserDefaults.standard
                                                    prefs.set(id_customer, forKey: "id_customer")
                                                    prefs.set(email, forKey: "email")
                                                    prefs.set(ktp, forKey: "ktp")
                                                    prefs.set(customer_name, forKey: "nama_akun")
                                                    prefs.set("login", forKey: "set_otp")
                                                    prefs.set(self.txt_password.text!, forKey: "password")
                                                    prefs.synchronize()
                                                    
                                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                                    vc.modalPresentationStyle = .fullScreen
                                                    self.present(vc, animated: true, completion: nil)
                                                } else {
                                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                                    }
                                                    alertController.addAction(OKAction)
                                                    self.present(alertController, animated: true, completion:nil)
                                                }
                                            }
                                            self.load.isHidden = true
                                            self.load.stopAnimating()
                                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                        }
                                }
                            } else {
                                let alertController = UIAlertController(title: "", message: "Autentikasi Gagal!", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                } else {
                    print("Sorry!!.. Could not evaluate policy.")
                }
            } else {
                print("Ooops!!.. This feature is not supported.")
            }
        })
    }
    
    @objc func goAkunTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_register")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goLupaTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_lupa_kata_sandi")
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goLogin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.setLogin()
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == self.txt_email) {
            self.txt_password.becomeFirstResponder()
        } else if (textField == self.txt_password) {
            setLogin()
        }
        return true
    }
    
    func setLogin() {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.load.isHidden = false
            self.load.startAnimating()
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "email": self.txt_email.text!,
                "password": self.txt_password.text!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "login"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            let id_customer = json["id_customer"].stringValue
                            let customer_name = json["customer_name"].stringValue
                            let ktp = json["id_number"].stringValue
                            let email = json["email"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set(id_customer, forKey: "id_customer")
                                prefs.set(email, forKey: "email")
                                prefs.set(ktp, forKey: "ktp")
                                prefs.set(customer_name, forKey: "nama_akun")
                                prefs.set("login", forKey: "set_otp")
                                prefs.synchronize()
                             
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            self.present(vc, animated: true, completion: nil)
        })
    }
}
