//
//  LupaKataSandi.swift
//  Andalanku
//
//  Created by Handoyo on 03/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON

class LupaKataSandi: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lblBuatAkun: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtEmail: TextField!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        let tapAkun = UITapGestureRecognizer(target:self, action:#selector(LupaKataSandi.goAkunTap(_:)))
        self.lblBuatAkun.isUserInteractionEnabled = true
        self.lblBuatAkun.addGestureRecognizer(tapAkun)
        self.txtEmail.delegate = self
        
        let tapImgMain = UITapGestureRecognizer(target:self, action:#selector(LupaKataSandi.goImgMainTap(_:)))
        self.imgMain.isUserInteractionEnabled = true
        self.imgMain.addGestureRecognizer(tapImgMain)
        self.load.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == self.txtEmail) {
            self.txtEmail.resignFirstResponder()
        }
        return true
    }
    
    @objc func goAkunTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_register")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goImgMainTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txtEmail.resignFirstResponder()
        })
    }
    
    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "email": self.txtEmail.text!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "reset_password"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("lupa", forKey: "set_katasandi")
                                prefs.synchronize()
                                
                                let alertController = UIAlertController(title: "Informasi", message: "Panduan dan reset kata sandi berhasil dikirim ke email " + self.txtEmail.text!, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                     let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                                    vc.modalPresentationStyle = .fullScreen
                                     self.present(vc, animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
