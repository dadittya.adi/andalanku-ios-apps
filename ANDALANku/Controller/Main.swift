//
//  Main.swift
//  Andalanku
//
//  Created by Handoyo on 19/03/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import LocalAuthentication
import SPStorkController

class Main: UIViewController {
    
    @IBOutlet var imgPromo: UIImageView!
    @IBOutlet var imgPembiayaan: UIImageView!
    @IBOutlet var imgBerita: UIImageView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapImgPromo = UITapGestureRecognizer(target:self, action:#selector(Main.goImgPromoTap(_:)))
        self.imgPromo.isUserInteractionEnabled = true
        self.imgPromo.addGestureRecognizer(tapImgPromo)
        
        let tapImgPembiayaan = UITapGestureRecognizer(target:self, action:#selector(Main.goImgPembiayaanTap(_:)))
        self.imgPembiayaan.isUserInteractionEnabled = true
        self.imgPembiayaan.addGestureRecognizer(tapImgPembiayaan)
        
        let tapImgBerita = UITapGestureRecognizer(target:self, action:#selector(Main.goImgBeritaTap(_:)))
        self.imgBerita.isUserInteractionEnabled = true
        self.imgBerita.addGestureRecognizer(tapImgBerita)
    }
    
    @objc func goImgPromoTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("main", forKey: "set_promo")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goImgPembiayaanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("main", forKey: "set_pembiayaan")
            prefs.synchronize()
            let controller = ModalPengajuanPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 320
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goImgBeritaTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("main", forKey: "set_berita")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_berita")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func btnBiometricLoginClicked(_ sender: Any) {
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

@IBDesignable
class DesignableView: UIView {
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
