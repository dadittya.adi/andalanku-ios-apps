//
//  ModalCarList.swift
//  Andalanku
//
//  Created by Handoyo on 18/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import SPFakeBar
import Alamofire
import SwiftyJSON

class ModalCarList: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let navBar = SPFakeBarView(style: .stork)
    let tableView = UITableView()
    private var data = [""]
    var arrName = [String]()
    var arrPrice = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.contentInset.top = self.navBar.height
        self.tableView.scrollIndicatorInsets.top = self.navBar.height
        self.view.addSubview(self.tableView)
        
        self.navBar.titleLabel.text = "Pilih Tipe Mobil"
        self.navBar.leftButton.setTitle("X", for: .normal)
        self.navBar.leftButton.setTitleColor(UIColor.black, for: .normal)
        self.navBar.leftButton.tintColor = UIColor.black
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        self.updateLayout(with: self.view.frame.size)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_car_type = prefs.value(forKey: "id_car_type") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_car_type": id_car_type!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "car_trim_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            for item in json["car_trim_list"].arrayValue {
                                self.arrPrice.append(item["price"].stringValue)
                                self.arrName.append(item["name"].stringValue)
                            }
                        }
                        self.tableView.reloadData()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func updateLayout(with size: CGSize) {
        self.tableView.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = arrName[indexPath.row]
        cell.transform = .identity
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let trim_price = arrPrice[indexPath.row]
        let trim_name = arrName[indexPath.row]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(trim_price, forKey: "trim_price")
        prefs.set(trim_name, forKey: "trim_name")
        prefs.synchronize()
        
        if let presenter = presentingViewController as? PilihMobilDetail {
            presenter.txt_premium.text = trim_name
            presenter.txtOTR.text = trim_price
            presenter.setOTR()
        }
        dismissAction()
        
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "story_mobil_detail")
        vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
        */
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            SPStorkController.scrollViewDidScroll(scrollView)
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
