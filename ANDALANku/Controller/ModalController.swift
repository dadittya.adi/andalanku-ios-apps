import UIKit
import SPStorkController

class ModalController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        let title = UILabel()
        title.text = "E-SERVICES ANDALANKU"
        self.view.addSubview(title)
        title.textAlignment = .center
        title.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute:NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 20)
        let verticalConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 30)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
        
        //tambah garis
        let screenSize: CGRect = UIScreen.main.bounds
        let headerLine = UIView(frame: CGRect(x: 0, y: 63, width: screenSize.width - 0, height: 1))
        self.view.addSubview(headerLine)
        headerLine.layer.backgroundColor = UIColor.lightGray.cgColor
        
        //tambah image
        //let image = UIImage(named: "img_menu")
        let image1 = UIImage(named: "icon_ecomplain_home")
        let image2 = UIImage(named: "icon_eclaim_home")
        let image3 = UIImage(named: "icon_erequest_home")
        
        let imgMenu1 = UIImageView(image: image1!)
        imgMenu1.frame = CGRect(x: 52.5, y: 76, width: 50, height: 50)
        imgMenu1.contentMode = .scaleAspectFit
        self.view.addSubview(imgMenu1)
        
        imgMenu1.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst1 = NSLayoutConstraint(item: imgMenu1, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConst1 = NSLayoutConstraint(item: imgMenu1, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0.0)
        
        let heightConstraint1 = NSLayoutConstraint(item: imgMenu1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint1 = NSLayoutConstraint(item: imgMenu1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu1.addConstraints([heightConstraint1, widthConstraint1])
        
        NSLayoutConstraint.activate([centerXConst1, centerYConst1])
        
        let tapImgEServices = UITapGestureRecognizer(target:self, action:#selector(ModalController.goEServicesTap(_:)))
        imgMenu1.isUserInteractionEnabled = true
        imgMenu1.addGestureRecognizer(tapImgEServices)
        
        let imgMenu2 = UIImageView(image: image2!)
        imgMenu2.frame = CGRect(x: 152.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu2)
        
        imgMenu2.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
        let centerYConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0.0)
        
        let heightConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu2.addConstraints([heightConstraint, widthConstraint])
        
        NSLayoutConstraint.activate([centerXConst, centerYConst])
        
        let tapImgClaim = UITapGestureRecognizer(target:self, action:#selector(ModalController.goClaimTap(_:)))
        imgMenu2.isUserInteractionEnabled = true
        imgMenu2.addGestureRecognizer(tapImgClaim)
        
        let imgMenu3 = UIImageView(image: image3!)
        imgMenu3.frame = CGRect(x: 252.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu3)
        
        imgMenu3.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst3 = NSLayoutConstraint(item: imgMenu3, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 95.0)
        let centerYConst3 = NSLayoutConstraint(item: imgMenu3, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 0.0)
        
        let heightConstraint3 = NSLayoutConstraint(item: imgMenu3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint3 = NSLayoutConstraint(item: imgMenu3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu3.addConstraints([heightConstraint3, widthConstraint3])
        
        NSLayoutConstraint.activate([centerXConst3, centerYConst3])
        
        let tapImgJaminan = UITapGestureRecognizer(target:self, action:#selector(ModalController.goJaminanTap(_:)))
        imgMenu3.isUserInteractionEnabled = true
        imgMenu3.addGestureRecognizer(tapImgJaminan)
        
        //tambah label menu
        let title1 = UILabel()
        title1.text = "E-Complain"
        title1.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title1)
        title1.textAlignment = .center
        title1.lineBreakMode = NSLineBreakMode.byWordWrapping
        title1.numberOfLines = 0
        title1.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle1 = NSLayoutConstraint(item: title1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle1 = NSLayoutConstraint(item: title1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle1 = NSLayoutConstraint(item: title1, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConsttitle1 = NSLayoutConstraint(item: title1, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 40.0)
        NSLayoutConstraint.activate([widthTitle1, heightTitle1, centerXConsttitle1, centerYConsttitle1])
        
        let title2 = UILabel()
        title2.text = "E-Claim Asuransi"
        title2.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title2)
        title2.textAlignment = .center
        title2.lineBreakMode = NSLineBreakMode.byWordWrapping
        title2.numberOfLines = 0
        title2.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle2 = NSLayoutConstraint(item: title2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle2 = NSLayoutConstraint(item: title2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
        let centerYConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 46.0)
        NSLayoutConstraint.activate([widthTitle2, heightTitle2, centerXConsttitle2, centerYConsttitle2])
        
        let title3 = UILabel()
        title3.text = "E-Request Ambil Jaminan"
        title3.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title3)
        title3.textAlignment = .center
        title3.lineBreakMode = NSLineBreakMode.byWordWrapping
        title3.numberOfLines = 0
        title3.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle3 = NSLayoutConstraint(item: title3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle3 = NSLayoutConstraint(item: title3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 95.0)
        let centerYConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 46.0)
        NSLayoutConstraint.activate([widthTitle3, heightTitle3, centerXConsttitle3, centerYConsttitle3])
        
        self.view.layoutIfNeeded()
    }
    
    @objc func goEServicesTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_e_complain_services")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goClaimTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_eclaim_asuransi")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goJaminanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_request_jaminan")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
