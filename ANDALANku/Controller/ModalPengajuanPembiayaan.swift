//
//  ModalPengajuanPembiayaan.swift
//  Andalanku
//
//  Created by Handoyo on 16/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController

class ModalPengajuanPembiayaan: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        let title = UILabel()
        title.text = "PENGAJUAN PEMBIAYAAN"
        self.view.addSubview(title)
        title.textAlignment = .center
        title.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute:NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 20)
        let verticalConstraint = NSLayoutConstraint(item: title, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 30)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
        
        //tambah garis
        let screenSize: CGRect = UIScreen.main.bounds
        let headerLine = UIView(frame: CGRect(x: 0, y: 63, width: screenSize.width - 0, height: 1))
        self.view.addSubview(headerLine)
        headerLine.layer.backgroundColor = UIColor.lightGray.cgColor
        
        //tambah image
        //let image = UIImage(named: "img_menu")
        let image1 = UIImage(named: "icon_new_cars_new")
        let image2 = UIImage(named: "icon_used_cars_new")
        let image3 = UIImage(named: "icon_dana_new")
        let image4 = UIImage(named: "icon_ecommerce_new")
        
        let imgMenu1 = UIImageView(image: image1!)
        imgMenu1.frame = CGRect(x: 52.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu1)
        
        imgMenu1.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst1 = NSLayoutConstraint(item: imgMenu1, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConst1 = NSLayoutConstraint(item: imgMenu1, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: -35.0)
        
        let heightConstraint1 = NSLayoutConstraint(item: imgMenu1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint1 = NSLayoutConstraint(item: imgMenu1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu1.addConstraints([heightConstraint1, widthConstraint1])
        
        NSLayoutConstraint.activate([centerXConst1, centerYConst1])
        
        let tapImgNewCars = UITapGestureRecognizer(target:self, action:#selector(ModalPengajuanPembiayaan.goNewCarsTap(_:)))
        imgMenu1.isUserInteractionEnabled = true
        imgMenu1.addGestureRecognizer(tapImgNewCars)
        
        let imgMenu2 = UIImageView(image: image2!)
        imgMenu2.frame = CGRect(x: 152.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu2)
        
        imgMenu2.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
        let centerYConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: -35.0)
        
        let heightConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu2.addConstraints([heightConstraint, widthConstraint])
        
        NSLayoutConstraint.activate([centerXConst, centerYConst])
        
        let tapImgMenu2 = UITapGestureRecognizer(target:self, action:#selector(ModalPengajuanPembiayaan.goTitle2Tap(_:)))
        imgMenu2.isUserInteractionEnabled = true
        imgMenu2.addGestureRecognizer(tapImgMenu2)
        
        let imgMenu3 = UIImageView(image: image3!)
        imgMenu3.frame = CGRect(x: 252.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu3)
        
        imgMenu3.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst3 = NSLayoutConstraint(item: imgMenu3, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 95.0)
        let centerYConst3 = NSLayoutConstraint(item: imgMenu3, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: -35.0)
        
        let heightConstraint3 = NSLayoutConstraint(item: imgMenu3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint3 = NSLayoutConstraint(item: imgMenu3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu3.addConstraints([heightConstraint3, widthConstraint3])
        
        NSLayoutConstraint.activate([centerXConst3, centerYConst3])
        
        let tapImgMenu3 = UITapGestureRecognizer(target:self, action:#selector(ModalPengajuanPembiayaan.goTitle3Tap(_:)))
        imgMenu3.isUserInteractionEnabled = true
        imgMenu3.addGestureRecognizer(tapImgMenu3)
        
        let imgMenu4 = UIImageView(image: image4!)
        imgMenu4.frame = CGRect(x: 52.5, y: 76, width: 50, height: 50)
        self.view.addSubview(imgMenu4)
        
        imgMenu4.translatesAutoresizingMaskIntoConstraints = false
        let centerXConst4 = NSLayoutConstraint(item: imgMenu4, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConst4 = NSLayoutConstraint(item: imgMenu4, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 70.0)
        
        let heightConstraint4 = NSLayoutConstraint(item: imgMenu4, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        let widthConstraint4 = NSLayoutConstraint(item: imgMenu4, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        imgMenu4.addConstraints([heightConstraint4, widthConstraint4])
        
        NSLayoutConstraint.activate([centerXConst4, centerYConst4])
        
        let tapImgMenu4 = UITapGestureRecognizer(target:self, action:#selector(ModalPengajuanPembiayaan.goTitle4Tap(_:)))
        imgMenu4.isUserInteractionEnabled = true
        imgMenu4.addGestureRecognizer(tapImgMenu4)
        
        //tambah label menu
        let title1 = UILabel()
        title1.text = "New Cars"
        title1.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title1)
        title1.textAlignment = .center
        title1.lineBreakMode = NSLineBreakMode.byWordWrapping
        title1.numberOfLines = 0
        title1.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle1 = NSLayoutConstraint(item: title1, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle1 = NSLayoutConstraint(item: title1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle1 = NSLayoutConstraint(item: title1, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConsttitle1 = NSLayoutConstraint(item: title1, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 11.0)
        NSLayoutConstraint.activate([widthTitle1, heightTitle1, centerXConsttitle1, centerYConsttitle1])
        
        let title2 = UILabel()
        title2.text = "Used Cars"
        title2.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title2)
        title2.textAlignment = .center
        title2.lineBreakMode = NSLineBreakMode.byWordWrapping
        title2.numberOfLines = 0
        title2.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle2 = NSLayoutConstraint(item: title2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle2 = NSLayoutConstraint(item: title2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
        let centerYConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 11.0)
        NSLayoutConstraint.activate([widthTitle2, heightTitle2, centerXConsttitle2, centerYConsttitle2])
        
        let tapTitle2 = UITapGestureRecognizer(target:self, action:#selector(ModalPengajuanPembiayaan.goTitle2Tap(_:)))
        title2.isUserInteractionEnabled = true
        title2.addGestureRecognizer(tapTitle2)
        
        let title3 = UILabel()
        title3.text = "Dana ANDALANKU"
        title3.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title3)
        title3.textAlignment = .center
        title3.lineBreakMode = NSLineBreakMode.byWordWrapping
        title3.numberOfLines = 0
        title3.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle3 = NSLayoutConstraint(item: title3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle3 = NSLayoutConstraint(item: title3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 95.0)
        let centerYConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 15.0)
        NSLayoutConstraint.activate([widthTitle3, heightTitle3, centerXConsttitle3, centerYConsttitle3])
        
        let title4 = UILabel()
        title4.text = "E-Commerce"
        title4.font = UIFont(name: "Helvetica Neue", size: 12.0)
        self.view.addSubview(title4)
        title4.textAlignment = .center
        title4.lineBreakMode = NSLineBreakMode.byWordWrapping
        title4.numberOfLines = 0
        title4.translatesAutoresizingMaskIntoConstraints = false
        
        let widthTitle4 = NSLayoutConstraint(item: title4, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80.0)
        let heightTitle4 = NSLayoutConstraint(item: title4, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
        
        let centerXConsttitle4 = NSLayoutConstraint(item: title4, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: -95.0)
        let centerYConsttitle4 = NSLayoutConstraint(item: title4, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 110.0)
        NSLayoutConstraint.activate([widthTitle4, heightTitle4, centerXConsttitle4, centerYConsttitle4])
        
        self.view.layoutIfNeeded()
    }
    
    @objc func goNewCarsTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new_cars", forKey: "set_pembiayaan")
            prefs.synchronize()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_pembiayaan_new_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTitle2Tap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("", forKey: "no_mobil")
            prefs.set("", forKey: "no_tenor")
            prefs.set("", forKey: "no_premi")
            prefs.set("", forKey: "no_wilayah")
            prefs.set("", forKey: "hasil")
            prefs.set("978500000", forKey: "otr")
            prefs.set("20", forKey: "dp")
            prefs.set("used_cars", forKey: "set_pembiayaan")
            prefs.synchronize()
            
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "story_pembiayaan_used_cars")
            vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTitle3Tap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("dana", forKey: "set_pembiayaan")
            prefs.set("", forKey: "atas_nama")
            prefs.set("", forKey: "no_polisi")
            prefs.set("", forKey: "kendaraan")
            prefs.set("", forKey: "kebutuhan_dana")
            prefs.synchronize()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_dana")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTitle4Tap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_e_commerce")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
