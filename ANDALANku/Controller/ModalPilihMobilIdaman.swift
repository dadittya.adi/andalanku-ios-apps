//
//  ModalPilihMobilIdaman.swift
//  Andalanku
//
//  Created by Handoyo on 08/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import SPFakeBar
import Alamofire
import SwiftyJSON

class ModalPilihMobilIdaman: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, CustomSearchControllerDelegate {
    
    let navBar = SPFakeBarView(style: .stork)
    let tableView = UITableView()
    var filteredArray = [String]()
    //private var data = ["Toyota NEW KIJANG INNOVA 24G", "Toyota AVANZA 2018", "Toyota ALPHART", "Honda CITY", "Honda VIOS"]
    var data = [String]()
    var harga = [String]()
    var shouldShowSearchResults = false
    var searchController: UISearchController!
    var customSearchController: CustomSearchController!
    var tahun:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.contentInset.top = self.navBar.height
        self.tableView.scrollIndicatorInsets.top = self.navBar.height
        self.tableView.keyboardDismissMode = .onDrag
        self.view.addSubview(self.tableView)
        
        let prefs:UserDefaults = UserDefaults.standard
        let list_used_car = prefs.value(forKey: "list_used_car") as? String
        if (list_used_car == "data_agunan") {
            self.navBar.titleLabel.text = "Unit Kendaraan"
        } else {
            self.navBar.titleLabel.text = "Mobil apa yang anda inginkan ?"
        }
        self.navBar.leftButton.setTitle("X", for: .normal)
        self.navBar.leftButton.setTitleColor(UIColor.black, for: .normal)
        self.navBar.leftButton.tintColor = UIColor.black
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number,
                "id_customer": id_customer
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "year": "2017",
                "keyword": ""
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "used_car_price"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            for item in json["car_price_list"].arrayValue {
                                self.data.append(item["AssetName"].stringValue)
                            }
                        }
                        self.tableView.reloadData()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
        
        self.configureCustomSearchController()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Cari mobil idaman..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        tableView.tableHeaderView = searchController.searchBar
    }
    
    
    func configureCustomSearchController() {
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: 50.0), searchBarFont: UIFont(name: "Helvetica Neue", size: 16.0)!, searchBarTextColor: UIColor.darkGray, searchBarTintColor: UIColor.white) //UIColor(rgb: 0x5BC57A))
        
        let prefs:UserDefaults = UserDefaults.standard
        let list_used_car = prefs.value(forKey: "list_used_car") as? String
        if (list_used_car == "data_agunan") {
            customSearchController.customSearchBar.placeholder = "Cari unit kendaraan..."
        } else {
            customSearchController.customSearchBar.placeholder = "Cari mobil idaman..."
        }
        tableView.tableHeaderView = customSearchController.customSearchBar
        
        customSearchController.customDelegate = self
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tableView.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tableView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        /*
        filteredArray = data.filter({ (origin) -> Bool in
            let originText:NSString = origin as NSString
            
            return (originText.range(of: searchString, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        tableView.reloadData()
        */
        let prefs:UserDefaults = UserDefaults.standard
        let list_used_car = prefs.value(forKey: "list_used_car") as? String
        if (list_used_car == "data_agunan") {
            if let presenter = presentingViewController as? DataAgunan {
                tahun = presenter.txtTahun.text!
            }
        } else {
            if let presenter = presentingViewController as? PembiayaanUsedCars {
                tahun = presenter.txtTahun.text!
            }
        }
        
        print(tahun)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number,
                "id_customer": id_customer
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "year": self.tahun,
                "keyword": searchString
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "used_car_price"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            self.filteredArray.removeAll()
                            self.harga.removeAll()
                            for item in json["car_price_list"].arrayValue {
                                self.filteredArray.append(item["AssetName"].stringValue)
                                self.harga.append(item["Price"].stringValue)
                            }
                        }
                        //self.tableView.reloadData()
                        if self.filteredArray.count == 0 {
                            self.filteredArray.append("Data Tidak Ditemukan")
                            self.harga.append("0")
                            self.tableView.reloadData()
                        } else {
                            self.tableView.reloadData()
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    func didStartSearching() {
        shouldShowSearchResults = true
        tableView.reloadData()
    }
    
    
    func didTapOnSearchButton() {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tableView.reloadData()
        }
    }
    
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        tableView.reloadData()
    }
    
    
    func didChangeSearchText(searchText: String) {
        /*
        filteredArray = data.filter({ (origin) -> Bool in
            let originText:NSString = origin as NSString
            return (originText.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        tableView.reloadData()
        */
        
        let prefs:UserDefaults = UserDefaults.standard
        let list_used_car = prefs.value(forKey: "list_used_car") as? String
        if (list_used_car == "data_agunan") {
            if let presenter = presentingViewController as? DataAgunan {
                tahun = presenter.txtTahun.text!
            }
        } else {
            if let presenter = presentingViewController as? PembiayaanUsedCars {
                tahun = presenter.txtTahun.text!
            }
        }
        
        print(tahun)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number,
                "id_customer": id_customer
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "year": self.tahun,
                "keyword": searchText
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "used_car_price"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            self.filteredArray.removeAll()
                            self.harga.removeAll()
                            for item in json["car_price_list"].arrayValue {
                                self.filteredArray.append(item["AssetName"].stringValue)
                                self.harga.append(item["Price"].stringValue)
                            }
                        }
                        //self.tableView.reloadData()
                        if self.filteredArray.count == 0 {
                            self.filteredArray.append("Data Tidak Ditemukan")
                            self.harga.append("0")
                            self.tableView.reloadData()
                        } else {
                            self.tableView.reloadData()
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func updateLayout(with size: CGSize) {
        self.tableView.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        //cell.textLabel?.text = data[indexPath.row]
        //cell.transform = .identity
        
        if shouldShowSearchResults {
            cell.textLabel?.text = filteredArray[indexPath.row]
            cell.transform = .identity
        } else {
            cell.textLabel?.text = data[indexPath.row]
            cell.transform = .identity
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            return data.count
        }
        //return data.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var nama:String = ""
        
        if self.shouldShowSearchResults {
            nama = filteredArray[indexPath.row]
        } else {
            nama = data[indexPath.row]
        }
        
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(nama, forKey: "no_mobil")
        prefs.synchronize()
        
        let list_used_car = prefs.value(forKey: "list_used_car") as? String
        
        if (list_used_car == "data_agunan") {
            if let presenter = presentingViewController as? DataAgunan {
                presenter.txtKendaraan.text = nama
                let valString:String = harga[indexPath.row]
                let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
                
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "id_ID")
                formatter.groupingSeparator = "."
                formatter.numberStyle = .decimal
                if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
                    presenter.txtHarga.text = formattedTipAmount
                }
            }
            dismissAction()
        } else {
            if let presenter = presentingViewController as? PembiayaanUsedCars {
                presenter.txtMobilIdaman.text = nama
                presenter.txtDP.text = "20"
                
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "id_ID")
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: Int(harga[indexPath.row] as String)! as NSNumber) {
                    //cell.lblPrice.text = formattedTipAmount + ",00"
                    presenter.getHarga = harga[indexPath.row]
                    presenter.txtHarga.text = formattedTipAmount
                }
                
                
                presenter.setOTR()
            }
            dismissAction()
        }
        
        /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "story_pembiayaan_used_cars")
        vc.modalPresentationStyle = .fullScreen
         self.present(vc, animated: true, completion: nil)
        */
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            SPStorkController.scrollViewDidScroll(scrollView)
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }

}
