//
//  ModalPilihNoKontrak.swift
//  Andalanku
//
//  Created by Handoyo on 16/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import SPFakeBar
import Alamofire
import SwiftyJSON

class ModalPilihNoKontrak: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let navBar = SPFakeBarView(style: .stork)
    let tableView = UITableView()
    let load = UIActivityIndicatorView(style: .whiteLarge)
    //private var data = ["ADN1298764817", "ADN1237531114", "ADN1438764812", "ADN1528364819", "ADN1778264813"]
    var arrDict: NSMutableArray = []
    var data = [String]()
    var sisa = [String]()
    var insurance = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.contentInset.top = self.navBar.height
        self.tableView.scrollIndicatorInsets.top = self.navBar.height
        self.view.addSubview(self.tableView)
        
        self.navBar.titleLabel.text = "Pilih No Kontrak"
        self.navBar.leftButton.setTitle("X", for: .normal)
        self.navBar.leftButton.setTitleColor(UIColor.black, for: .normal)
        self.navBar.leftButton.tintColor = UIColor.black
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        load.frame = CGRect(x: 0, y: 50, width: 80, height: 80)
        load.backgroundColor = .clear
        load.color = UIColor(rgb: 0xF1853B)
        //load.center = self.view.center
        load.center.x = self.view.center.x
        
        self.view.addSubview(load)
        
        self.updateLayout(with: self.view.frame.size)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.load.isHidden = false
            self.load.startAnimating()
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "agreement_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            //var i:Int = 0
                            self.data.removeAll()
                            self.insurance.removeAll()
                            self.sisa.removeAll()
                            /*
                            for item in json["agreement_list"].arrayValue {
                                print(item)
                                self.data.append(json["agreement_list"][i].string!)
                                let strList:String = json["agreement_list"][i].string!
                                let klaim:String = json["insurance_category_list"][strList].string!
                                self.insurance.append(klaim)
                                i = i + 1
                                
                            }
                            */
                            for item in json["agreement_list"].arrayValue {
                                self.data.append(item["agreement_no"].stringValue)
                                self.insurance.append(item["insurance_category"].stringValue)
                                self.sisa.append(item["sisa_pinjaman"].stringValue)
                            }
                        }
                        
                        if self.data.count == 0 {
                            self.tableView.isHidden = true
                            
                            //tambah image
                            let image2 = UIImage(named: "icon_no_kontrak")
                            
                            let imgMenu2 = UIImageView(image: image2!)
                            imgMenu2.frame = CGRect(x: 152.5, y: 76, width: 50, height: 90)
                            self.view.addSubview(imgMenu2)
                            
                            imgMenu2.translatesAutoresizingMaskIntoConstraints = false
                            let centerXConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
                            let centerYConst = NSLayoutConstraint(item: imgMenu2, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: -20.0)
                            
                            let heightConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
                            let widthConstraint = NSLayoutConstraint(item: imgMenu2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
                            imgMenu2.addConstraints([heightConstraint, widthConstraint])
                            NSLayoutConstraint.activate([centerXConst, centerYConst])
                            
                            let title2 = UILabel()
                            title2.text = "Tidak Ada Nomor Kontrak"
                            title2.font = UIFont(name: "Helvetica Neue", size: 16.0)
                            self.view.addSubview(title2)
                            title2.textAlignment = .center
                            title2.lineBreakMode = NSLineBreakMode.byWordWrapping
                            title2.numberOfLines = 0
                            title2.translatesAutoresizingMaskIntoConstraints = false
                            
                            let widthTitle2 = NSLayoutConstraint(item: title2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 240.0)
                            let heightTitle2 = NSLayoutConstraint(item: title2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
                            
                            let centerXConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
                            let centerYConsttitle2 = NSLayoutConstraint(item: title2, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 56.0)
                            NSLayoutConstraint.activate([widthTitle2, heightTitle2, centerXConsttitle2, centerYConsttitle2])
                            
                            let title3 = UILabel()
                            title3.text = "Nikmati kemudahan pembiayaan dalam satu aplikasi"
                            title3.font = UIFont(name: "Helvetica Neue", size: 12.0)
                            title3.textColor = UIColor.darkGray
                            self.view.addSubview(title3)
                            title3.textAlignment = .center
                            title3.lineBreakMode = NSLineBreakMode.byWordWrapping
                            title3.numberOfLines = 0
                            title3.translatesAutoresizingMaskIntoConstraints = false
                            
                            let widthTitle3 = NSLayoutConstraint(item: title3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 180.0)
                            let heightTitle3 = NSLayoutConstraint(item: title3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50.0)
                            
                            let centerXConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.0, constant: 0.0)
                            let centerYConsttitle3 = NSLayoutConstraint(item: title3, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.0, constant: 86.0)
                            NSLayoutConstraint.activate([widthTitle3, heightTitle3, centerXConsttitle3, centerYConsttitle3])
                            
                            self.view.layoutIfNeeded()
                            
                        } else {
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                        }
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func updateLayout(with size: CGSize) {
        self.tableView.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row]
        cell.transform = .identity
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let no_kontrak = data[indexPath.row]
        let sisa_pinjam = sisa[indexPath.row]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(no_kontrak, forKey: "no_kontrak")
        prefs.set(sisa_pinjam, forKey: "sisa_pinjam")
        prefs.synchronize()
        
        let set_no_kontrak = prefs.value(forKey: "set_no_kontrak") as? String
        if (set_no_kontrak != "" && set_no_kontrak != nil) {
            if (set_no_kontrak == "angsuran") {
                if let presenter = presentingViewController as? PembayaranAngsuran {
                    presenter.txtNoKontrak.text = no_kontrak
                }
                dismissAction()
                /*
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "story_pembayaran_angsuran")
                 vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                */
            } else if (set_no_kontrak == "cash") {
                if let presenter = presentingViewController as? CashValue {
                    presenter.txtNoKontrak.text = no_kontrak
                }
                dismissAction()
                /*
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "story_cash_value")
                 vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                */
            } else if (set_no_kontrak == "jadwal") {
                if let presenter = presentingViewController as? JadwalAngsuran {
                    presenter.txtNoKontrak.text = no_kontrak
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "story_detail_jadwal")
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                //dismissAction()
            } else if (set_no_kontrak == "dokumen") {
                if let presenter = presentingViewController as? DokumenKontrak {
                    presenter.txtNoKontrak.text = no_kontrak
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.load.isHidden = false
                        self.load.startAnimating()
                        
                        let prefs:UserDefaults = UserDefaults.standard
                        let api_key = prefs.value(forKey: "api_key") as? String
                        let token_number = prefs.value(forKey: "token_number") as? String
                        let id_customer = prefs.value(forKey: "id_customer") as? String
                        
                        let headers = [
                            "api_key": api_key,
                            "token_number": token_number
                        ]
                        
                        let parameters = [
                            "agreement_no": no_kontrak,
                            "id_customer": id_customer!
                            ] as [String : Any]
                        
                        let appConfig : app_config = app_config()
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            let dtString:String = json["maturity_date"].stringValue
                                            let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            
                                            let dateFormatterPrint = DateFormatter()
                                            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                            
                                            if let date = dateFormatterGet.date(from: dateString) {
                                                let tanggal_kredit_terakhir:String = dateFormatterPrint.string(from: date)
                                                let prefs:UserDefaults = UserDefaults.standard
                                                prefs.set(tanggal_kredit_terakhir, forKey: "tanggal_kredit_terakhir")
                                                prefs.synchronize()
                                            } else {
                                                let prefs:UserDefaults = UserDefaults.standard
                                                prefs.set(json["maturity_date"].stringValue, forKey: "tanggal_kredit_terakhir")
                                                prefs.synchronize()
                                            }
                                        }
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                        }
                    })
                }
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "story_detail_kontrak")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else if (set_no_kontrak == "pelunasan") {
                if let presenter = presentingViewController as? SimulasiPelunasan {
                    presenter.txtNoKontrak.text = no_kontrak
                }
                //dismissAction()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "story_pelunasan_maju")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else if (set_no_kontrak == "detail") {
                if let presenter = presentingViewController as? DetailJadwalAngsuran {
                    presenter.txtNoKontrak.text = no_kontrak
                    presenter.txtSisaPinjam.text = "Rp. " + sisa_pinjam
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.load.isHidden = false
                        self.load.startAnimating()
                        
                        let prefs:UserDefaults = UserDefaults.standard
                        let api_key = prefs.value(forKey: "api_key") as? String
                        let token_number = prefs.value(forKey: "token_number") as? String
                        let id_customer = prefs.value(forKey: "id_customer") as? String
                        
                        let headers = [
                            "api_key": api_key,
                            "token_number": token_number
                        ]
                        
                        let parameters = [
                            "agreement_no": no_kontrak,
                            "id_customer": id_customer!
                            ] as [String : Any]
                        
                        let appConfig : app_config = app_config()
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            //self.txtSisaPinjam.text = "Rp. " + json["installment_amount"].stringValue
                                            
                                            let dtString:String = json["maturity_date"].stringValue
                                            let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            
                                            let dateFormatterPrint = DateFormatter()
                                            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                            
                                            if let date = dateFormatterGet.date(from: dateString) {
                                                presenter.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                            } else {
                                                presenter.txtJatuhTempo.text = json["maturity_date"].stringValue
                                            }
                                        }
                                    }
                                    
                                }
                        }
                        
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_payment_history"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            presenter.urlDownload = json["kartu_piutang"].stringValue
                                            
                                            presenter.webPreview.scrollView.isScrollEnabled = true
                                            presenter.self.webPreview.scrollView.bounces = false
                                            presenter.webPreview.scrollView.isPagingEnabled = true
                                            presenter.webPreview.isUserInteractionEnabled = true
                                            
                                            let urlfull:String = presenter.urlDownload
                                            presenter.webPreview.loadRequest(URLRequest(url: URL(string: urlfull)!))
                                            
                                        } else {
                                            //self.lblStatus.text = json["message"].stringValue
                                        }
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                        }
                    })
                }
                dismissAction()
                /*
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "story_jadwal_angsuran")
                 vc.modalPresentationStyle = .fullScreen
                 self.present(vc, animated: true, completion: nil)
                 */
            } else if (set_no_kontrak == "detail_kontrak") {
                if let presenter = presentingViewController as? DetailKontrak {
                    presenter.txtNoKontrak.text = no_kontrak
                    
                    /*
                    DispatchQueue.main.async(execute: {
                        
                        self.load.isHidden = false
                        self.load.startAnimating()
                        
                        let prefs:UserDefaults = UserDefaults.standard
                        let api_key = prefs.value(forKey: "api_key") as? String
                        let token_number = prefs.value(forKey: "token_number") as? String
                        let id_customer = prefs.value(forKey: "id_customer") as? String
                        
                        let headers = [
                            "api_key": api_key,
                            "token_number": token_number
                        ]
                        
                        let parameters = [
                            "agreement_no": no_kontrak,
                            "id_customer": id_customer!
                            ] as [String : Any]
                        
                        let appConfig : app_config = app_config()
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_detail_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            let dtString:String = json["maturity_date"].stringValue
                                            let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            
                                            let dateFormatterPrint = DateFormatter()
                                            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                            
                                            if let date = dateFormatterGet.date(from: dateString) {
                                                presenter.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                            } else {
                                                presenter.txtJatuhTempo.text = json["maturity_date"].stringValue
                                            }
                                        }
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                        }
                    })
                    */
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.load.isHidden = false
                        self.load.startAnimating()
                        
                        let prefs:UserDefaults = UserDefaults.standard
                        let api_key = prefs.value(forKey: "api_key") as? String
                        let token_number = prefs.value(forKey: "token_number") as? String
                        let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                        let id_customer = prefs.value(forKey: "id_customer") as? String
                        
                        let headers = [
                            "api_key": api_key,
                            "token_number": token_number
                        ]
                        
                        let parameters = [
                            "agreement_no": no_kontrak!,
                            "id_customer": id_customer!
                            ] as [String : Any]
                        
                        let appConfig : app_config = app_config()
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            for item in json["andalan_detail_financial"].arrayValue {
                                                let dtString:String = item["maturity_date"].stringValue
                                                let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                                
                                                let dateFormatterGet = DateFormatter()
                                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                                
                                                let dateFormatterPrint = DateFormatter()
                                                dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                                
                                                if let date = dateFormatterGet.date(from: dateString) {
                                                    presenter.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                                } else {
                                                    presenter.txtJatuhTempo.text = item["maturity_date"].stringValue
                                                }
                                                
                                                let otString:String = item["otr"].stringValue
                                                let otrString:String = otString.replacingOccurrences(of: ".", with: "")
                                                
                                                let formatter = NumberFormatter()
                                                formatter.locale = Locale(identifier: "id_ID")
                                                formatter.groupingSeparator = "."
                                                formatter.numberStyle = .decimal
                                                if let formattedTipAmount = formatter.string(from: Int(otrString)! as NSNumber) {
                                                    presenter.lblOTR.text = "Rp. " + formattedTipAmount
                                                }
                                                
                                                let dpString:String = item["down_payment"].stringValue
                                                let downString:String = dpString.replacingOccurrences(of: ".", with: "")
                                                
                                                formatter.locale = Locale(identifier: "id_ID")
                                                formatter.groupingSeparator = "."
                                                formatter.numberStyle = .decimal
                                                if let formattedTipAmount = formatter.string(from: Int(downString)! as NSNumber) {
                                                    presenter.lblUangMuka.text = "Rp. " + formattedTipAmount
                                                }
                                                
                                                presenter.lblAsuransi.text = "TLO"
                                                
                                                let netString:String = item["net_finance"].stringValue
                                                let netFinString:String = netString.replacingOccurrences(of: ".", with: "")
                                                
                                                formatter.locale = Locale(identifier: "id_ID")
                                                formatter.groupingSeparator = "."
                                                formatter.numberStyle = .decimal
                                                if let formattedTipAmount = formatter.string(from: Int(netFinString)! as NSNumber) {
                                                    presenter.lblNilaiBiaya.text = "Rp. " + formattedTipAmount
                                                }
                                                
                                                let agString:String = item["agreement_date"].stringValue
                                                let agreementString:String = agString.replacingOccurrences(of: "T", with: " ")
                                                
                                                if let date = dateFormatterGet.date(from: agreementString) {
                                                    presenter.lblTanggalKontrak.text = dateFormatterPrint.string(from: date)
                                                } else {
                                                    presenter.lblTanggalKontrak.text = item["agreement_date"].stringValue
                                                }
                                                
                                                let efString:String = item["effective_date"].stringValue
                                                let effectiveString:String = efString.replacingOccurrences(of: "T", with: " ")
                                                
                                                if let date = dateFormatterGet.date(from: effectiveString) {
                                                    presenter.lblTanggalBerlaku.text = dateFormatterPrint.string(from: date)
                                                } else {
                                                    presenter.lblTanggalBerlaku.text = item["effective_date"].stringValue
                                                }
                                                
                                                let maString:String = item["maturity_date"].stringValue
                                                let maturityString:String = maString.replacingOccurrences(of: "T", with: " ")
                                                
                                                if let date = dateFormatterGet.date(from: maturityString) {
                                                    presenter.lblJatuhTempo.text = dateFormatterPrint.string(from: date)
                                                } else {
                                                    presenter.lblJatuhTempo.text = item["maturity_date"].stringValue
                                                }
                                                
                                                presenter.lblTenor.text = item["tenor"].stringValue + " Bulan"
                                            }
                                            
                                            for item in json["andalan_detail_unit"].arrayValue {
                                                presenter.lblMerk.text = item["asset_description"].stringValue
                                                presenter.lblTahun.text = item["manufacturing_year"].stringValue
                                                presenter.lblWarna.text = item["colour"].stringValue
                                                presenter.lblNoPolisi.text = item["license_plate"].stringValue
                                                presenter.lblNoRangka.text = item["chasis_no"].stringValue
                                                presenter.lblNoMesin.text = item["engine_no"].stringValue
                                            }
                                            
                                            for item in json["andalan_detail_payment_history"].arrayValue {
                                                presenter.urlHistory = item["kartu_piutang"].stringValue
                                                presenter.lblHistory.text = "History payment available"
                                            }
                                            
                                            for item in json["andalan_detail_insurance"].arrayValue {
                                                presenter.urlPolis = item["kartu_piutang"].stringValue
                                                presenter.lblPolis.text = "Insurance detail not available"
                                            }
                                        }
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                        }
                    })
                    
                }
                dismissAction()
            } else if (set_no_kontrak == "klaim") {
                if let presenter = presentingViewController as? KlaimAsuransi {
                    presenter.txtNoKontrak.text = no_kontrak
                    if self.insurance[indexPath.row] == "ARK" {
                        presenter.txtPremiAsuransi.text = "All Risk"
                    } else if self.insurance[indexPath.row] == "TLO" {
                        presenter.txtPremiAsuransi.text = "Total Loss"
                    }
                }
                dismissAction()
            } else if (set_no_kontrak == "ambil") {
                if let presenter = presentingViewController as? AmbilJaminan {
                    presenter.txtNoKontrak.text = no_kontrak
                }
                dismissAction()
            } else if (set_no_kontrak == "pelunasan_maju") {
                if let presenter = presentingViewController as? SimulasiPelunasanMaju {
                    presenter.txtNoKontrak.text = no_kontrak
                    presenter.txtSisaPinjaman.text = "Rp. " + sisa_pinjam
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.load.isHidden = false
                        self.load.startAnimating()
                        
                        let prefs:UserDefaults = UserDefaults.standard
                        let api_key = prefs.value(forKey: "api_key") as? String
                        let token_number = prefs.value(forKey: "token_number") as? String
                        let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                        let id_customer = prefs.value(forKey: "id_customer") as? String
                        
                        let headers = [
                            "api_key": api_key,
                            "token_number": token_number
                        ]
                        
                        let parameters = [
                            "agreement_no": no_kontrak!,
                            "id_customer": id_customer!
                            ] as [String : Any]
                        
                        let appConfig : app_config = app_config()
                        Alamofire.request(appConfig.getApiUrl(type: "andalan_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                            .responseJSON { response in
                                
                                if (response.result.value == nil) {
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                    let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                } else {
                                    
                                    if let value = response.result.value {
                                        let json = JSON(value)
                                        print(json)
                                        
                                        if json["status"].stringValue == "success" {
                                            for item in json["andalan_detail_financial"].arrayValue {
                                                let dtString:String = item["maturity_date"].stringValue
                                                let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                                
                                                let dateFormatterGet = DateFormatter()
                                                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                                
                                                let dateFormatterPrint = DateFormatter()
                                                dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                                
                                                if let date = dateFormatterGet.date(from: dateString) {
                                                    presenter.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                                } else {
                                                    presenter.txtJatuhTempo.text = item["maturity_date"].stringValue
                                                }
                                            }
                                        }
                                    }
                                    
                                    self.load.isHidden = true
                                    self.load.stopAnimating()
                                }
                        }
                    })
                }
                dismissAction()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            SPStorkController.scrollViewDidScroll(scrollView)
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
}
