//
//  ModalPremiAsuransi.swift
//  Andalanku
//
//  Created by Handoyo on 01/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import SPFakeBar

class ModalPremiAsuransi: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let navBar = SPFakeBarView(style: .stork)
    let tableView = UITableView()
    private var data = ["Total Loss", "All Risk"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.contentInset.top = self.navBar.height
        self.tableView.scrollIndicatorInsets.top = self.navBar.height
        self.view.addSubview(self.tableView)
        
        self.navBar.titleLabel.text = "Pilih Premi Asuransi"
        self.navBar.leftButton.setTitle("X", for: .normal)
        self.navBar.leftButton.setTitleColor(UIColor.black, for: .normal)
        self.navBar.leftButton.tintColor = UIColor.black
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        self.updateLayout(with: self.view.frame.size)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func updateLayout(with size: CGSize) {
        self.tableView.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row]
        cell.transform = .identity
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let no_premi = data[indexPath.row]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(no_premi, forKey: "no_premi")
        prefs.synchronize()
        
        let set_simulasi = prefs.value(forKey: "set_simulasi") as? String
        if set_simulasi == "new" {
            if let presenter = presentingViewController as? PilihMobilDetail {
                presenter.txtPremiAsuransi.text = no_premi
            }
            dismissAction()
            /*
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_mobil_detail")
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            */
        } else if set_simulasi == "klaim" {
            if let presenter = presentingViewController as? KlaimAsuransi {
                presenter.txtPremiAsuransi.text = no_premi
            }
            dismissAction()
            /*
             let storyboard = UIStoryboard(name: "Main", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "story_mobil_detail")
             vc.modalPresentationStyle = .fullScreen
             self.present(vc, animated: true, completion: nil)
             */
        } else {
            if let presenter = presentingViewController as? PembiayaanUsedCars {
                presenter.txtPremiAsuransi.text = no_premi
            }
            dismissAction()
            /*
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "story_pembiayaan_used_cars")
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            */
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            SPStorkController.scrollViewDidScroll(scrollView)
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
}

