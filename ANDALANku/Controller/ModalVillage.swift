//
//  ModalVillage.swift
//  Andalanku
//
//  Created by Handoyo on 15/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import SPStorkController
import SPFakeBar
import Alamofire
import SwiftyJSON

class ModalVillage: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let navBar = SPFakeBarView(style: .stork)
    let tableView = UITableView()
    private var data = [""]
    var arrVillage = [String]()
    var arrIDVillage = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.tableView.contentInset.top = self.navBar.height
        self.tableView.scrollIndicatorInsets.top = self.navBar.height
        self.view.addSubview(self.tableView)
        
        self.navBar.titleLabel.text = "Pilih Kecamatan"
        self.navBar.leftButton.setTitle("X", for: .normal)
        self.navBar.leftButton.setTitleColor(UIColor.black, for: .normal)
        self.navBar.leftButton.tintColor = UIColor.black
        self.navBar.leftButton.addTarget(self, action: #selector(self.dismissAction), for: .touchUpInside)
        self.view.addSubview(self.navBar)
        
        self.updateLayout(with: self.view.frame.size)
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_district = prefs.value(forKey: "id_district") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_district": id_district!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "village_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            for item in json["village_list"].arrayValue {
                                self.arrIDVillage.append(item["id"].stringValue)
                                self.arrVillage.append(item["name"].stringValue)
                            }
                        }
                        self.tableView.reloadData()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updateLayout(with: self.view.frame.size)
    }
    
    func updateLayout(with size: CGSize) {
        self.tableView.frame = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = arrVillage[indexPath.row]
        cell.transform = .identity
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrIDVillage.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id_village = arrIDVillage[indexPath.row]
        let village = arrVillage[indexPath.row]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(id_village, forKey: "id_village")
        prefs.set(village, forKey: "village")
        prefs.synchronize()
        
        let set_formulir = prefs.value(forKey: "set_formulir") as? String
        if (set_formulir != nil && set_formulir != "") {
            if (set_formulir == "pembiayaan") {
                if let presenter = presentingViewController as? FormulirAplikasiPembiayaan {
                    presenter.txtVillage.text = village
                }
                dismissAction()
            } else if (set_formulir == "sesuai_ktp") {
                if let presenter = presentingViewController as? EditAlamatKTP {
                    presenter.txtVillage.text = village
                }
                dismissAction()
            } else if (set_formulir == "sesuai_kantor") {
                if let presenter = presentingViewController as? EditAlamatKantor {
                    presenter.txtVillage.text = village
                }
                dismissAction()
            } else if (set_formulir == "sesuai_domisili") {
                if let presenter = presentingViewController as? EditAlamatDomisili {
                    presenter.txtVillage.text = village
                }
                dismissAction()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            SPStorkController.scrollViewDidScroll(scrollView)
        }
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
