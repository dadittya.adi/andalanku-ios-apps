//
//  NotifikasiRegister.swift
//  Andalanku
//
//  Created by Handoyo on 09/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class NotifikasiRegister: UIViewController {

    @IBOutlet var lblContent: UILabel!
    var myString:NSString = ""
    var myMutableString = NSMutableAttributedString()
    @IBOutlet var navBar: UINavigationBar!
    
    var pageIndex: Int = 0
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.myString = self.lblContent.text! as NSString
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 12.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(rgb: 0x069DE4), range: NSRange(location:108,length:11))
        
        self.lblContent.attributedText = myMutableString
    }

    @IBAction func goLanjut(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
