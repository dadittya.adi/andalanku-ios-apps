//
//  Partner.swift
//  Andalanku
//
//  Created by Handoyo on 14/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class Partner: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async(execute: {
            self.load.isHidden = true
            self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navBar.shadowImage = UIImage()
            self.webView.scrollView.isScrollEnabled = true
            self.webView.scrollView.bounces = false
            self.webView.scrollView.isPagingEnabled = true
            self.webView.isUserInteractionEnabled = true
            self.webView.delegate = self
            
            let prefs:UserDefaults = UserDefaults.standard
            let url = prefs.value(forKey: "partner_url") as? String
            let urlfull:String = url!
            self.webView.loadRequest(URLRequest(url: URL(string: urlfull)!))
        })
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        self.load.isHidden = false
        self.load.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        self.load.isHidden = true
        self.load.stopAnimating()
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

}
