//
//  PembayaranAdvance.swift
//  Andalanku
//
//  Created by Macintosh on 30/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PembayaranAdvance: UIViewController {
    
    var pageIndex:Int = 0
    @IBOutlet weak var lblPokokPinjaman: UILabel!
    @IBOutlet weak var lblTenor: UILabel!
    @IBOutlet weak var lblPembayaranPertama: UILabel!
    @IBOutlet weak var lblAngsuran: UILabel!
    
    @IBOutlet weak var txtRincian: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let prefs:UserDefaults = UserDefaults.standard
        let pokok_pinjaman = prefs.value(forKey: "pokok_pinjaman") as? String
        let tenor = prefs.value(forKey: "tenor") as? String
        let pembayaran_pertama = prefs.value(forKey: "pembayaran_pertama") as? String
        let angsuran = prefs.value(forKey: "angsuran") as? String
        
        self.lblPokokPinjaman.text = "Rp." + pokok_pinjaman!
        self.lblTenor.text = tenor! + " Bulan"
        self.lblPembayaranPertama.text = "Rp. " + pembayaran_pertama!
        self.lblAngsuran.text = "Rp. " + angsuran!
        
        let text = """
            • Rincian simulasi di atas bersifat estimasi dan tidak mengikat dan dapat berubah sewaktu-waktu mengkuti kebijakan yang berlaku.
            • Total Pembayaran Pertama sudah termasuk Down Payment (DP) Nett, biaya admin, dan biaya asuransi.
            • Untuk informasi lengkap, silahkan menghubungi Kantor Cabang Andalan Finance terdekat.
            """
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        txtRincian.attributedText = NSAttributedString(attributedListString: attributedText, withFont: txtRincian.font)
    }
    
    @IBAction func goApply(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_otp = prefs.value(forKey: "set_otp") as? String
        if (set_otp != nil)
        {
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let parameters = [
                    "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                    "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            if let value = response.result.value {
                                let json = JSON(value)
                                let status = json["status"].stringValue
                                let token_number = json["token_number"].stringValue
                                if status == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                    prefs.set(token_number, forKey: "token_number")
                                    prefs.synchronize()
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                }
            })
        } else {
            getMain()
        }
    }
    
    func getMain() {
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goUlang(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_new_cars")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}

extension NSAttributedString {
    
    convenience init(listString string: String, withFont font: UIFont) {
        self.init(attributedListString: NSAttributedString(string: string), withFont: font)
    }
    
    convenience init(attributedListString attributedString: NSAttributedString, withFont font: UIFont) {
        guard let regex = try? NSRegularExpression(pattern: "^(\\d+\\.|[•\\-\\*])(\\s+).+$",
                                                   options: [.anchorsMatchLines]) else { fatalError() }
        let matches = regex.matches(in: attributedString.string, options: [],
                                    range: NSRange(location: 0, length: attributedString.string.utf16.count))
        let nsString = attributedString.string as NSString
        let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
        
        for match in matches {
            let size = NSAttributedString(
                string: nsString.substring(with: match.range(at: 1)) + nsString.substring(with: match.range(at: 2)),
                attributes: [.font: font]).size()
            let indentation = ceil(size.width)
            let range = match.range(at: 0)
            
            let paragraphStyle = NSMutableParagraphStyle()
            
            if let style = attributedString.attribute(.paragraphStyle, at: 0, longestEffectiveRange: nil, in: range)
                as? NSParagraphStyle {
                paragraphStyle.setParagraphStyle(style)
            }
            
            paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: indentation, options: [:])]
            paragraphStyle.defaultTabInterval = indentation
            paragraphStyle.firstLineHeadIndent = 0
            paragraphStyle.headIndent = indentation
            
            mutableAttributedString.addAttribute(.font, value: font, range: range)
            mutableAttributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: range)
        }
        
        self.init(attributedString: mutableAttributedString)
    }
}
