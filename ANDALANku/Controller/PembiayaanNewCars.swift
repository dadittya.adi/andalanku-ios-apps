//
//  PembiayaanNewCars.swift
//  Andalanku
//
//  Created by Handoyo on 26/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Material

class PembiayaanNewCars: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var cvNewCars: UICollectionView!
    var arrPartnerImage: [UIImage] = [
        UIImage(named: "img_jenis_1")!,
        UIImage(named: "img_jenis_2")!,
        UIImage(named: "img_jenis_3")!,
        UIImage(named: "img_jenis_4")!,
        UIImage(named: "img_jenis_5")!,
        UIImage(named: "img_jenis_6")!,
        UIImage(named: "img_jenis_1")!,
        UIImage(named: "img_jenis_2")!,
        UIImage(named: "img_jenis_3")!
    ]
    var arrCarBrand: NSMutableArray = []
    @IBOutlet weak var txtLokasi: TextField!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.cvNewCars.keyboardDismissMode = .onDrag
        let sz = UIScreen.main.bounds.width
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (sz / 2) - 25, height: (sz / 2) - 25)
        self.cvNewCars.setCollectionViewLayout(layout, animated: true)
        self.txtLokasi.delegate = self
        self.txtLokasi.text = "Jawa Tengah"
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "keyword": ""
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "car_brand_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["car_brand_list"].arrayObject != nil {
                                self.arrCarBrand.removeAllObjects()
                                if let past : NSArray = json["car_brand_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["car_brand_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrCarBrand.add((json["car_brand_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.cvNewCars.reloadData()
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                    }
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvNewCars {
            return arrCarBrand.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            if collectionView == self.cvNewCars {
                let id_car_brand : NSString = (self.arrCarBrand[indexPath.row] as AnyObject).value(forKey: "id_car_brand") as! NSString
                let car_image : NSString = (self.arrCarBrand[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
                let brand_name : NSString = (self.arrCarBrand[indexPath.row] as AnyObject).value(forKey: "name") as! NSString
                let prefs:UserDefaults = UserDefaults.standard
                prefs.set(id_car_brand as String, forKey: "id_car_brand")
                prefs.set(car_image as String, forKey: "car_image")
                prefs.set(brand_name as String, forKey: "brand_name")
                prefs.synchronize()
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pilih_mobil_idaman")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        if collectionView == self.cvNewCars {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PembiayaanNewCarsCell
            
            let image : NSString = (self.arrCarBrand[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            cell.downloadImage(URL(string: image as String)!)
            
            return cell as PembiayaanNewCarsCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    @IBAction func goBack(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
        if set_pembiayaan == "main" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_main")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
