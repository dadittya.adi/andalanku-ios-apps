//
//  PembiayaanNewCarsCell.swift
//  Andalanku
//
//  Created by Macintosh on 27/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class PembiayaanNewCarsCell: UICollectionViewCell {
    
    @IBOutlet weak var imgNewCars: UIImageView!
    @IBOutlet weak var viewNewCars: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewNewCars.layer.shadowColor = UIColor.gray.cgColor
        viewNewCars.layer.shadowOpacity = 0.5
        viewNewCars.layer.shadowOffset = CGSize.zero
        viewNewCars.layer.cornerRadius = 5
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let data = data, error == nil else
                {
                    return
                }
                self.imgNewCars.image = UIImage(data: data as Data)
            }
        }
    }
}
