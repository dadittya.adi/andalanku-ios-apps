//
//  PembiayaanUsedCars.swift
//  Andalanku
//
//  Created by Handoyo on 26/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SPStorkController
import Material

class PembiayaanUsedCars: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var arrowMobilIdaman: UIImageView!
    @IBOutlet weak var txtMobilIdaman: TextField!
    @IBOutlet weak var txtHarga: TextField!
    @IBOutlet weak var txtDP: TextField!
    @IBOutlet weak var txtUangMuka: TextField!
    
    @IBOutlet weak var txtTenorPembiayaan: TextField!
    @IBOutlet weak var imgTenor: UIImageView!
    @IBOutlet weak var txtPremiAsuransi: TextField!
    @IBOutlet weak var imgPremi: UIImageView!
    @IBOutlet weak var txtWilayah: TextField!
    @IBOutlet weak var imgWilayah: UIImageView!
    @IBOutlet weak var txtTahun: TextField!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    var getHarga:String = ""
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            /*
            let prefs:UserDefaults = UserDefaults.standard
            let no_mobil = prefs.value(forKey: "no_mobil") as? String
            if (no_mobil != nil && no_mobil != "") {
                self.txtMobilIdaman.text = no_mobil
                self.txtDP.text = "20"
                self.txtHarga.text = "978500000"
                self.txtTahun.text = "2019"
                
                let no_tenor = prefs.value(forKey: "no_tenor") as? String
                let no_premi = prefs.value(forKey: "no_premi") as? String
                let no_wilayah = prefs.value(forKey: "no_wilayah") as? String
                let hasil = prefs.value(forKey: "hasil") as? String
                
                let otr = prefs.value(forKey: "otr") as? String
                let dp = prefs.value(forKey: "dp") as? String
                if (no_tenor != nil) {
                    self.txtTenorPembiayaan.text = no_tenor
                }
                if (no_premi != nil) {
                    self.txtPremiAsuransi.text = no_premi
                }
                if (no_wilayah != nil) {
                    self.txtWilayah.text = no_wilayah
                }
                if (hasil != nil) {
                    self.txtUangMuka.text = hasil
                }
                if (otr != nil) {
                    self.txtHarga.text = otr
                }
                if (dp != nil) {
                    self.txtDP.text = dp
                }
                if self.txtHarga.text == "" {
                    self.txtHarga.text = "0"
                }
                if self.txtDP.text == "" {
                    self.txtDP.text = "20"
                }
                var otr_int:Int = 0
                otr_int = Int(self.txtHarga.text!)!
                self.txtHarga.text = String(otr_int)
                
                var hitung:Int = 0
                hitung = (Int(self.txtDP.text!)! * Int(self.txtHarga.text!)!) / 100
                //var hasil2:Int = 0
                //hasil2 = Int(self.txtHarga.text!)! - hitung
                self.txtUangMuka.text = String(hitung)
            }
            */
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.scrollView.keyboardDismissMode = .onDrag
        let tapArrowMobilIdaman = UITapGestureRecognizer(target:self, action:#selector(PembayaranAngsuran.goDownTap(_:)))
        self.arrowMobilIdaman.isUserInteractionEnabled = true
        self.arrowMobilIdaman.addGestureRecognizer(tapArrowMobilIdaman)
        self.txtMobilIdaman.delegate = self
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(PembiayaanUsedCars.goDownDownTap(_:)))
        self.imgTenor.isUserInteractionEnabled = true
        self.imgTenor.addGestureRecognizer(tapImgDown)
        
        let tapImgPremi = UITapGestureRecognizer(target:self, action:#selector(PembiayaanUsedCars.goPremiTap(_:)))
        self.imgPremi.isUserInteractionEnabled = true
        self.imgPremi.addGestureRecognizer(tapImgPremi)
        
        let tapImgWilayah = UITapGestureRecognizer(target:self, action:#selector(PembiayaanUsedCars.goWilayahTap(_:)))
        self.imgWilayah.isUserInteractionEnabled = true
        self.imgWilayah.addGestureRecognizer(tapImgWilayah)
        self.load.isHidden = true
    }
    
    @IBAction func setOTR() {
        if self.txtHarga.text == "" {
            self.txtHarga.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        
        var otr_int:Int = 0
        otr_int = Int(getHarga)!
        self.txtHarga.text = String(otr_int)
        
        let valString:String = self.txtHarga.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtHarga.text = formattedTipAmount
        }
        
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(getHarga)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(self.txtUangMuka.text!, forKey: "hasil")
        prefs.set(getHarga, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func txtHargaChange(_ sender: Any) {
        if self.txtHarga.text == "" {
            self.txtHarga.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        
        let valString:String = self.txtHarga.text!
        var hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtHarga.text = formattedTipAmount
        }
        
        //ayo konversi .......
        if self.txtDP.text == "" {
            self.txtDP.text = "0"
        }
        
        let getHarga:String = valString.replacingOccurrences(of: ".", with: "")
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(getHarga)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let umString:String = self.txtUangMuka.text!
        let uangString:String = umString.replacingOccurrences(of: ".", with: "")
        hargaString = valString.replacingOccurrences(of: ".", with: "")
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(uangString, forKey: "hasil")
        prefs.set(hargaString, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func txtDPChange(_ sender: Any) {
        if self.txtHarga.text == "" {
            self.txtHarga.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        
        let valString:String = self.txtHarga.text!
        var hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtHarga.text = formattedTipAmount
        }
        
        //ayo konversi .......
        if self.txtDP.text == "" {
            self.txtDP.text = "0"
        }
        
        let getHarga:String = valString.replacingOccurrences(of: ".", with: "")
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(getHarga)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let umString:String = self.txtUangMuka.text!
        let uangString:String = umString.replacingOccurrences(of: ".", with: "")
        hargaString = valString.replacingOccurrences(of: ".", with: "")
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(uangString, forKey: "hasil")
        prefs.set(hargaString, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func goTxtMobilIdamanBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used_car", forKey: "list_used_car")
            prefs.synchronize()
            
            self.txtMobilIdaman.resignFirstResponder()
            let controller = ModalPilihMobilIdaman()
            let transitionDelegate = SPStorkTransitioningDelegate()
            //transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used_car", forKey: "list_used_car")
            prefs.synchronize()
            
            let controller = ModalPilihMobilIdaman()
            let transitionDelegate = SPStorkTransitioningDelegate()
            //transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtTenorBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtTenorPembiayaan.resignFirstResponder()
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtPremiBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtPremiAsuransi.resignFirstResponder()
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goPremiTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtWilayahBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtWilayah.resignFirstResponder()
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goWilayahTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("used", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goKalkulasi(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            var premi:String = ""
            if self.txtPremiAsuransi.text == "Total Loss" {
                premi = "TLO"
            } else if self.txtPremiAsuransi.text == "All Risk" {
                premi = "ARK"
            } else {
                premi = "ARK"
            }
            
            var wilayah:String = ""
            if self.txtWilayah.text == "Sumatra dan Kepulauannya" {
                wilayah = "1"
            } else if self.txtWilayah.text == "Jakarta, Banten dan Jawa Barat" {
                wilayah = "2"
            } else if self.txtWilayah.text == "Lainnya" {
                wilayah = "3"
            } else {
                wilayah = "3"
            }
            
            let hgString:String = self.txtHarga.text!
            let hargaString:String = hgString.replacingOccurrences(of: ".", with: "")
            
            let parameters = [
                "type": "USED",
                "otr": hargaString,
                "dp": self.txtDP.text!,
                "tenor": self.txtTenorPembiayaan.text!,
                "asuransi": premi,
                "wilayah" : wilayah
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "credit_simulation"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            
                            if status == "success" {
                                for item in json["simulation_data"].arrayValue {
                                    if item["credit_type"].stringValue == "ADVANCE" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set("ADVANCE", forKey: "credit_type")
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman")
                                        prefs.set(tenor, forKey: "tenor")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama")
                                        prefs.set(angsuran, forKey: "angsuran")
                                        prefs.synchronize()
                                    } else if item["credit_type"].stringValue == "ARREAR" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set("ARREAR", forKey: "credit_type")
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman_arrear")
                                        prefs.set(tenor, forKey: "tenor_arrear")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama_arrear")
                                        prefs.set(angsuran, forKey: "angsuran_arrear")
                                        prefs.synchronize()
                                    }
                                }
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_simulasi_pembiayaan_update")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
        if set_pembiayaan == "main" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_main")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
