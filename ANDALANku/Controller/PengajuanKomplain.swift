//
//  PengajuanKomplain.swift
//  Andalanku
//
//  Created by Handoyo on 02/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON
import SPStorkController

class PengajuanKomplain: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewFoto: UIView!
    @IBOutlet weak var txtKategori: TextField!
    @IBOutlet weak var arrowKategori: UIImageView!
    @IBOutlet weak var txtNamaLengkap: TextField!
    @IBOutlet weak var txtNoHandphone: TextField!
    @IBOutlet weak var txtNoTelp: TextField!
    @IBOutlet weak var txtPesan: UITextView!
    @IBOutlet weak var imgKeluhan: UIImageView!
    @IBOutlet weak var lblUpload: UILabel!
    var arrMedia = [String]()
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            /*
            let prefs:UserDefaults = UserDefaults.standard
            let no_kategori = prefs.value(forKey: "no_kategori") as? String
            if (no_kategori != nil && no_kategori != "") {
                self.txtKategori.text = no_kategori
            }
            */
        })
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        
        viewFoto.layer.shadowColor = UIColor.lightGray.cgColor
        viewFoto.layer.shadowOpacity = 0.3
        viewFoto.layer.shadowOffset = CGSize.zero
        viewFoto.layer.cornerRadius = 10
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(PengajuanKomplain.goDownTap(_:)))
        self.arrowKategori.isUserInteractionEnabled = true
        self.arrowKategori.addGestureRecognizer(tapImgDown)
        self.txtKategori.delegate = self
        
        let tapImgUpload = UITapGestureRecognizer(target:self, action:#selector(PengajuanKomplain.imageTapped(_:)))
        self.lblUpload.isUserInteractionEnabled = true
        self.lblUpload.addGestureRecognizer(tapImgUpload)
        self.load.isHidden = true
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "personal_information"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            self.txtNamaLengkap.text = json["customer_name"].stringValue
                            self.txtNoTelp.text = json["home_phone_number"].stringValue
                            self.txtNoHandphone.text = json["phone_number"].stringValue
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txtKategori.resignFirstResponder()
            let controller = ModalPilihKategoriKomplain()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let controller = ModalPilihKategoriKomplain()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let image: UIImage = self.imgKeluhan.image!
            let size = image.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
            let hasAlpha = false
            let scale: CGFloat = 0.0
            
            UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
            image.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
            
            UIGraphicsEndImageContext()
            
            let imageData = image.jpegData(compressionQuality: 0.9)
            
            let base64String = imageData!.base64EncodedString(options: [NSData.Base64EncodingOptions(rawValue: 0)])
            
            self.arrMedia.append(base64String)
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            print(id_customer!)
            print(self.txtKategori.text!)
            print(self.txtNoHandphone.text!)
            print(self.txtPesan.text!)
            print(self.arrMedia)
            
            let parameters = [
                "id_customer": id_customer!,
                "category": self.txtKategori.text!,
                "phone": self.txtNoHandphone.text!,
                "message": self.txtPesan.text!,
                "media": self.arrMedia
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "input_e_complain"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["status"].stringValue == "success" {
                                let alertController = UIAlertController(title: "Informasi", message: "Pengajuan Komplain Berhasil", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_e_complain_services")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let ImagePicker = UIImagePickerController()
            ImagePicker.delegate = self
            ImagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            self.present(ImagePicker, animated: true, completion: nil)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        DispatchQueue.main.async(execute: {
            self.imgKeluhan.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_e_complain_services")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

}
