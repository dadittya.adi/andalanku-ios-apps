//
//  PengaturanAkun.swift
//  Andalanku
//
//  Created by Handoyo on 19/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON

class PengaturanAkun: UIViewController, UITextFieldDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var txt_nama_lengkap: TextField!
    @IBOutlet var txt_nomor_ktp: TextField!
    @IBOutlet var txt_nomor_hp: TextField!
    @IBOutlet var txt_email: TextField!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        self.txt_nama_lengkap.delegate = self
        self.txt_nomor_ktp.delegate = self
        self.txt_nomor_hp.delegate = self
        self.txt_email.delegate = self
        
        let prefs:UserDefaults = UserDefaults.standard
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        let ktp = prefs.value(forKey: "ktp") as? String
        let email = prefs.value(forKey: "email") as? String
        
        if (nama_akun != nil && nama_akun != "") {
            self.txt_nama_lengkap.text = nama_akun
        }
        
        if ktp != nil {
            self.txt_nomor_ktp.text = ktp
        }
        
        if email != nil {
            self.txt_email.text = email
        }
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "personal_information"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            self.txt_nama_lengkap.text = json["customer_name"].stringValue
                            self.txt_nomor_ktp.text = json["id_number"].stringValue
                            self.txt_nomor_hp.text = json["phone_number"].stringValue
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }

    @IBAction func goPerbaharui(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_perbarui_diri")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
