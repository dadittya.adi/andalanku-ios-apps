//
//  PengaturanKeamanan.swift
//  Andalanku
//
//  Created by Handoyo on 09/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class PengaturanKeamanan: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblKataSandi: UILabel!
    @IBOutlet weak var arrowKataSandi: UIImageView!
    @IBOutlet weak var lblTouch: UILabel!
    @IBOutlet weak var arrowTouch: UIImageView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapLblKataSandi = UITapGestureRecognizer(target:self, action:#selector(PengaturanKeamanan.goKataSandiTap(_:)))
        self.lblKataSandi.isUserInteractionEnabled = true
        self.lblKataSandi.addGestureRecognizer(tapLblKataSandi)
        
        let tapArrowKataSandi = UITapGestureRecognizer(target:self, action:#selector(PengaturanKeamanan.goKataSandiTap(_:)))
        self.arrowKataSandi.isUserInteractionEnabled = true
        self.arrowKataSandi.addGestureRecognizer(tapArrowKataSandi)
        
        let tapLblTouch = UITapGestureRecognizer(target:self, action:#selector(PengaturanKeamanan.goTouchTap(_:)))
        self.lblTouch.isUserInteractionEnabled = true
        self.lblTouch.addGestureRecognizer(tapLblTouch)
        
        let tapArrowTouch = UITapGestureRecognizer(target:self, action:#selector(PengaturanKeamanan.goTouchTap(_:)))
        self.arrowTouch.isUserInteractionEnabled = true
        self.arrowTouch.addGestureRecognizer(tapArrowTouch)
    }
    
    @objc func goKataSandiTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("pengaturan", forKey: "set_katasandi")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_ubah_kata_sandi")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTouchTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_touch_id")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }

    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.selectedIndex = 3
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
