//
//  PilihMobilDetail.swift
//  Andalanku
//
//  Created by Handoyo on 27/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON
import SPStorkController
import AACarousel
import Kingfisher

class PilihMobilDetail: UIViewController, UITextFieldDelegate, AACarouselDelegate {

    @IBOutlet weak var txt_premium: TextField!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtDP: TextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtTenorPembiayaan: TextField!
    @IBOutlet weak var imgTenor: UIImageView!
    @IBOutlet weak var txtPremiAsuransi: TextField!
    @IBOutlet weak var imgPremi: UIImageView!
    @IBOutlet weak var txtWilayah: TextField!
    @IBOutlet weak var imgWilayah: UIImageView!
    @IBOutlet weak var txtOTR: TextField!
    @IBOutlet weak var txtUangMuka: TextField!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewImage: AACarousel!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleText: UILabel!
    @IBOutlet weak var imgTipe: UIImageView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    var imgSourceArray = [String]()
    var titleArray = [String]()
    var tglArray = [String]()
    var contentArray = [String]()
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            /*
            let prefs:UserDefaults = UserDefaults.standard
            let no_tenor = prefs.value(forKey: "no_tenor") as? String
            let no_premi = prefs.value(forKey: "no_premi") as? String
            let no_wilayah = prefs.value(forKey: "no_wilayah") as? String
            let hasil = prefs.value(forKey: "hasil") as? String
            //let otr = prefs.value(forKey: "otr") as? String
            let dp = prefs.value(forKey: "dp") as? String
            let trim_name = prefs.value(forKey: "trim_name") as? String
            let trim_price = prefs.value(forKey: "trim_price") as? String
            if (trim_name != nil) {
                self.txt_premium.text = trim_name
                self.txtOTR.text = trim_price
            }
            if (no_tenor != nil) {
                self.txtTenorPembiayaan.text = no_tenor
            }
            if (no_premi != nil) {
                self.txtPremiAsuransi.text = no_premi
            }
            if (no_wilayah != nil) {
                self.txtWilayah.text = no_wilayah
            }
            if (hasil != nil) {
                self.txtUangMuka.text = hasil
            }
            /*
            if (otr != nil) {
                self.txtOTR.text = otr
            }
            */
            if (dp != nil) {
                self.txtDP.text = dp
            }
            if self.txtOTR.text == "" {
                self.txtOTR.text = "0"
            }
            if self.txtDP.text == "" {
                self.txtDP.text = "20"
            }
            var otr_int:Int = 0
            otr_int = Int(self.txtOTR.text!)!
            self.txtOTR.text = String(otr_int)
            
            var hitung:Int = 0
            hitung = (Int(self.txtDP.text!)! * Int(self.txtOTR.text!)!) / 100
            //var hasil2:Int = 0
            //hasil2 = Int(self.txtOTR.text!)! - hitung
            self.txtUangMuka.text = String(hitung)
            */
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBackground.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_full")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        self.txt_premium.text = ""
        self.txtOTR.text = "0"
        self.txtDP.text = "20"
        self.txtDP.textAlignment = .left
        self.txtTenorPembiayaan.delegate = self
        self.txtPremiAsuransi.delegate = self
        self.txtWilayah.delegate = self
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(PilihMobilDetail.goDownTap(_:)))
        self.imgTenor.isUserInteractionEnabled = true
        self.imgTenor.addGestureRecognizer(tapImgDown)
        
        let tapImgPremi = UITapGestureRecognizer(target:self, action:#selector(PilihMobilDetail.goPremiTap(_:)))
        self.imgPremi.isUserInteractionEnabled = true
        self.imgPremi.addGestureRecognizer(tapImgPremi)
        
        let tapImgWilayah = UITapGestureRecognizer(target:self, action:#selector(PilihMobilDetail.goWilayahTap(_:)))
        self.imgWilayah.isUserInteractionEnabled = true
        self.imgWilayah.addGestureRecognizer(tapImgWilayah)
        
        self.viewImage.setCarouselOpaque(layer: true, describedTitle:
            true, pageIndicator: false)
        
        viewImage.layer.shadowColor = UIColor.black.cgColor
        viewImage.layer.shadowOpacity = 0.5
        viewImage.layer.shadowOffset = CGSize.zero
        viewImage.layer.cornerRadius = 5
        viewImage.layoutMargins = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        
        self.load.isHidden = true
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_car_brand = prefs.value(forKey: "id_car_brand") as? String
            let id_car_type = prefs.value(forKey: "id_car_type") as! String
            let type_name = prefs.value(forKey: "type_name") as! String
            let brand_name = prefs.value(forKey: "brand_name") as! String
            
            self.lblTitleName.text = brand_name + " " + type_name
            self.lblTitleText.text = brand_name + " " + type_name
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "id_car_brand": id_car_brand!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "car_type_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            for item in json["car_type_list"].arrayValue {
                                var i:Int = 0
                                if id_car_type == item["id_car_type"].stringValue {
                                    for item2 in item["car_type_image"].arrayValue {
                                        print(item2)
                                        self.imgSourceArray.append(item["car_type_image"][i].string!)
                                        i = i + 1
                                    }
                                }
                                self.titleArray.append(item["name"].stringValue)
                            }
                        }
                        
                        self.viewImage.delegate = self
                        self.viewImage.setCarouselData(paths: self.imgSourceArray,  describedTitle: self.titleArray, isAutoScroll: true, timer: 5.0, defaultImage: "defaultImage")
                        
                        self.viewImage.setCarouselLayout(displayStyle: 0, pageIndicatorPositon: 2, pageIndicatorColor: nil, describedTitleColor: UIColor.white, layerColor: nil)
                        
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
        let tapImgTipe = UITapGestureRecognizer(target:self, action:#selector(PilihMobilDetail.goTipeTap(_:)))
        self.imgTipe.isUserInteractionEnabled = true
        self.imgTipe.addGestureRecognizer(tapImgTipe)
        self.txt_premium.delegate = self
    }
    
    @IBAction func goTxtPremiumBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.txt_premium.resignFirstResponder()
            let controller = ModalCarList()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goTipeTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let controller = ModalCarList()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    func didSelectCarouselView(_ view:AACarousel ,_ index:Int) {
        /*
        let promo_image = imgSourceArray[index]
        let tanggalPromo = tglArray[index]
        let contentPromo = contentArray[index]
        let titlePromo = titleArray[index]
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(promo_image as String, forKey: "imgPromo")
        prefs.set(tanggalPromo as String, forKey: "tanggalPromo")
        prefs.set(contentPromo as String, forKey: "contentPromo")
        prefs.set(titlePromo as String, forKey: "titlePromo")
        prefs.synchronize()
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo_detail")
         vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        */
    }
    
    func downloadImages(_ url: String, _ index:Int) {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize.zero
        imageView.layer.cornerRadius = 5
        imageView.layoutMargins = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        
        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.viewImage.contentMode = .scaleAspectFill
            self.viewImage.clipsToBounds = true
            self.viewImage.images[index] = downloadImage!
        })
    }
    
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOpacity = 0.5
        imageView.layer.shadowOffset = CGSize.zero
        imageView.layer.cornerRadius = 5
        imageView.layoutMargins = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "defaultImage"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.viewImage.images[index] = downloadImage!
        })
    }
    
    func startAutoScroll() {
        viewImage.startScrollImageView()
    }
    
    func stopAutoScroll() {
        viewImage.stopScrollImageView()
    }
    
    @IBAction func setOTR() {
        if self.txtOTR.text == "" {
            self.txtOTR.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        var otr_int:Int = 0
        otr_int = Int(self.txtOTR.text!)!
        self.txtOTR.text = String(otr_int)
        
        let valString:String = self.txtOTR.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtOTR.text = formattedTipAmount
        }
        
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(hargaString)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(self.txtUangMuka.text!, forKey: "hasil")
        prefs.set(self.txtOTR.text!, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func goTxtOTRChange(_ sender: Any) {
        if self.txtOTR.text == "" {
            self.txtOTR.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        
        let valString:String = self.txtOTR.text!
        var hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtOTR.text = formattedTipAmount
        }
        
        //ayo konversi .......
        if self.txtDP.text == "" {
            self.txtDP.text = "0"
        }
        
        let getHarga:String = valString.replacingOccurrences(of: ".", with: "")
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(getHarga)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let umString:String = self.txtUangMuka.text!
        let uangString:String = umString.replacingOccurrences(of: ".", with: "")
        hargaString = valString.replacingOccurrences(of: ".", with: "")
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(uangString, forKey: "hasil")
        prefs.set(hargaString, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func goTxtDPChange(_ sender: Any) {
        if self.txtOTR.text == "" {
            self.txtOTR.text = "0"
        }
        if self.txtDP.text == "" {
            self.txtDP.text = "20"
        }
        
        let valString:String = self.txtOTR.text!
        var hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtOTR.text = formattedTipAmount
        }
        
        //ayo konversi .......
        if self.txtDP.text == "" {
            self.txtDP.text = "0"
        }
        
        let getHarga:String = valString.replacingOccurrences(of: ".", with: "")
        var hitung:Int = 0
        hitung = (Int(self.txtDP.text!)! * Int(getHarga)!) / 100
        if let formattedTipAmount = formatter.string(from: Int(String(hitung) as String)! as NSNumber) {
            self.txtUangMuka.text = formattedTipAmount
        }
        
        let umString:String = self.txtUangMuka.text!
        let uangString:String = umString.replacingOccurrences(of: ".", with: "")
        hargaString = valString.replacingOccurrences(of: ".", with: "")
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(uangString, forKey: "hasil")
        prefs.set(hargaString, forKey: "otr")
        prefs.set(self.txtDP.text!, forKey: "dp")
        prefs.synchronize()
    }
    
    @IBAction func goTxtTenorBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtTenorPembiayaan.resignFirstResponder()
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalTenorPembiayaan()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtPremiBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtPremiAsuransi.resignFirstResponder()
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goPremiTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalPremiAsuransi()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goTxtWilayahBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            self.txtWilayah.resignFirstResponder()
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goWilayahTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("new", forKey: "set_simulasi")
            prefs.synchronize()
            
            let controller = ModalZonaWilayah()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }

    @IBAction func goKalkulasi(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            prefs.set(self.txtUangMuka.text!, forKey: "hasil")
            prefs.set(self.txtOTR.text!, forKey: "otr")
            prefs.set(self.txtDP.text!, forKey: "dp")
            prefs.synchronize()
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            var premi:String = ""
            if self.txtPremiAsuransi.text == "Total Loss" {
                premi = "TLO"
            } else if self.txtPremiAsuransi.text == "All Risk" {
                premi = "ARK"
            } else {
                premi = "ARK"
            }
            
            var wilayah:String = ""
            if self.txtWilayah.text == "Sumatra dan Kepulauannya" {
                wilayah = "1"
            } else if self.txtWilayah.text == "Jakarta, Banten dan Jawa Barat" {
                wilayah = "2"
            } else if self.txtWilayah.text == "Lainnya" {
                wilayah = "3"
            } else {
                wilayah = "3"
            }
            
            let valString:String = self.txtOTR.text!
            let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
            
            let parameters = [
                "type": "NEW",
                "otr": hargaString,
                "dp": self.txtDP.text!,
                "tenor": self.txtTenorPembiayaan.text!,
                "asuransi": premi,
                "wilayah" : wilayah
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "credit_simulation"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            
                            if status == "success" {
                                for item in json["simulation_data"].arrayValue {
                                    if item["credit_type"].stringValue == "ADVANCE" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set("ADVANCE", forKey: "credit_type")
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman")
                                        prefs.set(tenor, forKey: "tenor")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama")
                                        prefs.set(angsuran, forKey: "angsuran")
                                        prefs.synchronize()
                                    } else if item["credit_type"].stringValue == "ARREAR" {
                                        let pokok_pinjaman = item["pokok_hutang"].stringValue
                                        let tenor = item["tenor"].stringValue
                                        let pembayaran_pertama = item["dp"].stringValue
                                        let angsuran = item["angsuran"].stringValue
                                        
                                        let prefs:UserDefaults = UserDefaults.standard
                                        prefs.set("ARREAR", forKey: "credit_type")
                                        prefs.set(pokok_pinjaman, forKey: "pokok_pinjaman_arrear")
                                        prefs.set(tenor, forKey: "tenor_arrear")
                                        prefs.set(pembayaran_pertama, forKey: "pembayaran_pertama_arrear")
                                        prefs.set(angsuran, forKey: "angsuran_arrear")
                                        prefs.synchronize()
                                    }
                                }
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_simulasi_pembiayaan_update")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pilih_mobil_idaman")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
