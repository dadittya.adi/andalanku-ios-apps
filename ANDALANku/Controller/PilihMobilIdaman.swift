//
//  PilihMobilIdaman.swift
//  Andalanku
//
//  Created by Handoyo on 27/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PilihMobilIdaman: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var tvJSON: UITableView!
    var arrMobilLabel = ["Alpart","INNOVA","All New Avanza","Hilux"]
    var arrMobilImage: [UIImage] = [
        UIImage(named: "img_idaman1")!,
        UIImage(named: "img_idaman2")!,
        UIImage(named: "img_idaman3")!,
        UIImage(named: "img_idaman4")!
    ]
    
    @IBOutlet weak var imageBrand: UIImageView!
    @IBOutlet var load: UIActivityIndicatorView!
    @IBOutlet weak var jumlahMobil: UILabel!
    
    var arrDict :NSMutableArray=[]
    var par_num = 0
    var limit = 4
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        self.tvJSON.tableFooterView = UIView(frame: .zero)
        self.load.isHidden = true
        
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set("", forKey: "no_tenor")
        prefs.set("", forKey: "no_premi")
        prefs.set("", forKey: "no_wilayah")
        prefs.set("", forKey: "hasil")
        prefs.set("978500000", forKey: "otr")
        prefs.set("20", forKey: "dp")
        prefs.synchronize()
        
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_car_brand = prefs.value(forKey: "id_car_brand") as? String
            let car_image = prefs.value(forKey: "car_image") as! String
            self.downloadImage(URL(string: car_image as String)!)
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_car_brand": id_car_brand!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "car_type_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["car_type_list"].arrayObject != nil {
                                self.arrDict.removeAllObjects()
                                if let past : NSArray = json["car_type_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["car_type_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["car_type_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.tvJSON.reloadData()
                                self.jumlahMobil.text = String(self.arrDict.count) + " Mobil Pilihan"
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                guard let data = data, error == nil else { return }
                self.imageBrand.image = UIImage(data: data as Data)
            }
        }
    }
    
    @objc func loadBottom() {
        /*
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "keyword": "",
                "page": self.par_num
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            if json["promo_list"].arrayObject != nil {
                                if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.par_num = self.par_num + self.limit
                                self.tvJSON.reloadData()
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
        */
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PilihMobilIdamanCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? PilihMobilIdamanCell
        
        let prefs:UserDefaults = UserDefaults.standard
        let brand_name = prefs.value(forKey: "brand_name") as? String
        cell.lblBrand.text = brand_name!
        let name : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "name") as! NSString
        cell.lblTitle.text = name as String
        if let harga : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "lowestPrice") as? NSString {
            cell.lblHarga.text = harga as String
        } else {
            cell.lblHarga.text = "0"
        }
        
        let json = JSON(arrDict[indexPath.row])
        var i:Int = 0
        for item in json["car_type_image"].arrayValue {
            print(item)
            if i == 0 {
                let car_image:String = json["car_type_image"][i].string!
                cell.downloadImage(URL(string: car_image as String)!)
            }
            i = i + 1
        }
        return cell as PilihMobilIdamanCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            
            let id_car_type : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "id_car_type") as! NSString
            let type_name : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "name") as! NSString
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(id_car_type as String, forKey: "id_car_type")
            prefs.set(type_name as String, forKey: "type_name")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_mobil_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            /*
            let promo_image : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            let promo_tanggal : NSString=(self.arrDict[indexPath.row] as AnyObject).value(forKey: "promo_date") as! NSString
            let content_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
            let title_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(promo_image as String, forKey: "imgPromo")
            prefs.set(content_promo as String, forKey: "contentPromo")
            prefs.set(title_promo as String, forKey: "titlePromo")
            prefs.set(promo_tanggal as String, forKey: "tanggalPromo")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo_detail")
             vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            */
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_new_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }

}
