//
//  PilihMobilIdamanCell.swift
//  Andalanku
//
//  Created by Macintosh on 27/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class PilihMobilIdamanCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMobil: UIImageView!
    @IBOutlet weak var viewPilih: UIView!
    @IBOutlet weak var lblHarga: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewPilih.layer.shadowColor = UIColor.lightGray.cgColor
        viewPilih.layer.shadowOpacity = 0.3
        viewPilih.layer.shadowOffset = CGSize.zero
        viewPilih.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.async() { () -> Void in
                guard let data = data, error == nil else { return }
                self.imgMobil.image = UIImage(data: data as Data)
            }
        }
    }
}
