//
//  PinAndalanku.swift
//  Andalanku
//
//  Created by Handoyo on 13/06/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material

class PinAndalanku: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var txtPIN: TextField!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.txtPIN.delegate = self
        self.txtPIN.textAlignment = .center
        self.txtPIN.textColor = UIColor.white
        self.txtPIN.placeholderNormalColor = UIColor.white
        self.txtPIN.placeholderActiveColor = UIColor.white
        self.txtPIN.dividerActiveColor = UIColor.white
        self.txtPIN.dividerNormalColor = UIColor.white
        self.txtPIN.detailColor = UIColor.white
        self.txtPIN.isEnabled = false
    }

    @IBAction func go1(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "1"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "1"
        }
    }
    
    @IBAction func go2(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "2"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "2"
        }
    }
    
    @IBAction func go3(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "3"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "3"
        }
    }
    
    @IBAction func go4(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "4"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "4"
        }
    }
    
    @IBAction func go5(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "5"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "5"
        }
    }
    
    @IBAction func go6(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "6"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "6"
        }
    }
    
    @IBAction func go7(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "7"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "7"
        }
    }
    
    @IBAction func go8(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "8"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "8"
        }
    }
    
    @IBAction func go9(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "9"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "9"
        }
    }
    
    @IBAction func go0(_ sender: Any) {
        if (txtPIN.text == "") {
            self.txtPIN.text = "0"
        } else {
            self.txtPIN.text = self.txtPIN.text! + "0"
        }
    }
    
    @IBAction func goLupa(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_ganti_pin")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goDelete(_ sender: Any) {
        let string = self.txtPIN.text?.characters.dropLast()
        self.txtPIN.text = String(string!)
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.selectedIndex = 3
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
