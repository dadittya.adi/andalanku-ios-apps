//
//  Plafond.swift
//  Andalanku
//
//  Created by Handoyo on 08/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class Plafond: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewPlafond: UIView!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var lblNama: UILabel!
    
    //@IBOutlet weak var imgNewCars: UIImageView!
    //@IBOutlet weak var imgUsedCars: UIImageView!
    //@IBOutlet weak var imgDana: UIImageView!
    //@IBOutlet weak var imgECommerce: UIImageView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        viewPlafond.layer.shadowColor = UIColor.black.cgColor
        viewPlafond.layer.shadowOpacity = 0.3
        viewPlafond.layer.shadowOffset = CGSize.zero
        viewPlafond.layer.cornerRadius = 10
        
        let prefs:UserDefaults = UserDefaults.standard
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        self.lblNama.text = "Hi, " + nama_akun!
        
        /*
        let tapImgNew = UITapGestureRecognizer(target:self, action:#selector(Plafond.goNewTap(_:)))
        self.imgNewCars.isUserInteractionEnabled = true
        self.imgNewCars.addGestureRecognizer(tapImgNew)
        
        let tapImgUsed = UITapGestureRecognizer(target:self, action:#selector(Plafond.goUsedTap(_:)))
        self.imgUsedCars.isUserInteractionEnabled = true
        self.imgUsedCars.addGestureRecognizer(tapImgUsed)
        
        let tapImgDana = UITapGestureRecognizer(target:self, action:#selector(Plafond.goDanaTap(_:)))
        self.imgDana.isUserInteractionEnabled = true
        self.imgDana.addGestureRecognizer(tapImgDana)
        
        let tapImgECommerce = UITapGestureRecognizer(target:self, action:#selector(Plafond.goECommerceTap(_:)))
        self.imgECommerce.isUserInteractionEnabled = true
        self.imgECommerce.addGestureRecognizer(tapImgECommerce)
        */
    }
    
    @objc func goNewTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_new_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goUsedTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_used_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goDanaTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dana")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goECommerceTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_e_commerce")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goSimulasi(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_simulasi_plafond")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let set_plafond = prefs.value(forKey: "set_plafond") as? String
            if set_plafond == "menu" {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                vc.selectedIndex = 3
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }

}
