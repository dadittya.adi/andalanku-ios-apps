//
//  Profil.swift
//  Andalanku
//
//  Created by Handoyo on 09/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Profil: UIViewController {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var lblLogout: UILabel!
    @IBOutlet var lbl_nama_lengkap: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var imgPengaturan: UIImageView!
    
    var save_last_id:String = "6"
    var save_last_name:String = "Donovan"
    var save_last_email:String = "user@example.com"
    var save_last_password:String = "password"
    
    @IBOutlet weak var lblPlafond: UILabel!
    @IBOutlet weak var rpPlafond: UILabel!
    @IBOutlet weak var arrowPlafond: UIImageView!
    
    @IBOutlet weak var lblPoin: UILabel!
    @IBOutlet weak var rpPoin: UILabel!
    @IBOutlet weak var arrowPoin: UIImageView!
    
    @IBOutlet weak var lblKeamanan: UILabel!
    @IBOutlet weak var arrowKeamanan: UIImageView!
    
    @IBOutlet weak var lblDokumen: UILabel!
    @IBOutlet weak var arrowDokumen: UIImageView!
    
    @IBOutlet weak var lblPIN: UILabel!
    @IBOutlet weak var arrowPIN: UIImageView!
    @IBOutlet weak var viewProfil: UIView!
    
    @IBOutlet weak var lblHelp: UILabel!
    @IBOutlet weak var arrowHelp: UIImageView!
    
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var arrowTerm: UIImageView!
    
    @IBOutlet weak var lblPrivacy: UILabel!
    @IBOutlet weak var arrowPrivacy: UIImageView!
    @IBOutlet weak var btnPerbarui: UIButton!
    
    @IBOutlet weak var lblVersion: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewProfil.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        let prefs:UserDefaults = UserDefaults.standard
        let id_customer = prefs.value(forKey: "id_customer") as? String
        let nama_akun = prefs.value(forKey: "nama_akun") as? String
        let email = prefs.value(forKey: "email") as? String
        let password = prefs.value(forKey: "password") as? String
        if id_customer != nil {
            save_last_id = id_customer!
            if password != nil {
                save_last_email = email!
                save_last_password = password!
            } else {
                save_last_email = "user@example.com"
                save_last_password = "password"
            }
        }
        
        if (nama_akun != nil && nama_akun != "") {
            save_last_name = nama_akun!
            lbl_nama_lengkap.text = nama_akun!
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
        if email != nil {
            lblEmail.text = email!
            save_last_email = email!
        }
        
        prefs.set("", forKey: "province")
        prefs.set("", forKey: "city")
        prefs.set("", forKey: "district")
        prefs.set("", forKey: "village")
        prefs.set("", forKey: "id_province")
        prefs.set("", forKey: "id_city")
        prefs.set("", forKey: "id_district")
        prefs.set("", forKey: "id_village")
        prefs.synchronize()
        
        self.btnPerbarui.layer.borderWidth = 1
        self.btnPerbarui.layer.borderColor = UIColor.white.cgColor
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapLblLogout = UITapGestureRecognizer(target:self, action:#selector(Profil.goLogoutTap(_:)))
        self.lblLogout.isUserInteractionEnabled = true
        self.lblLogout.addGestureRecognizer(tapLblLogout)
        
        let tapImgPengaturan = UITapGestureRecognizer(target:self, action:#selector(Profil.goPengaturanTap(_:)))
        self.imgPengaturan.isUserInteractionEnabled = true
        self.imgPengaturan.addGestureRecognizer(tapImgPengaturan)
        
        let tapLblPlafond = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.lblPlafond.isUserInteractionEnabled = true
        self.lblPlafond.addGestureRecognizer(tapLblPlafond)
        
        let tapRpPlafond = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.rpPlafond.isUserInteractionEnabled = true
        self.rpPlafond.addGestureRecognizer(tapRpPlafond)
        
        let tapArrowPlafond = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.arrowPlafond.isUserInteractionEnabled = true
        self.arrowPlafond.addGestureRecognizer(tapArrowPlafond)
        
        let tapLblPoin = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.lblPoin.isUserInteractionEnabled = true
        self.lblPoin.addGestureRecognizer(tapLblPoin)
        
        let tapRpPoin = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.rpPoin.isUserInteractionEnabled = true
        self.rpPoin.addGestureRecognizer(tapRpPoin)
        
        let tapArrowPoin = UITapGestureRecognizer(target:self, action:#selector(Profil.goPlafondTap(_:)))
        self.arrowPoin.isUserInteractionEnabled = true
        self.arrowPoin.addGestureRecognizer(tapArrowPoin)
        
        let tapLblKeamanan = UITapGestureRecognizer(target:self, action:#selector(Profil.goKeamananTap(_:)))
        self.lblKeamanan.isUserInteractionEnabled = true
        self.lblKeamanan.addGestureRecognizer(tapLblKeamanan)
        
        let tapArrowKemanan = UITapGestureRecognizer(target:self, action:#selector(Profil.goKeamananTap(_:)))
        self.arrowKeamanan.isUserInteractionEnabled = true
        self.arrowKeamanan.addGestureRecognizer(tapArrowKemanan)
        
        let tapLblDokumen = UITapGestureRecognizer(target:self, action:#selector(Profil.goDokumenTap(_:)))
        self.lblDokumen.isUserInteractionEnabled = true
        self.lblDokumen.addGestureRecognizer(tapLblDokumen)
        
        let tapArrowDokumen = UITapGestureRecognizer(target:self, action:#selector(Profil.goDokumenTap(_:)))
        self.arrowDokumen.isUserInteractionEnabled = true
        self.arrowDokumen.addGestureRecognizer(tapArrowDokumen)
        
        let tapLblPIN = UITapGestureRecognizer(target:self, action:#selector(Profil.goPINTap(_:)))
        self.lblPIN.isUserInteractionEnabled = true
        self.lblPIN.addGestureRecognizer(tapLblPIN)
        
        let tapArrowPIN = UITapGestureRecognizer(target:self, action:#selector(Profil.goPINTap(_:)))
        self.arrowPIN.isUserInteractionEnabled = true
        self.arrowPIN.addGestureRecognizer(tapArrowPIN)
        
        let tapLblHelp = UITapGestureRecognizer(target:self, action:#selector(Profil.goHelpTap(_:)))
        self.lblHelp.isUserInteractionEnabled = true
        self.lblHelp.addGestureRecognizer(tapLblHelp)
        
        let tapArrowHelp = UITapGestureRecognizer(target:self, action:#selector(Profil.goHelpTap(_:)))
        self.arrowHelp.isUserInteractionEnabled = true
        self.arrowHelp.addGestureRecognizer(tapArrowHelp)
        
        let tapLblTerm = UITapGestureRecognizer(target:self, action:#selector(Profil.goTermTap(_:)))
        self.lblTerm.isUserInteractionEnabled = true
        self.lblTerm.addGestureRecognizer(tapLblTerm)
        
        let tapArrowTerm = UITapGestureRecognizer(target:self, action:#selector(Profil.goTermTap(_:)))
        self.arrowTerm.isUserInteractionEnabled = true
        self.arrowTerm.addGestureRecognizer(tapArrowTerm)
        
        let tapLblPrivacy = UITapGestureRecognizer(target:self, action:#selector(Profil.goPrivacyTap(_:)))
        self.lblPrivacy.isUserInteractionEnabled = true
        self.lblPrivacy.addGestureRecognizer(tapLblPrivacy)
        
        let tapArrowPrivacy = UITapGestureRecognizer(target:self, action:#selector(Profil.goPrivacyTap(_:)))
        self.arrowPrivacy.isUserInteractionEnabled = true
        self.arrowPrivacy.addGestureRecognizer(tapArrowPrivacy)
    }
    
    @objc func goPlafondTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("menu", forKey: "set_plafond")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_plafond")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goKeamananTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pengaturan")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goDokumenTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dokumen_kontrak")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goPINTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_ganti_pin")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goHelpTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.selectedIndex = 1
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goTermTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_term")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goPrivacyTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_privacy")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goPengaturanTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pengaturan_akun")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @objc func goLogoutTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let appDomain = Bundle.main.bundleIdentifier
            UserDefaults.standard.removePersistentDomain(forName: appDomain!)
            
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(self.save_last_id, forKey: "id_customer")
            prefs.set("", forKey: "nama_akun")
            prefs.set(self.save_last_email, forKey: "email")
            prefs.set(self.save_last_password, forKey: "password")
            prefs.synchronize()
            
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_launch")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goPerbarui(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_perbarui_diri")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
}
