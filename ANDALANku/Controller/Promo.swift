//
//  Promo.swift
//  Andalanku
//
//  Created by Handoyo on 02/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Promo: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var tvJSON: UITableView!
    var arrPromoLabel = ["01 - 30 April","10 - 15 April","01 - 30 April","05 - 15 April"]
    var arrPromoImage: [UIImage] = [
        UIImage(named: "sample_promo")!,
        UIImage(named: "sample_promo2")!,
        UIImage(named: "sample_promo3")!,
        UIImage(named: "sample_promo4")!
    ]
    
    @IBOutlet var load: UIActivityIndicatorView!
    
    var arrDict :NSMutableArray=[]
    var par_num = 0
    var limit = 4
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(Promo.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor(rgb: 0x069DE4)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.tvJSON.separatorStyle = .none
        self.tvJSON.reloadData()
        
        self.tvJSON.delegate = self
        self.tvJSON.dataSource = self
        //self.tvJSON.addSubview(self.refreshControl)
        self.tvJSON.tableFooterView = UIView(frame: .zero)
        self.load.isHidden = true
        
        DispatchQueue.main.async(execute: {
            
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "keyword": "",
                "page": 1
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["promo_list"].arrayObject != nil {
                                self.arrDict.removeAllObjects()
                                if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.par_num = self.par_num + self.limit
                                self.tvJSON.reloadData()
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "keyword": "",
                "page": self.par_num
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["promo_list"].arrayObject != nil {
                                if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.par_num = self.par_num + self.limit
                                self.tvJSON.reloadData()
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
    }
    
    @objc func loadBottom() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "keyword": "",
                "page": self.par_num
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "promo_list"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                        if let value = response.result.value {
                            let json = JSON(value)
                            if json["promo_list"].arrayObject != nil {
                                if let past : NSArray = json["promo_list"].arrayObject as NSArray? {
                                    print(past)
                                    for i in 0 ..< (json["promo_list"].arrayObject! as NSArray).count
                                    {
                                        self.arrDict.add((json["promo_list"].arrayObject! as NSArray) .object(at: i))
                                    }
                                }
                                self.par_num = self.par_num + self.limit
                                self.tvJSON.reloadData()
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                        }
                    }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PromoCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? PromoCell
        
        let promo_tanggal : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "promo_date") as! NSString
    
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: promo_tanggal as String) {
            print(dateFormatterPrint.string(from: date))
            cell.tglPromo.text = dateFormatterPrint.string(from: date)
        } else {
            cell.tglPromo.text = promo_tanggal as String
        }
        
        let promo_image : NSString=(arrDict[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
        cell.downloadImage(URL(string: promo_image as String)!)
        
        return cell as PromoCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            self.loadBottom()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            let promo_image : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "image") as! NSString
            let promo_tanggal : NSString=(self.arrDict[indexPath.row] as AnyObject).value(forKey: "promo_date") as! NSString
            let content_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "content") as! NSString
            let title_promo : NSString = (self.arrDict[indexPath.row] as AnyObject).value(forKey: "title") as! NSString
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(promo_image as String, forKey: "imgPromo")
            prefs.set(content_promo as String, forKey: "contentPromo")
            prefs.set(title_promo as String, forKey: "titlePromo")
            prefs.set(promo_tanggal as String, forKey: "tanggalPromo")
            prefs.synchronize()
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_promo_detail")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let set_promo = prefs.value(forKey: "set_promo") as? String
            if set_promo == "main" {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_main")
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
