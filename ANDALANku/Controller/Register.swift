//
//  Register.swift
//  Andalanku
//
//  Created by Handoyo on 30/03/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON

class Register: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var imgEyesPass: UIImageView!
    @IBOutlet var imgEyesRetype: UIImageView!
    @IBOutlet var imgMain: UIImageView!
    @IBOutlet var txt_nama_lengkap: TextField!
    @IBOutlet var txt_no_ktp: TextField!
    @IBOutlet var txt_no_hp: TextField!
    @IBOutlet var txt_email: TextField!
    @IBOutlet var txt_password: TextField!
    @IBOutlet var txt_retype: TextField!
    @IBOutlet var txt_referral: TextField!
    var iconClick = true
    @IBOutlet var lblLogin: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var viewMain: UIView!
    var sizeViewHeight:CGFloat = 0.0
    @IBOutlet var lblKetentuan: UILabel!
    var myString:NSString = ""
    var myMutableString = NSMutableAttributedString()
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.viewMain.backgroundColor = UIColor(patternImage: UIImage(named: "bg_header")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        let mainFrame = self.view.frame
        self.sizeViewHeight = mainFrame.size.height
        
        self.myString = self.lblKetentuan.text! as NSString
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 11.0)!])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(rgb: 0x069DE4), range: NSRange(location:35,length:18))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(rgb: 0x069DE4), range: NSRange(location:57,length:17))
        
        self.lblKetentuan.attributedText = myMutableString
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let underlineAttributedString = NSAttributedString(string: self.lblLogin.text!, attributes: underlineAttribute)
        self.lblLogin.attributedText = underlineAttributedString
        
        self.txt_nama_lengkap.delegate = self
        self.txt_no_ktp.delegate = self
        self.txt_no_hp.delegate = self
        self.txt_email.delegate = self
        self.txt_password.delegate = self
        self.txt_retype.delegate = self
        self.txt_referral.delegate = self
        
        self.txt_nama_lengkap.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_nama_lengkap.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_nama_lengkap.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_nama_lengkap.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_no_ktp.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_no_ktp.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_no_ktp.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_no_ktp.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_no_hp.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_no_hp.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_no_hp.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_no_hp.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_email.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_email.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_email.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_email.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_password.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_password.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_password.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_password.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_retype.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_retype.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_retype.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_retype.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_referral.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_referral.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_referral.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_referral.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.load.isHidden = true
        
        let tapPass = UITapGestureRecognizer(target:self, action:#selector(Register.goPassTap(_:)))
        self.imgEyesPass.isUserInteractionEnabled = true
        self.imgEyesPass.addGestureRecognizer(tapPass)
        
        let tapMain = UITapGestureRecognizer(target:self, action:#selector(Register.goMainTap(_:)))
        self.imgMain.isUserInteractionEnabled = true
        self.imgMain.addGestureRecognizer(tapMain)
        
        let tapView = UITapGestureRecognizer(target:self, action:#selector(Register.goViewTap(_:)))
        self.viewMain.isUserInteractionEnabled = true
        self.viewMain.addGestureRecognizer(tapView)
        
        let tapRetype = UITapGestureRecognizer(target:self, action:#selector(Register.goRetypeTap(_:)))
        self.imgEyesRetype.isUserInteractionEnabled = true
        self.imgEyesRetype.addGestureRecognizer(tapRetype)
        
        let tapLogin = UITapGestureRecognizer(target:self, action:#selector(Register.goLoginTap(_:)))
        self.lblLogin.isUserInteractionEnabled = true
        self.lblLogin.addGestureRecognizer(tapLogin)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Register.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(Register.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        let info = sender.userInfo!
        let keyboardHeight:CGFloat = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size.height
        
        var frame = self.view.frame
        
        frame.size.height = sizeViewHeight - keyboardHeight
        self.view.frame = frame
        self.imgMain.isHidden = true
    }
    
    @objc func keyboardWillHide(_ sender: Notification) {
        var frame = self.view.frame
        
        frame.size.height = sizeViewHeight
        self.view.frame = frame
        self.imgMain.isHidden = false
    }
    
    @objc func goPassTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            if(self.iconClick == true) {
                self.txt_password.isSecureTextEntry = false
                self.imgEyesPass.image = UIImage(named: "icon_mata")
            } else {
                self.txt_password.isSecureTextEntry = true
                self.imgEyesPass.image = UIImage(named: "icon_mata")
            }
            self.iconClick = !self.iconClick
        })
    }
    
    @objc func goRetypeTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            if(self.iconClick == true) {
                self.txt_retype.isSecureTextEntry = false
                self.imgEyesRetype.image = UIImage(named: "icon_mata")
            } else {
                self.txt_retype.isSecureTextEntry = true
                self.imgEyesRetype.image = UIImage(named: "icon_mata")
            }
            self.iconClick = !self.iconClick
        })
    }
    
    @objc func goMainTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_nama_lengkap.resignFirstResponder()
            self.txt_no_ktp.resignFirstResponder()
            self.txt_no_hp.resignFirstResponder()
            self.txt_email.resignFirstResponder()
            self.txt_password.resignFirstResponder()
            self.txt_retype.resignFirstResponder()
            self.txt_referral.resignFirstResponder()
        })
    }
    
    @objc func goViewTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_nama_lengkap.resignFirstResponder()
            self.txt_no_ktp.resignFirstResponder()
            self.txt_no_hp.resignFirstResponder()
            self.txt_email.resignFirstResponder()
            self.txt_password.resignFirstResponder()
            self.txt_retype.resignFirstResponder()
            self.txt_referral.resignFirstResponder()
        })
    }
    
    @objc func goLoginTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }

    @IBAction func goAkun(_ sender: Any) {
        setOTP()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if (textField == self.txt_nama_lengkap) {
            self.txt_no_ktp.becomeFirstResponder()
        } else if (textField == self.txt_no_ktp) {
                self.txt_no_hp.becomeFirstResponder()
        } else if (textField == self.txt_no_hp) {
            self.txt_email.becomeFirstResponder()
        } else if (textField == self.txt_email) {
            self.txt_password.becomeFirstResponder()
        } else if (textField == self.txt_password) {
            self.txt_retype.becomeFirstResponder()
        } else if (textField == self.txt_retype) {
            self.txt_referral.becomeFirstResponder()
        } else if (textField == self.txt_referral) {
            setOTP()
        }
        return true
    }
    
    func setOTP() {
        DispatchQueue.main.async(execute: {
            if self.txt_password.text == self.txt_retype.text {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                let parameters = [
                    "customer_name": self.txt_nama_lengkap.text!,
                    "id_number": self.txt_no_ktp.text!,
                    "email": self.txt_email.text!,
                    "password": self.txt_password.text!,
                    "phone_number": self.txt_no_hp.text!
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "register"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                let status = json["status"].stringValue
                                let id_customer = json["id_customer"].stringValue
                                if status == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(id_customer, forKey: "id_customer")
                                    prefs.set("register", forKey: "set_otp")
                                    prefs.set(self.txt_nama_lengkap.text!, forKey: "nama_akun")
                                    prefs.set(self.txt_email.text!, forKey: "email")
                                    prefs.set(self.txt_password.text!, forKey: "password")
                                    prefs.synchronize()
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            }
                        }
                }
            } else {
                let alertController = UIAlertController(title: "", message: "Check your password!", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        })
    }
    
    @IBAction func goBack2(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
