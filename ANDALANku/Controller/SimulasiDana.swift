//
//  SimulasiDana.swift
//  Andalanku
//
//  Created by Handoyo on 16/06/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SimulasiDana: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtRincian: UILabel!
    @IBOutlet weak var lblDanaDiterima: UILabel!
    @IBOutlet weak var lblPokokPinjaman: UILabel!
    @IBOutlet weak var lblTenor: UILabel!
    @IBOutlet weak var lblAngsuran: UILabel!
    @IBOutlet weak var lblBiayaAdmin: UILabel!
    @IBOutlet weak var lblProvisi: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    @IBOutlet weak var lblAdvance: UILabel!
    @IBOutlet weak var indicAdvance: UIView!
    @IBOutlet weak var lblArrear: UILabel!
    @IBOutlet weak var indicArrear: UIView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.indicArrear.isHidden = true
        
        let prefs:UserDefaults = UserDefaults.standard
        //let pokok_pinjaman = prefs.value(forKey: "pokok_pinjaman") as? String
        let tenor = prefs.value(forKey: "tenor") as? String
        let angsuran = prefs.value(forKey: "angsuran") as? String
        let biaya_admin = prefs.value(forKey: "biaya_admin") as? String
        let provisi = prefs.value(forKey: "provisi") as? String
        let dana_diterima = prefs.value(forKey: "dana_diterima") as? String
        let pengajuan_pinjaman = prefs.value(forKey: "pengajuan_pinjaman") as? String
        
        self.lblPokokPinjaman.text = "Rp. " + pengajuan_pinjaman!
        self.lblTenor.text = tenor! + " Bulan"
        self.lblBiayaAdmin.text = "Rp. " + biaya_admin!
        self.lblAngsuran.text = "Rp. " + angsuran!
        self.lblProvisi.text = "Rp. " + provisi!
        self.lblDanaDiterima.text = "Rp. " + dana_diterima!
        
        let text = """
            Rincian simulasi di atas bersifat estimasi dan tidak mengikat dan dapat berubah sewaktu-waktu mengikuti kebijakan yang berlaku.
            """
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        txtRincian.attributedText = NSAttributedString(attributedListString: attributedText, withFont: txtRincian.font)
    }
    
    @IBAction func goApply(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_otp = prefs.value(forKey: "set_otp") as? String
        if (set_otp != nil)
        {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let parameters = [
                    "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                    "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            if let value = response.result.value {
                                let json = JSON(value)
                                let status = json["status"].stringValue
                                let token_number = json["token_number"].stringValue
                                if status == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                    prefs.set(token_number, forKey: "token_number")
                                    prefs.synchronize()
                                    
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_formulir_pembiayaan")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                }
            })
        } else {
            getMain()
        }
    }
    
    func getMain() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goUlang(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dana")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_dana")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
