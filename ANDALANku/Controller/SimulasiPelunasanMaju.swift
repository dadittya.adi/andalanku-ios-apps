//
//  SimulasiPelunasanMaju.swift
//  Andalanku
//
//  Created by Handoyo on 30/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Material
import SPStorkController

class SimulasiPelunasanMaju: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var txtNoKontrak: TextField!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var txtJatuhTempo: TextField!
    @IBOutlet weak var txtSisaPinjaman: TextField!
    @IBOutlet weak var txtTanggal: TextField!
    @IBOutlet weak var viewHasil: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var lblSisaPinjaman: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    var datePicker: UIDatePicker!
    let dateFormatter = DateFormatter()
    let timeFormatter = DateFormatter()
    @IBOutlet weak var checkSetuju: CCheckbox!
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
            let sisa_pinjam = prefs.value(forKey: "sisa_pinjam") as? String
            if (no_kontrak != nil && no_kontrak != "") {
                self.txtNoKontrak.text = no_kontrak
                if (sisa_pinjam != nil && sisa_pinjam != "") {
                    self.txtSisaPinjaman.text = "Rp. " + sisa_pinjam!
                }
                
                DispatchQueue.main.async(execute: {
                    
                    self.load.isHidden = false
                    self.load.startAnimating()
                    
                    let prefs:UserDefaults = UserDefaults.standard
                    let api_key = prefs.value(forKey: "api_key") as? String
                    let token_number = prefs.value(forKey: "token_number") as? String
                    let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                    let id_customer = prefs.value(forKey: "id_customer") as? String
                    
                    let headers = [
                        "api_key": api_key,
                        "token_number": token_number
                    ]
                    
                    let parameters = [
                        "agreement_no": no_kontrak!,
                        "id_customer": id_customer!
                        ] as [String : Any]
                    
                    let appConfig : app_config = app_config()
                    Alamofire.request(appConfig.getApiUrl(type: "andalan_financial"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                        .responseJSON { response in
                            
                            if (response.result.value == nil) {
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    print(json)
                                    
                                    if json["status"].stringValue == "success" {
                                        for item in json["andalan_detail_financial"].arrayValue {
                                            let dtString:String = item["maturity_date"].stringValue
                                            let dateString:String = dtString.replacingOccurrences(of: "T", with: " ")
                                            
                                            let dateFormatterGet = DateFormatter()
                                            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                            
                                            let dateFormatterPrint = DateFormatter()
                                            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
                                            
                                            if let date = dateFormatterGet.date(from: dateString) {
                                                self.txtJatuhTempo.text = dateFormatterPrint.string(from: date)
                                            } else {
                                                self.txtJatuhTempo.text = item["maturity_date"].stringValue
                                            }
                                        }
                                    }
                                }
                                
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                    }
                })
            }
        })
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_new")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapImgDown = UITapGestureRecognizer(target:self, action:#selector(DokumenKontrak.goDownTap(_:)))
        self.imgDown.isUserInteractionEnabled = true
        self.imgDown.addGestureRecognizer(tapImgDown)
        self.txtNoKontrak.delegate = self
        self.load.isHidden = true
        
        self.txtNoKontrak.textColor = UIColor.white
        self.txtSisaPinjaman.textColor = UIColor.white
        self.txtJatuhTempo.textColor = UIColor.white
        
        self.datePicker = UIDatePicker()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        timeFormatter.dateFormat = "HH:mm:ss"
        
        let dateA = dateFormatter.date(from: dateFormatter.string(from: datePicker.date))
        
        let dateBirth = dateA?.addingTimeInterval(60 * 90 * 24 * 1)
        self.datePicker.date = dateBirth!
        self.datePicker.datePickerMode = .date
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(rgb: 0x5DC4F8)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(SimulasiPelunasanMaju.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(SimulasiPelunasanMaju.donePicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.txtTanggal.inputView = self.datePicker
        self.txtTanggal.inputAccessoryView = toolBar
        
        self.datePicker.addTarget(self, action: #selector(SimulasiPelunasanMaju.handleDatePicker(_:)), for: UIControl.Event.valueChanged)
        
        /*
        viewHasil.translatesAutoresizingMaskIntoConstraints = false
        let heightTitle3 = NSLayoutConstraint(item: viewHasil, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.0)
        NSLayoutConstraint.activate([heightTitle3])
        self.viewHasil.isHidden = true
        */
    }
    
    @objc func donePicker() {
        txtTanggal.resignFirstResponder()
    }
    
    @objc func handleDatePicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtTanggal.text = dateFormatter.string(from: sender.date)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            //return justGender.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        print(pickerView.tag)
        if pickerView.tag == 0 {
            //return justGender[row]
        }
        return " "
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerView.tag == 0 {
            //txtGender.text = justGender[row]
        }
    }
    
    @IBAction func goTxtNoKontrakBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("pelunasan_maju", forKey: "set_no_kontrak")
            prefs.synchronize()
            self.txtNoKontrak.resignFirstResponder()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goDownTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("pelunasan_maju", forKey: "set_no_kontrak")
            prefs.synchronize()
            let controller = ModalPilihNoKontrak()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }

    @IBAction func goSubmit(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            /*
            self.viewHasil.isHidden = false
            self.viewHasil.translatesAutoresizingMaskIntoConstraints = false
            let heightTitle3 = NSLayoutConstraint(item: self.viewHasil, attribute: .height, relatedBy: .equal, toItem: self.viewHome, attribute: .notAnAttribute, multiplier: 1.0, constant: 279.0)
            NSLayoutConstraint.activate([heightTitle3])
            */
            if self.checkSetuju.isCheckboxSelected == true {
                let txt = self.btnSubmit.titleLabel!.text
                if txt == "SUBMIT" {
                    let prefs:UserDefaults = UserDefaults.standard
                    let api_key = prefs.value(forKey: "api_key") as? String
                    let token_number = prefs.value(forKey: "token_number") as? String
                    let id_customer = prefs.value(forKey: "id_customer") as? String
                    let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                    
                    let headers = [
                        "api_key": api_key,
                        "token_number": token_number
                    ]
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    self.load.isHidden = false
                    self.load.startAnimating()
                    
                    let parameters = [
                        "id_customer": id_customer!,
                        "agreement_no": no_kontrak!,
                        "prepayment_date": self.txtTanggal.text!
                        ] as [String : Any]
                    
                    let appConfig : app_config = app_config()
                    Alamofire.request(appConfig.getApiUrl(type: "prepayment_simulation"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                        .responseJSON { response in
                            
                            if (response.result.value == nil) {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    print(json)
                                    if json["status"].stringValue == "success" {
                                        let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                            self.lblSisaPinjaman.text = "Rp. " + json["sisaPinjaman"].stringValue
                                            self.lblTotal.text = "Rp. " + json["totalPelunasan"].stringValue
                                            self.btnSubmit.setTitle("AJUKAN PELUNASAN", for: .normal)
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    } else {
                                        let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    }
                                }
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                    }
                } else {
                    let prefs:UserDefaults = UserDefaults.standard
                    let api_key = prefs.value(forKey: "api_key") as? String
                    let token_number = prefs.value(forKey: "token_number") as? String
                    let id_customer = prefs.value(forKey: "id_customer") as? String
                    let no_kontrak = prefs.value(forKey: "no_kontrak") as? String
                    
                    let headers = [
                        "api_key": api_key,
                        "token_number": token_number
                    ]
                    
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    self.load.isHidden = false
                    self.load.startAnimating()
                    
                    let parameters = [
                        "id_customer": id_customer!,
                        "agreement_no": no_kontrak!,
                        "prepayment_date": self.txtTanggal.text!
                        ] as [String : Any]
                    
                    let appConfig : app_config = app_config()
                    Alamofire.request(appConfig.getApiUrl(type: "prepayment_request"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                        .responseJSON { response in
                            
                            if (response.result.value == nil) {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                self.load.isHidden = true
                                self.load.stopAnimating()
                                let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    print(json)
                                    if json["status"].stringValue == "success" {
                                        let alertController = UIAlertController(title: "Pengajuan Pelunasan Maju", message: "Formulir Pengajuan pelunasan kredit Anda sudah kami terima, untuk mengetahui status pengajuan anda lihat progres nya di Halaman Pesan.", preferredStyle: .alert)
                                        let KembaliAction = UIAlertAction(title: "KEMBALI", style: .cancel) { (action:UIAlertAction!) in
                                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                            vc.modalPresentationStyle = .fullScreen
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                        let LihatAction = UIAlertAction(title: "LIHAT", style: .default) { (action:UIAlertAction!) in
                                            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                            vc.selectedIndex = 2
                                            vc.modalPresentationStyle = .fullScreen
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                        alertController.addAction(KembaliAction)
                                        alertController.addAction(LihatAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    } else {
                                         let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                         let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                         }
                                         alertController.addAction(OKAction)
                                         self.present(alertController, animated: true, completion:nil)
                                    }
                                }
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                self.load.isHidden = true
                                self.load.stopAnimating()
                            }
                    }
                }
            } else {
                let alertController = UIAlertController(title: "", message: "Please accept to continue", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
