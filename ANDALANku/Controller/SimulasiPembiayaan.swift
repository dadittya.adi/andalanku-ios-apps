//
//  SimulasiPembiayaan.swift
//  Andalanku
//
//  Created by Handoyo on 30/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class SimulasiPembiayaan: UIViewController {

    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet weak var menuBarView: MenuTabsView!
    
    var currentIndex: Int = 0
    var tabs = ["ADVANCE","ARREAR"]
    var pageController: UIPageViewController!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        menuBarView.dataArray = tabs
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.collView.backgroundColor = UIColor.clear
        
        presentPageVCOnView()
        
        menuBarView.menuDelegate = self
        pageController.delegate = self
        pageController.dataSource = self
        
        menuBarView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        
        pageController.setViewControllers([viewController(At: 0)!], direction: .forward, animated: true, completion: nil)
    }
    
    func presentPageVCOnView() {
        
        self.pageController = storyboard?.instantiateViewController(withIdentifier: "page_complain") as! PageComplain
        self.pageController.view.frame = CGRect.init(x: 0, y: menuBarView.frame.maxY, width: self.view.frame.width, height: self.view.frame.height - menuBarView.frame.maxY)
        self.addChild(self.pageController)
        self.view.addSubview(self.pageController.view)
        self.pageController.didMove(toParent: self)
        
    }
    
    func viewController(At index: Int) -> UIViewController? {
        
        if((self.menuBarView.dataArray.count == 0) || (index >= self.menuBarView.dataArray.count)) {
            return nil
        }
        
        if (index == 0) {
            let Menu2 = storyboard?.instantiateViewController(withIdentifier: "story_pembayaran_advance") as! PembayaranAdvance
            Menu2.pageIndex = index
            currentIndex = index
            return Menu2
        } else if (index == 1) {
            let Menu1 = storyboard?.instantiateViewController(withIdentifier: "story_pembayaran_arrear") as! PembayaranArrear
            Menu1.pageIndex = index
            currentIndex = index
            return Menu1
        } else {
            return nil
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SimulasiPembiayaan: MenuBarDelegate {
    
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        
        if index != currentIndex {
            
            if index > currentIndex {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            } else {
                self.pageController.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            
            menuBarView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
}

extension SimulasiPembiayaan: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = 0
        
        if viewController.isKind(of: PembayaranAdvance.self) {
            index = (viewController as! PembayaranAdvance).pageIndex
        } else if (viewController.isKind(of: PembayaranArrear.self)) {
            index = (viewController as! PembayaranArrear).pageIndex
        }
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewController(At: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = 0
        
        if viewController.isKind(of: PembayaranAdvance.self) {
            index = (viewController as! PembayaranAdvance).pageIndex
        } else if (viewController.isKind(of: PembayaranArrear.self)) {
            index = (viewController as! PembayaranArrear).pageIndex
        }
        
        if (index == tabs.count) || (index == NSNotFound) {
            return nil
        }
        
        index += 1
        return self.viewController(At: index)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished {
            if completed {
                
                if (pageViewController.viewControllers?.first?.isKind(of: PembayaranAdvance.self))! {
                    let cvc = pageViewController.viewControllers!.first as! PembayaranAdvance
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                } else if (pageViewController.viewControllers?.first?.isKind(of: PembayaranArrear.self))! {
                    let cvc = pageViewController.viewControllers!.first as! PembayaranArrear
                    let newIndex = cvc.pageIndex
                    menuBarView.collView.selectItem(at: IndexPath.init(item: newIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
                    menuBarView.collView.scrollToItem(at: IndexPath.init(item: newIndex, section: 0), at: .centeredHorizontally, animated: true)
                }
                
            }
        }
        
    }
    
}
