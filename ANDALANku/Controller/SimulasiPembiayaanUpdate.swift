//
//  SimulasiPembiayaanUpdate.swift
//  Andalanku
//
//  Created by Handoyo on 06/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SimulasiPembiayaanUpdate: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    @IBOutlet weak var lblAdvance: UILabel!
    @IBOutlet weak var indicAdvance: UIView!
    @IBOutlet weak var lblArrear: UILabel!
    @IBOutlet weak var indicArrear: UIView!
    @IBOutlet weak var scrollAdvance: UIScrollView!
    @IBOutlet weak var scrollArrear: UIScrollView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var lblPokokPinjaman: UILabel!
    @IBOutlet weak var lblTenor: UILabel!
    //@IBOutlet weak var lblPembayaranPertama: UILabel!
    @IBOutlet weak var lblAngsuran: UILabel!
    
    @IBOutlet weak var lblPokokPinjamanArrear: UILabel!
    @IBOutlet weak var lblTenorArrear: UILabel!
    //@IBOutlet weak var lblPembayaranPertamaArrear: UILabel!
    @IBOutlet weak var lblAngsuranArrear: UILabel!
    
    @IBOutlet weak var txtRincian: UILabel!
    @IBOutlet weak var txtRincianArrear: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.indicArrear.isHidden = true
        self.scrollArrear.isHidden = true
        
        let tapLblAdvance = UITapGestureRecognizer(target:self, action:#selector(SimulasiPembiayaanUpdate.goAdvanceTap(_:)))
        self.lblAdvance.isUserInteractionEnabled = true
        self.lblAdvance.addGestureRecognizer(tapLblAdvance)
        
        let tapLblArrear = UITapGestureRecognizer(target:self, action:#selector(SimulasiPembiayaanUpdate.goArrearTap(_:)))
        self.lblArrear.isUserInteractionEnabled = true
        self.lblArrear.addGestureRecognizer(tapLblArrear)
        
        let prefs:UserDefaults = UserDefaults.standard
        let pokok_pinjaman = prefs.value(forKey: "pokok_pinjaman") as? String
        let tenor = prefs.value(forKey: "tenor") as? String
        // pembayaran_pertama = prefs.value(forKey: "pembayaran_pertama") as? String
        let angsuran = prefs.value(forKey: "angsuran") as? String
        
        let pokok_pinjaman_arrear = prefs.value(forKey: "pokok_pinjaman_arrear") as? String
        let tenor_arrear = prefs.value(forKey: "tenor_arrear") as? String
        //let pembayaran_pertama_arrear = prefs.value(forKey: "pembayaran_pertama_arrear") as? String
        let angsuran_arrear = prefs.value(forKey: "angsuran_arrear") as? String
        
        self.lblPokokPinjaman.text = "Rp." + pokok_pinjaman!
        self.lblTenor.text = tenor! + " Bulan"
        //self.lblPembayaranPertama.text = "Rp. " + pembayaran_pertama!
        self.lblAngsuran.text = "Rp. " + angsuran!
        
        self.lblPokokPinjamanArrear.text = "Rp." + pokok_pinjaman_arrear!
        self.lblTenorArrear.text = tenor_arrear! + " Bulan"
        //self.lblPembayaranPertamaArrear.text = "Rp. " + pembayaran_pertama!
        self.lblAngsuranArrear.text = "Rp. " + angsuran_arrear!
        self.load.isHidden = true
        
        let text = """
            • Rincian simulasi di atas bersifat estimasi dan tidak mengikat dan dapat berubah sewaktu-waktu mengkuti kebijakan yang berlaku.
            • Total Pembayaran Pertama sudah termasuk Down Payment (DP) Nett, biaya admin, dan biaya asuransi.
            • Untuk informasi lengkap, silahkan menghubungi Kantor Cabang Andalan Finance terdekat.
            """
        
        let lineHeightStyle = NSMutableParagraphStyle()
        lineHeightStyle.lineHeightMultiple = 1.3
        lineHeightStyle.alignment = .left
        
        let attributedText = NSAttributedString(string: text, attributes: [.paragraphStyle: lineHeightStyle])
        txtRincian.attributedText = NSAttributedString(attributedListString: attributedText, withFont: txtRincian.font)
        txtRincianArrear.attributedText = NSAttributedString(attributedListString: attributedText, withFont: txtRincianArrear.font)
    }
    
    @IBAction func goApply(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_otp = prefs.value(forKey: "set_otp") as? String
        if (set_otp != nil)
        {
            DispatchQueue.main.async(execute: {
                self.load.isHidden = false
                self.load.startAnimating()
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                let parameters = [
                    "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                    "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            if let value = response.result.value {
                                let json = JSON(value)
                                let status = json["status"].stringValue
                                let token_number = json["token_number"].stringValue
                                if status == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                    prefs.set(token_number, forKey: "token_number")
                                    prefs.synchronize()
                                    
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_formulir_pembiayaan")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                    /*
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                     vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                    */
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                }
            })
        } else {
            getMain()
        }
    }
    
    func getMain() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let parameters = [
                "api_key": "1BA3A9444B0C4EB8F344EDB588E5101E",
                "api_secret": "82A9AD292A4FAF4B9C029BDB88D2DAB0"
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_token"), method: .post, parameters: parameters)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            let status = json["status"].stringValue
                            let token_number = json["token_number"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1BA3A9444B0C4EB8F344EDB588E5101E", forKey: "api_key")
                                prefs.set(token_number, forKey: "token_number")
                                prefs.synchronize()
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                                vc.modalPresentationStyle = .fullScreen
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goUlang(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_pembiayaan = prefs.value(forKey: "set_pembiayaan") as? String
        
        if (set_pembiayaan == "new_cars") {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_new_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_pembiayaan_used_cars")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func goAdvanceTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.indicAdvance.isHidden = false
            self.lblAdvance.textColor = UIColor(rgb: 0x4653B3)
            self.indicArrear.isHidden = true
            self.lblArrear.textColor = UIColor.black
            self.scrollAdvance.isHidden = false
            self.scrollArrear.isHidden = true
        })
    }
    
    @objc func goArrearTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.indicArrear.isHidden = false
            self.lblArrear.textColor = UIColor(rgb: 0x4653B3)
            self.indicAdvance.isHidden = true
            self.lblAdvance.textColor = UIColor.black
            self.scrollAdvance.isHidden = true
            self.scrollArrear.isHidden = false
        })
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
