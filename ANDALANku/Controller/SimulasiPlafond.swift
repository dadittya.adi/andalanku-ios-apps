//
//  SimulasiPlafond.swift
//  Andalanku
//
//  Created by Handoyo on 22/06/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON
import SPStorkController

class SimulasiPlafond: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var txtOccupation: TextField!
    @IBOutlet weak var arrowOccupation: UIImageView!
    @IBOutlet weak var txtPenghasilanPerBulan: TextField!
    @IBOutlet weak var txtPenghasilanTambahan: TextField!
    @IBOutlet weak var txtSumberPenghasilan: TextField!
    @IBOutlet weak var txtNamaLengkapIbu: TextField!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var checkSetuju: CCheckbox!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapArrowOccupation = UITapGestureRecognizer(target:self, action:#selector(SimulasiPlafond.goOccupTap(_:)))
        self.arrowOccupation.isUserInteractionEnabled = true
        self.arrowOccupation.addGestureRecognizer(tapArrowOccupation)
        
        self.txtOccupation.delegate = self
        self.load.isHidden = true
        self.scrollView.keyboardDismissMode = .onDrag
    }
    
    @IBAction func txtPenghasilanPerBulanChange(_ sender: Any) {
        if txtPenghasilanPerBulan.text == "" {
            txtPenghasilanPerBulan.text = "0"
        }
        let valString:String = self.txtPenghasilanPerBulan.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtPenghasilanPerBulan.text = formattedTipAmount
        }
    }
    
    @IBAction func txtPenghasilanTambahanChange(_ sender: Any) {
        if txtPenghasilanTambahan.text == "" {
            txtPenghasilanTambahan.text = "0"
        }
        let valString:String = self.txtPenghasilanTambahan.text!
        let hargaString:String = valString.replacingOccurrences(of: ".", with: "")
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: Int(hargaString)! as NSNumber) {
            self.txtPenghasilanTambahan.text = formattedTipAmount
        }
    }
    
    @IBAction func goTxtOccupationBegin(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("plafond", forKey: "set_simulasi")
            prefs.synchronize()
            self.txtOccupation.resignFirstResponder()
            let controller = ModalOccupation()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @objc func goOccupTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("plafond", forKey: "set_simulasi")
            prefs.synchronize()
            let controller = ModalOccupation()
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.customHeight = 300
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller, animated: true, completion: nil)
        })
    }
    
    @IBAction func goSimulasi(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            if self.checkSetuju.isCheckboxSelected == true {
                let prefs:UserDefaults = UserDefaults.standard
                let api_key = prefs.value(forKey: "api_key") as? String
                let token_number = prefs.value(forKey: "token_number") as? String
                let id_customer = prefs.value(forKey: "id_customer") as? String
                let id_occup = prefs.value(forKey: "id_occup") as? String
                
                let blnString:String = self.txtPenghasilanPerBulan.text!
                let bulanString:String = blnString.replacingOccurrences(of: ".", with: "")
                
                let tmbString:String = self.txtPenghasilanTambahan.text!
                let tambahanString:String = tmbString.replacingOccurrences(of: ".", with: "")
                
                //let totalIncome:String = String(Int(bulanString)! + Int(tambahanString)!)
                let headers = [
                    "api_key": api_key,
                    "token_number": token_number
                ]
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                self.load.isHidden = false
                self.load.startAnimating()
                
                let parameters = [
                    "id_customer": id_customer!,
                    "occupation_id": id_occup!,
                    "monthly_income": bulanString,
                    "additional_income": tambahanString,
                    "side_job": self.txtSumberPenghasilan.text!
                    ] as [String : Any]
                
                let appConfig : app_config = app_config()
                Alamofire.request(appConfig.getApiUrl(type: "plafond_simulation"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                    .responseJSON { response in
                        
                        if (response.result.value == nil) {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.load.isHidden = true
                            self.load.stopAnimating()
                            let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            if let value = response.result.value {
                                let json = JSON(value)
                                print(json)
                                if json["status"].stringValue == "success" {
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(json["plafond"].stringValue, forKey: "plafond")
                                    prefs.synchronize()
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_hasil_simulasi_plafond")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                } else {
                                    let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.load.isHidden = true
                            self.load.stopAnimating()
                        }
                }
            } else {
                let alertController = UIAlertController(title: "", message: "Please Accept Agreement ANDALAN Finance", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
