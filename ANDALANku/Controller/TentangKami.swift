//
//  TentangKami.swift
//  Andalanku
//
//  Created by Handoyo on 04/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit

class TentangKami: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var lblBranch: UILabel!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHome.backgroundColor = UIColor(patternImage: UIImage(named: "bg_home_full")!)
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapLblBranch = UITapGestureRecognizer(target:self, action:#selector(TentangKami.goBranchTap(_:)))
        self.lblBranch.isUserInteractionEnabled = true
        self.lblBranch.addGestureRecognizer(tapLblBranch)
    }
    
    @objc func goBranchTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            guard let url = URL(string: "https://www.andalanfinance.com/kantor-cabang/eng") else { return }
            UIApplication.shared.open(url)
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
