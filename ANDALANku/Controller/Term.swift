//
//  Term.swift
//  Andalanku
//
//  Created by Handoyo on 14/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Term: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var lblLegal: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            self.load.isHidden = false
            self.load.startAnimating()
            
            let parameters = [
                "keyword": ""
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "get_legal_document"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            if json["status"].stringValue == "success" {
                                for item in json["legal_document_list"].arrayValue {
                                    //self.lblLegal.text = item["content"].stringValue
                                    let htmlString = item["content"].stringValue
                                    let htmlText = htmlString
                                    if let htmlData = htmlText.data(using: String.Encoding.unicode) {
                                        do {
                                            let attributedText = try NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
                                            self.lblLegal.attributedText = attributedText
                                        } catch let e as NSError {
                                            print("Couldn't translate \(String(describing: htmlText)): \(e.localizedDescription) ")
                                        }
                                    }
                                }
                            }
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.load.isHidden = true
                        self.load.stopAnimating()
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
        vc.selectedIndex = 3
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

}
