//
//  UbahKataSandi.swift
//  Andalanku
//
//  Created by Handoyo on 09/05/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Material

class UbahKataSandi: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var load: UIActivityIndicatorView!
    @IBOutlet weak var txtKataSandiLama: TextField!
    @IBOutlet weak var txtPassword: TextField!
    @IBOutlet weak var txtPasswordConfirm: TextField!
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        self.scrollView.keyboardDismissMode = .onDrag
        self.load.isHidden = true
        
        let tapScroll = UITapGestureRecognizer(target:self, action:#selector(UbahKataSandi.goScrollTap(_:)))
        self.scrollView.isUserInteractionEnabled = true
        self.scrollView.addGestureRecognizer(tapScroll)
    }
    
    @objc func goScrollTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txtKataSandiLama.resignFirstResponder()
            self.txtPassword.resignFirstResponder()
            self.txtPasswordConfirm.resignFirstResponder()
        })
    }

    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "password_code": self.txtKataSandiLama.text!,
                "password": self.txtPassword.text!,
                "password_confirm": self.txtPasswordConfirm.text!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "reset_password_process"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            let status = json["status"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("lupa", forKey: "set_katasandi")
                                prefs.synchronize()
                                
                                let alertController = UIAlertController(title: "Informasi", message: "Kata sandi aplikasi Andalanku berhasil dirubah", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_login")
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                                
                                /*
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                vc.modalPresentationStyle = .fullScreen
                                 self.present(vc, animated: true, completion: nil)
                                */
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let prefs:UserDefaults = UserDefaults.standard
        let set_katasandi = prefs.value(forKey: "set_katasandi") as? String
        if set_katasandi == "lupa" {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_lupa_kata_sandi")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
            vc.selectedIndex = 3
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
}
