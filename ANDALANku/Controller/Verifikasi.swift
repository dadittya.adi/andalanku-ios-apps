//
//  Verifikasi.swift
//  Andalanku
//
//  Created by Handoyo on 31/03/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON
import SPStorkController

class Verifikasi: UIViewController {

    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var txt_pin: TextField!
    @IBOutlet var txt_konfirmasi: TextField!
    @IBOutlet var imgPin: UIImageView!
    @IBOutlet var imgKonfirmasi: UIImageView!
    @IBOutlet var imgMain: UIImageView!
    var iconClick = true
    var pageIndex:Int = 0
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        let tapPin = UITapGestureRecognizer(target:self, action:#selector(Verifikasi.goPinTap(_:)))
        self.imgPin.isUserInteractionEnabled = true
        self.imgPin.addGestureRecognizer(tapPin)
        
        let tapKonfirmasi = UITapGestureRecognizer(target:self, action:#selector(Verifikasi.goKonfirmasiTap(_:)))
        self.imgKonfirmasi.isUserInteractionEnabled = true
        self.imgKonfirmasi.addGestureRecognizer(tapKonfirmasi)
        
        let tapMain = UITapGestureRecognizer(target:self, action:#selector(Register.goMainTap(_:)))
        self.imgMain.isUserInteractionEnabled = true
        self.imgMain.addGestureRecognizer(tapMain)
        
    }
    
    @objc func dismissAction() {
        self.dismiss(animated: true)
    }
    
    @objc func goPinTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            if(self.iconClick == true) {
                self.txt_pin.isSecureTextEntry = false
                self.imgPin.image = UIImage(named: "icon_mata")
            } else {
                self.txt_pin.isSecureTextEntry = true
                self.imgPin.image = UIImage(named: "icon_mata")
            }
            self.iconClick = !self.iconClick
        })
    }
    
    @objc func goKonfirmasiTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            if(self.iconClick == true) {
                self.txt_konfirmasi.isSecureTextEntry = false
                self.imgKonfirmasi.image = UIImage(named: "icon_mata")
            } else {
                self.txt_konfirmasi.isSecureTextEntry = true
                self.imgKonfirmasi.image = UIImage(named: "icon_mata")
            }
            self.iconClick = !self.iconClick
        })
    }
    
    @objc func goMainTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_pin.resignFirstResponder()
            self.txt_konfirmasi.resignFirstResponder()
        })
    }
    
    @IBAction func goLanjut(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_register")
        self.present(vc, animated: true, completion: nil)
    }
}
