//
//  VerifikasiKodeOTP.swift
//  Andalanku
//
//  Created by Handoyo on 01/04/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON

class VerifikasiKodeOTP: UIViewController, UITextFieldDelegate {

    @IBOutlet var txt_kode_otp: TextField! {
        didSet {
            txt_kode_otp?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyNumericTextField)))
        }
    }
    
    @IBOutlet var txt_kode_otp2: TextField! {
        didSet {
            txt_kode_otp2?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButton2TappedForMyNumericTextField)))
        }
    }
    
    @IBOutlet var txt_kode_otp3: TextField! {
        didSet {
            txt_kode_otp3?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButton3TappedForMyNumericTextField)))
        }
    }
    
    @IBOutlet var txt_kode_otp4: TextField! {
        didSet {
            txt_kode_otp4?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButton4TappedForMyNumericTextField)))
        }
    }
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var imgMain: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var viewMain: UIView!
    var sizeViewHeight:CGFloat = 0.0
    var pageIndex:Int = 0
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet var nohpLabel: UILabel!
    @IBOutlet var kirimulangLabel: UILabel!
    @IBOutlet weak var load: UIActivityIndicatorView!
    
    var seconds = 300
    var timer = Timer()
    var isTimerRunning = false
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(VerifikasiKodeOTP.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        timerLabel.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        //let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        //return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        runTimer()
        self.navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navBar.shadowImage = UIImage()
        
        self.txt_kode_otp.delegate = self
        self.txt_kode_otp.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_kode_otp.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_kode_otp2.delegate = self
        self.txt_kode_otp2.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp2.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_kode_otp2.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp2.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_kode_otp3.delegate = self
        self.txt_kode_otp3.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp3.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_kode_otp3.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp3.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        self.txt_kode_otp4.delegate = self
        self.txt_kode_otp4.dividerActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp4.dividerNormalColor = UIColor(rgb: 0x069DE4)
        self.txt_kode_otp4.placeholderActiveColor = UIColor(rgb: 0x0D67A8)
        self.txt_kode_otp4.placeholderNormalColor = UIColor(rgb: 0x069DE4)
        
        let tapMain = UITapGestureRecognizer(target:self, action:#selector(VerifikasiKodeOTP.goMainTap(_:)))
        self.imgMain.isUserInteractionEnabled = true
        self.imgMain.addGestureRecognizer(tapMain)
        
        let tapViewMain = UITapGestureRecognizer(target:self, action:#selector(VerifikasiKodeOTP.goMainViewTap(_:)))
        self.viewMain.isUserInteractionEnabled = true
        self.viewMain.addGestureRecognizer(tapViewMain)
        
        let tapKirim = UITapGestureRecognizer(target:self, action:#selector(VerifikasiKodeOTP.goKirimTap(_:)))
        self.kirimulangLabel.isUserInteractionEnabled = true
        self.kirimulangLabel.addGestureRecognizer(tapKirim)
        
        self.txt_kode_otp.textAlignment = .center
        self.txt_kode_otp2.textAlignment = .center
        self.txt_kode_otp3.textAlignment = .center
        self.txt_kode_otp4.textAlignment = .center
        let mainFrame = self.view.frame
        self.sizeViewHeight = mainFrame.size.height
        self.scrollView.keyboardDismissMode = .onDrag
        self.load.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(VerifikasiKodeOTP.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(VerifikasiKodeOTP.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ sender: Notification) {
        let info = sender.userInfo!
        let keyboardHeight:CGFloat = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size.height
        
        var frame = self.view.frame
        
        frame.size.height = sizeViewHeight - keyboardHeight
        self.view.frame = frame
    }
    
    @objc func keyboardWillHide(_ sender: Notification) {
        var frame = self.view.frame
        
        frame.size.height = sizeViewHeight
        self.view.frame = frame
    }
    
    @IBAction func txtOtpChange(_ sender: Any) {
        txt_kode_otp.resignFirstResponder()
        txt_kode_otp2.becomeFirstResponder()
    }
    
    @IBAction func txtOtp2Change(_ sender: Any) {
        txt_kode_otp2.resignFirstResponder()
        txt_kode_otp3.becomeFirstResponder()
    }
    
    @IBAction func txtOtp3Change(_ sender: Any) {
        txt_kode_otp3.resignFirstResponder()
        txt_kode_otp4.becomeFirstResponder()
    }
    
    @IBAction func txtOtp4Change(_ sender: Any) {
        txt_kode_otp4.resignFirstResponder()
        setOTP()
    }
    
    @objc func goKirimTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            //KIRIM ULANG API SMS RESPONSE
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let parameters = [
                "id_customer": id_customer!
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "resend_otp"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
    
    @objc func goMainTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_kode_otp.resignFirstResponder()
        })
    }
    
    @objc func goMainViewTap(_ sender: UITapGestureRecognizer)
    {
        DispatchQueue.main.async(execute: {
            self.txt_kode_otp.resignFirstResponder()
        })
    }

    @IBAction func goKirim(_ sender: Any) {
        DispatchQueue.main.async(execute: {
            self.setOTP()
        })
    }
    
    @IBAction func goBack(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_register")
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func doneButtonTappedForMyNumericTextField() {
        print("Done");
        txt_kode_otp.resignFirstResponder()
        txt_kode_otp2.becomeFirstResponder()
    }
    
    @objc func doneButton2TappedForMyNumericTextField() {
        txt_kode_otp2.resignFirstResponder()
        txt_kode_otp3.becomeFirstResponder()
    }
    
    @objc func doneButton3TappedForMyNumericTextField() {
        txt_kode_otp3.resignFirstResponder()
        txt_kode_otp4.becomeFirstResponder()
    }
    
    @objc func doneButton4TappedForMyNumericTextField() {
        txt_kode_otp4.resignFirstResponder()
        setOTP()
    }
    
    func setOTP() {
        DispatchQueue.main.async(execute: {
            self.load.isHidden = false
            self.load.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let prefs:UserDefaults = UserDefaults.standard
            let api_key = prefs.value(forKey: "api_key") as? String
            let token_number = prefs.value(forKey: "token_number") as? String
            let set_otp = prefs.value(forKey: "set_otp") as? String
            let id_customer = prefs.value(forKey: "id_customer") as? String
            
            let headers = [
                "api_key": api_key,
                "token_number": token_number
            ]
            
            let kodeotp:String = self.txt_kode_otp.text! + self.txt_kode_otp2.text! + self.txt_kode_otp3.text! + self.txt_kode_otp4.text!
            
            let parameters = [
                "id_customer": id_customer!,
                "otp_code": kodeotp
                ] as [String : Any]
            
            let appConfig : app_config = app_config()
            Alamofire.request(appConfig.getApiUrl(type: "validate_otp"), method: .post, parameters: parameters, headers: headers as? HTTPHeaders)
                .responseJSON { response in
                    
                    if (response.result.value == nil) {
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alertController = UIAlertController(title: "", message: "Server / Jaringan Error", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        if let value = response.result.value {
                            let json = JSON(value)
                            print(json)
                            
                            if set_otp == "login" {
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "tab_home") as! UITabBarController
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_notifikasi_register")
                                self.present(vc, animated: true, completion: nil)
                            }
                            
                            /*
                            let status = json["status"].stringValue
                            let id_customer = json["id_customer"].stringValue
                            if status == "success" {
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set(id_customer, forKey: "id_customer")
                                prefs.synchronize()
                                
                                let vc = self.storyboard!.instantiateViewController(withIdentifier: "story_otp")
                                self.present(vc, animated: true, completion: nil)
                            } else {
                                let alertController = UIAlertController(title: "", message: json["message"].stringValue, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                            */
                        }
                        self.load.isHidden = true
                        self.load.stopAnimating()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
            }
        })
    }
}

extension TextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Batal", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Kirim", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}
