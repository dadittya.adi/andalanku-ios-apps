//
//  app_config.swift
//
//  Created by Handoyo on 19/03/19.
//  Copyright © 2019 Lokavor. All rights reserved.
//

import Foundation

class app_config {
    //let ANDALANKU_API: NSString = "http://depanbelih.id/andalanku-api/"
    let ANDALANKU_API: NSString = "http://devapi.andalanku.id/"
    
    func getApiUrl(type : String) -> String
    {
        var newUrl : String = ""
        switch type {
            case "get_token"  :
                newUrl = (ANDALANKU_API as String) + "get_token"
                break
            case "login"  :
                newUrl = (ANDALANKU_API as String) + "login"
                break
            case "validate_otp"  :
                newUrl = (ANDALANKU_API as String) + "validate_otp"
                break
            case "resend_otp"  :
                newUrl = (ANDALANKU_API as String) + "resend_otp"
                break
            case "register"  :
                newUrl = (ANDALANKU_API as String) + "register"
                break
            case "credit_simulation"  :
                newUrl = (ANDALANKU_API as String) + "credit_simulation"
                break
            case "promo_list"  :
                newUrl = (ANDALANKU_API as String) + "promo_list"
                break
            case "news_list"  :
                newUrl = (ANDALANKU_API as String) + "news_list"
                break
            case "partner_list"  :
                newUrl = (ANDALANKU_API as String) + "partner_list"
                break
            case "complain_category_list"  :
                newUrl = (ANDALANKU_API as String) + "complain_category_list"
                break
            case "input_e_complain"  :
                newUrl = (ANDALANKU_API as String) + "input_e_complain"
                break
            case "e_complain_list"  :
                newUrl = (ANDALANKU_API as String) + "e_complain_list"
                break
            case "province_list"  :
                newUrl = (ANDALANKU_API as String) + "province_list"
                break
            case "city_list"  :
                newUrl = (ANDALANKU_API as String) + "city_list"
                break
            case "district_list"  :
                newUrl = (ANDALANKU_API as String) + "district_list"
                break
            case "village_list"  :
                newUrl = (ANDALANKU_API as String) + "village_list"
                break
            case "profile_update_request"  :
                newUrl = (ANDALANKU_API as String) + "profile_update_request"
                break
            case "faq_list"  :
                newUrl = (ANDALANKU_API as String) + "faq_list"
                break
            case "car_brand_list"  :
                newUrl = (ANDALANKU_API as String) + "car_brand_list"
                break
            case "car_type_list"  :
                newUrl = (ANDALANKU_API as String) + "car_type_list"
                break
            case "car_trim_list"  :
                newUrl = (ANDALANKU_API as String) + "car_trim_list"
                break
            case "inbox_list"  :
                newUrl = (ANDALANKU_API as String) + "inbox_list"
                break
            case "inbox_detail"  :
                newUrl = (ANDALANKU_API as String) + "inbox_detail"
                break
            case "loan_application"  :
                newUrl = (ANDALANKU_API as String) + "loan_application"
                break
            case "occupation_list"  :
                newUrl = (ANDALANKU_API as String) + "occupation_list"
                break
            case "agreement_list"  :
                newUrl = (ANDALANKU_API as String) + "agreement_list"
                break
            case "reset_password"  :
                newUrl = (ANDALANKU_API as String) + "reset_password"
                break
            case "reset_password_process"  :
                newUrl = (ANDALANKU_API as String) + "reset_password_process"
                break
            case "get_legal_document"  :
                newUrl = (ANDALANKU_API as String) + "get_legal_document"
                break
            case "insurance_claim"  :
                newUrl = (ANDALANKU_API as String) + "insurance_claim"
                break
            case "insurance_claim_list"  :
                newUrl = (ANDALANKU_API as String) + "insurance_claim_list"
                break
            case "collateral_request"  :
                newUrl = (ANDALANKU_API as String) + "collateral_request"
                break
            case "collateral_request_list"  :
                newUrl = (ANDALANKU_API as String) + "collateral_request_list"
                break
            case "prepayment_simulation"  :
                newUrl = (ANDALANKU_API as String) + "prepayment_simulation"
                break
            case "prepayment_request"  :
                newUrl = (ANDALANKU_API as String) + "prepayment_request"
                break
            case "andalan_detail_financial"  :
                newUrl = (ANDALANKU_API as String) + "andalan_detail_financial"
                break
            case "andalan_detail_payment_history"  :
                newUrl = (ANDALANKU_API as String) + "andalan_detail_payment_history"
                break
            case "andalan_detail_unit"  :
                newUrl = (ANDALANKU_API as String) + "andalan_detail_unit"
                break
            case "andalan_detail_insurance"  :
                newUrl = (ANDALANKU_API as String) + "andalan_detail_insurance"
                break
            case "used_car_price"  :
                newUrl = (ANDALANKU_API as String) + "used_car_price"
                break
            case "cash_value"  :
                newUrl = (ANDALANKU_API as String) + "cash_value"
                break
            case "branch_list"  :
                newUrl = (ANDALANKU_API as String) + "branch_list"
                break
            case "personal_information"  :
                newUrl = (ANDALANKU_API as String) + "personal_information"
                break
            case "plafond_simulation"  :
                newUrl = (ANDALANKU_API as String) + "plafond_simulation"
                break
            case "plafond_application"  :
                newUrl = (ANDALANKU_API as String) + "plafond_application"
                break
            case "andalan_financial"  :
                newUrl = (ANDALANKU_API as String) + "andalan_financial"
                break
            default: break
        }
        return (newUrl as NSString) as String
    }
}
